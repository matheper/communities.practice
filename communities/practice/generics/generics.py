# -*- coding: utf-8 -*-
import textwrap
import string
import re
import csv
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import _createObjectByType                                                       

from Acquisition import aq_parent, aq_inner, aq_chain
from AccessControl import getSecurityManager
from zope.component import queryUtility
from plone.i18n.normalizer.interfaces import IIDNormalizer
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from zope.component.hooks import getSite
from zope.component import getUtility


def cleanhtml(raw_html):
    """Remove HTML tags from a string
    """
    cleanr =re.compile('<.*?>')
    cleantext = re.sub(cleanr,'', raw_html)
    return cleantext

def limit_description(description, limit=139):
    """Restrict a description text down to a certain character limit."""
    wrapped = textwrap.wrap(unicode(description, "utf-8"), width=limit)
    if wrapped:
        first_line = wrapped[0].strip(string.punctuation)
        return (first_line + u"\u2026").encode("utf-8")
    return ""

def getCoPContext(obj, get_type=['CoP', 'CoPCommunityFolder']):
    """ Bottom-up function that returns first community or communities folder.
        You can force search to return only community or communities folder.
    """
    for parent in aq_chain(aq_inner(obj)):
        if hasattr(parent, 'portal_type'):
            if parent.portal_type in get_type: 
                return parent
    return False

def getMemberData(obj, user_id):
    """ Recebe um objeto e uma string com o ID do membro.
        Retorna uma tupla com fullname e email"""
    portal = obj.portal_url.getPortalObject()
    user = portal.portal_membership.getMemberById(user_id)
    if user:
        fullname = encodeUTF8(user.getProperty('fullname','Fullname missing'))
        email = user.getProperty('email',None)
        if not fullname:
            fullname = user_id
        return (fullname, email)
    return False

def getMembersData(obj, users_list_id):
    """ Recebe um objeto e uma lista com o ID dos membros.
        Retorna um lista de dicionarios com id, full_name, email, img_url e author_url"""
    portal = obj.portal_url.getPortalObject()
    users_list= []
    for user_data in users_list_id:
        user = portal.portal_membership.getMemberById(user_data)
        if user:
            fullname = encodeUTF8(user.getProperty('fullname','Fullname missing'))
            email = user.getProperty('email',None)
            img_url = portal.portal_membership.getPersonalPortrait(user.getId()).absolute_url()
            author_url = "%s/author/%s" % (portal.absolute_url(), user.getId())
            if not fullname:
                fullname = user.getId()
            users_list.append({ "id" : user.getId(),
                                "full_name" : fullname,
                                "email" : email,
                                "img_url" : img_url,
                                "author_url" : author_url,})
    return users_list

def encodeUTF8(texto):
    """codifica uma string para utf-8
    """
    try:
        encoded = texto.encode('utf-8')
    except:
        encoded = texto
    return encoded

def allowTypes(obj, list_types = []):
    """Locally allow inclusion of a list of types in an object
    """
    obj.setConstrainTypesMode(True)
    obj.setLocallyAllowedTypes(list_types)
    obj.setImmediatelyAddableTypes(list_types)
    obj.reindexObject()

def getCoPParticipants(cop):
    """ Receives CoP object and returns list of participants.
    """
    participants = []
    list_local_roles = cop.get_local_roles()
    for user_id, roles in list_local_roles:
        if ("Participante" in roles) or ("Moderador" in roles) or ("Owner" in roles):
            participants.append(user_id)
    return participants

def getCoPRole(cop_uid, user_id):
    """ Receives CoP UID and user id and returns user role from community cache.
    """
    from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
    member_communities = listMemberRoleCommunitiesParticipating(user_id)
    member_communities = member_communities.get(user_id)
    role = member_communities.get(cop_uid, "")
    return role

def getLocalRole(context, user_id):
    """ Receives context and user id and returns user role from local roles.
    """
    return context.get_local_roles_for_userid(user_id)

def getBrainValues(local, brain_list, datetime_type=""):
    """
        Receives a catalog brain list.
        Returns a list of dictionaries.
    """
    portal_url = local.portal_url()
    portal_membership = getToolByName(local, 'portal_membership')
    news = []
    for item in brain_list:
        if item.id != "arquivo_configuracao":
            user = getMemberData(local, item.Creator)
            if user:
                title = encodeUTF8(item.Title)
                if not title:
                    title = item.id
                descricao = limit_description(encodeUTF8(item.Description),600)
                id_criador = item.Creator
                imagem_criador = portal_membership.getPersonalPortrait(id_criador).absolute_url()
                url = item.getURL()
                path = item.getPath()
                tipo_item = item.portal_type
                label_tipo = item.Type
                review_state = item.review_state
                size = item.getObjSize
                partOfCommunity = item.partOfCommunity
                mensagem = "postou"

                if label_tipo == 'Pagina':
                    label_tipo = 'Página'
                url_imagem = ''
                if tipo_item in ["CoPImage", "CoPProfileImage",]:
                    url_imagem = "%s" % url
                remote_url = ''
                if tipo_item == "CoPLink":
                    remote_url = item.getRemoteUrl
                if tipo_item == "CoPShare":
                    mensagem = "compartilhou"
                datetime = item.created
                if datetime_type == "modified":
                    datetime = item.modified

                news.append({"titulo": title,
                             "id": item.id,
                             "nome_criador": user[0],
                             "id_criador": id_criador,
                             "descricao": descricao,
                             "url": url,
                             "tipo":tipo_item,
                             "label_tipo":label_tipo,
                             "imagem_criador":imagem_criador,
                             "partOfCommunity":partOfCommunity,
                             "timestamp":datetime,
                             "url_imagem":url_imagem,
                             "url_criador":"%s/author/%s"%(portal_url, id_criador),
                             "url_icone":"%s/%s"%(portal_url, item.getIcon),
                             "remote_url":remote_url,
                             "path":path,
                             "estado":review_state,
                             "tamanho":size,
                             "mensagem":mensagem
                            })
    return news

def getCoPUpdates(local, limit = 0, types = ''):
    """ Search types(string list) into local(obj).
        Limit = 0 returns all values.
        Returns dict list from getBrainValues.
    """
    catalog = getToolByName(local, 'portal_catalog')
    path = '/'.join(local.getPhysicalPath())
    if not types:
        types = [ "CoPEvent", "CoPLink", "CoPImage", "CoPShare",
                  "CoPFile", "CoPDocument", "CoPATA",
                  "PloneboardConversation",
                  "BFCheckList", "BFGoodPracticeReport", "BFVisitAcknowledge", "DABExperience", "DABChroniclesEvaluation"]
    query = {}
    query['path'] = path
    query['portal_type'] = types
    if limit:
        query['sort_limit'] = limit
    query['sort_on'] = 'copLastModification'
    query['sort_order'] = 'reverse'
    items = catalog(query)
    return items

def getUsersByRole(obj, user_type=["Participante"]):
    """Recebe o objeto onde sera feita a pesquisa e
       uma lista de strings com o tipo de usuario.
       Retorna um dicionario onde a chave é o tipo de usuario e o
       valor é uma lista de usuarios com (id, nome, email, url_img, url_author).
    """
    portal = obj.portal_url.getPortalObject()
    portal_membership = getToolByName(obj,"portal_membership")
    list_local_roles = obj.get_local_roles()
    users_list = {}
    for Type in user_type:
        users_list[Type] = []
        for user in list_local_roles:
            if Type in user[1]:
                user_full = getMemberData(obj, user[0])
                if user_full and user_full[1]:
                    img_url = portal_membership.getPersonalPortrait(user[0]).absolute_url()
                    author_url = "%s/author/%s" % (portal.absolute_url(), user[0])
                    users_list[Type].append({ "id" : user[0],
                                              "full_name" : user_full[0],
                                              "email" : user_full[1],
                                              "img_url" : img_url,
                                              "author_url" : author_url,})
    return users_list

def getCommunitiesParticipating(user, local):
    """
        Busca as comunidades, a partir do local, que o usuario participa.
    """
    portal = getToolByName(local, 'portal_url').getPortalObject()
    catalog = getToolByName(portal, 'portal_catalog')
    options = {}
    options['portal_type'] = "CoP"
    options['path'] = '/'.join(local.getPhysicalPath())
    communities = catalog(options)
    communities_participating = []
    for brain_community in communities:
        community = brain_community.getObject()
        local_roles = community.get_local_roles_for_userid(user.getId())
        if local_roles and not 'Bloqueado' in local_roles and not 'Aguardando' in local_roles:
            communities_participating.append(community)
    return communities_participating

def getCommentaries(context, path):
    """
        Busca Comentarios a partir do path. Pode ser limitado atraves do parametro qtd.
        Retorna no formato lista de dicionarios.
    """
    catalog = getToolByName(context, 'portal_catalog')
    query = {}
    query['sort_on'] = 'created'
    query['path'] = path
    query['portal_type'] = ['Discussion Item', 'PloneboardComment']
    commentaries = catalog(query)
    brain_values = getBrainValues(context, commentaries)
    return brain_values

def isDiscussionAllowed(context, cop_uid):
    """
        Verifica se comentarios sao permitidos para o objeto.
    """
    from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating

    can_reply = getSecurityManager().checkPermission('Reply to item', aq_inner(context))
    portal_membership = getToolByName(context, 'portal_membership')
    authenticated_member = portal_membership.getAuthenticatedMember()
    authenticated_id = authenticated_member.getId()
    if cop_uid:
        communities_participating = listMemberRoleCommunitiesParticipating(authenticated_id)
        communities_participating = communities_participating.get(authenticated_id)
        authenticated_role = communities_participating.get(cop_uid)
        can_reply = can_reply and  authenticated_role in ["Participante", "Moderador", "Owner"]
    # Check if anonymous comments are allowed in the registry
    registry = queryUtility(IRegistry)
    settings = registry.forInterface(IDiscussionSettings, check=False)
    anonymous_discussion_allowed = settings.anonymous_comments
    globally_enabled = settings.globally_enabled

    return globally_enabled and (can_reply or anonymous_discussion_allowed)

def getCoPFormsInstalled():
    """
    """
    portal = getSite()
    visible = portal.portal_quickinstaller.isProductInstalled("cop.forms")
    return visible

def getUserMailList(context):
    """
        Retorna string com lista de emails dos usuarios que recebem novidades.
    """
    try:
        cop = getCoPContext(context, ['CoP'])
        if not hasConfigurationFile(cop):
            createConfigurationFile(cop)
        arquivo_configuracao = cop.configuracoes.arquivo_configuracao
        string_usuarios = arquivo_configuracao.data
        return string_usuarios
    except:
        return ""

def setUserMailList(context, email, inserir = True):
    """
        Insere ou remove email do usuario autenticado a lista de envio de emails da comunidade.
    """
    if email:
        try:
            cop = getCoPContext(context, ['CoP'])
            arquivo_configuracao = cop.configuracoes.arquivo_configuracao
            lista_usuarios = arquivo_configuracao.data
            if inserir:
                if email and not email in lista_usuarios:
                    lista_usuarios += email + '\n'
            else:
                if email and email in lista_usuarios:
                    lista_usuarios = lista_usuarios.replace(email + '\n', '')
            arquivo_configuracao.setFile(lista_usuarios)
            return True
        except:
            return False
    return False

def changeCoPOwnership(community, user_id, permission="Excluir"):
    """ Recebe objeto CoP e id do usuário.
        Altera Owner e Creator de todos os tipos
        container criados pelo usuario.
    """
    from communities.practice.generics.cache import updateCommunityCache
    master_roles = ["Moderador_Master", "Observador_Master"]
    catalog = getToolByName(community, "portal_catalog")
    query = {}
    query['portal_type'] = ['CoPFolder', 'CoP', 'CoPMenu', 'CoPUpload']
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    cop_objects = catalog(query)
    cop_owner = community.getOwner()
    cop_owner_id = cop_owner.getId()
    for cop_obj in cop_objects:
        obj = cop_obj.getObject()
        #change creator
        old_roles = obj.get_local_roles_for_userid(user_id)
        new_roles = [role for role in old_roles if role in master_roles]
        obj.manage_delLocalRoles([user_id])
        if permission != "Excluir":
            new_roles.append(permission)
        if new_roles:
            obj.manage_setLocalRoles(user_id, new_roles)
        obj.setCreators(cop_owner_id)
        #change owner
        obj.changeOwnership(cop_owner, recursive=False)
        roles = list(obj.get_local_roles_for_userid(cop_owner_id))
        if not 'Owner' in roles:
            roles.append('Owner')
        obj.manage_setLocalRoles(cop_owner_id, roles)
        if obj.portal_type == 'CoP':
            updateCommunityCache(obj, user_id, None)
            updateCommunityCache(obj, cop_owner_id, 'Owner')
        obj.reindexObject()

def blockPortfolio(community, user_id):
    """Bloqueia portfolio de usuarios excluidos da CoP.
    """
    catalog = getToolByName(community, "portal_catalog")
    portal_workflow = getToolByName(community, "portal_workflow")
    query = {}
    query['portal_type'] = 'CoPPortfolio'
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    brain = catalog(query)
    for portfolio in brain:
        obj = portfolio.getObject()
        if portal_workflow.getInfoFor(obj, 'review_state') in ['publico','restrito']:
            portal_workflow.doActionFor(obj, 'privado')
            portal_workflow.doActionFor(obj, 'bloqueado')
            obj.reindexObjectSecurity()
            return
        if portal_workflow.getInfoFor(obj, 'review_state') == 'privado':
            portal_workflow.doActionFor(obj, 'bloqueado')
            obj.reindexObjectSecurity()

def propagateMasterRole(community_folder, user_id, permission):
    """ Insere permissao local para a pasta de comunidades.
        Propaga ou remove permissoes master para CoPs e SubCoPs.
    """

    master_roles = ["Moderador_Master", "Observador_Master"]
    old_roles = list(community_folder.get_local_roles_for_userid(user_id))
    new_roles = [role for role in old_roles if not role in master_roles]
    if permission in ["Moderador_Master", "Observador_Master"]:
        new_roles.append(permission)
    if new_roles:
        community_folder.manage_setLocalRoles(user_id, new_roles)
    else:
        community_folder.manage_delLocalRoles([user_id])
    community_folder.reindexObjectSecurity()

    portal_catalog = getToolByName(community_folder, 'portal_catalog')
    query = {}
    query['path'] = '/'.join(community_folder.getPhysicalPath())
    query['portal_type'] = 'CoP'
    brain = portal_catalog(query)
    for cop_brain in brain:
        cop = cop_brain.getObject()
        old_roles = list(cop.get_local_roles_for_userid(user_id))
        new_roles = [role for role in old_roles if not role in master_roles]
        if permission in ["Moderador_Master", "Observador_Master"]:
            new_roles.append(permission)
        if new_roles:
            cop.manage_setLocalRoles(user_id, new_roles)
        else:
            cop.manage_delLocalRoles([user_id])
        cop.reindexObjectSecurity()

def propagateRole(community, user_id, permission):
    """ Insere permissao para a comunidade recebida por parametro e
        propaga para as SubCoPs conforme tipo do papel. Ticket #747.
    """
    from communities.practice.generics.cache import updateCommunityCache

    master_roles = ["Moderador_Master", "Observador_Master"]
    portal_catalog = getToolByName(community, 'portal_catalog')
    query = {}
    query['path'] = '/'.join(community.getPhysicalPath())
    query['portal_type'] = 'CoP'
    brain = portal_catalog(query)
    name, email = getMemberData(community, user_id)

    for cop_brain in brain:
        cop = cop_brain.getObject()
        old_roles = list(cop.get_local_roles_for_userid(user_id))
        old_local_roles = [role for role in old_roles if not role in master_roles]
        new_roles = [role for role in old_roles if role in master_roles]

        if permission == "Participante":
            if "Bloqueado" in old_local_roles:
                new_roles.append(permission)
                updateCommunityCache(cop, user_id, permission)
                setUserMailList(cop, email, True)
            else:
                new_roles += old_local_roles

        if permission == "Excluir":
            updateCommunityCache(cop, user_id, None)
            changeCoPOwnership(cop, user_id, permission)
            blockPortfolio(cop, user_id)
            setUserMailList(cop, email, False)
            deleteCoPShareUser(community, user_id)
        elif permission == "Moderador":
            new_roles.append(permission)
            if "Owner" in old_roles:
                new_roles.append("Owner")
            updateCommunityCache(cop, user_id, permission)
            setUserMailList(cop, email, True)
        elif permission == "Bloqueado" and old_local_roles:
            new_roles.append(permission)
            updateCommunityCache(cop, user_id, permission)
            changeCoPOwnership(cop, user_id, permission)
            blockPortfolio(cop, user_id)
            setUserMailList(cop, email, False)
        elif permission == "Observador":
            new_roles.append(permission)
            updateCommunityCache(cop, user_id, permission)
            changeCoPOwnership(cop, user_id, permission)
            blockPortfolio(cop, user_id)
            setUserMailList(cop, email, False)

        if new_roles:
            cop.manage_setLocalRoles(user_id, new_roles)
        else:
            cop.manage_delLocalRoles([user_id])
        cop.reindexObjectSecurity()

#   Garante alteracao local do papel para participante.
    if permission == "Participante":
        old_roles = list(community.get_local_roles_for_userid(user_id))
        new_roles = [role for role in old_roles if role in master_roles]
        new_roles.append(permission)
        updateCommunityCache(community, user_id, permission)
        setUserMailList(community, email, True)
        community.manage_setLocalRoles(user_id, new_roles)


def isSubCoP(community):
    """ Recebe objeto CoP.
        Retorna True para SubCoP e False para RootCoP.
    """
    parent = aq_parent(aq_inner(community))
    if parent.portal_type == "CoPMenu":
        return True
    return False

def setSuperCoPRoles(community):
    """ Recebe objeto CoP que esta sendo criado.
        Metodo busca por papeis da lista no nivel superior
        e os insere no local roles da SubCoP.
    """
    from communities.practice.generics.cache import updateCommunityCache
    roles_list = ["Moderador_Master",
                  "Observador_Master",
                  "Moderador",
                  "Observador",
                 ]
    roles_to_cache = ["Moderador", "Observador"]
    cop_context = getCoPContext(aq_parent(community))
    super_cop_roles = cop_context.get_local_roles()
    super_members = []
    for user_id, super_roles in super_cop_roles:
        roles = [role for role in super_roles if role in roles_list]
        if roles:
            local_roles = community.get_local_roles_for_userid(user_id)
            if local_roles:
                roles = list(set(list(local_roles) + roles))
            community.manage_setLocalRoles(user_id, roles)
            role_to_cache = [role for role in roles if role in roles_to_cache]
            if role_to_cache:
                updateCommunityCache(community, user_id, roles_to_cache[0])
            name, email = getMemberData(community, user_id)
            setUserMailList(community, email, True)
    community.reindexObjectSecurity()

def hasSuperRole(community, user_id):
    """ Recebe objeto CoP e id do usuario.
        Retorna True se usuario e' moderador ou observador da CoP do nivel superior.
    """
    super_cop = getSuperCoP(community)
    if super_cop:
        local_roles = super_cop.get_local_roles_for_userid(user_id)
        if "Moderador" in local_roles or "Owner" in local_roles \
            or "Observador" in local_roles or "Bloqueado" in local_roles:
            return True
    return False

def getSubCoPDepth(community):
    """ Recebe objeto CoP. Retorna nivel na hierarquia de comunidades.
        Retorna 0 para CoP e maior que 0 para SubCoP.
    """
    depth = -1
    parents = aq_chain(aq_inner(community))
    for parent in parents:
        portal_type = getattr(parent, 'portal_type','')
        if portal_type == 'CoP':
            depth += 1
    return depth

def getSuperCoP(community):
    """ Recebe SubCoP e retorna CoP do nivel superior.
        Retorna False se nao existir CoP acima.
    """
    super_cop = aq_parent(aq_parent(aq_inner(community)))
    if super_cop.portal_type == "CoP":
        return super_cop
    return False

def getRootCoP(community):
    """ Retorna primeira CoP da hierarquia
    """
    for parent in aq_chain(aq_inner(community))[::-1]:
        if hasattr(parent, 'portal_type'):
            if parent.portal_type == 'CoP':
                return parent
    return False

def deleteCoPShareUser(community, user_id):
    """ Deleta todos os CoPShare do user_id na community
    """
    portal_catalog = getToolByName(community, "portal_catalog")
    query = {}
    query['portal_type'] = 'CoPShare'
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    copshares = portal_catalog(query)
    for copshare in copshares:
        obj = copshare.getObject()
        parent = aq_parent(aq_inner(obj))
        parent.manage_delObjects([obj.getId()])
    return False


def addParticipantsInBlock(community, user_list):
    """ Recebe comunidade e lista de usuários.
        Adiciona participantes nas comunidades em cascada.
        Do nível mais baixo para o mais alto.
    """
    from communities.practice.generics.cache import updateCommunityCache
    permissao = 'Participante'
    object_list = aq_chain(community)
    for obj in object_list:
        if getattr(obj, 'portal_type','') == 'CoP':
            for user in user_list:
                obj.manage_setLocalRoles(user,[permissao])
                updateCommunityCache(obj, user, permissao)
                name, email = getMemberData(obj, user)
                setUserMailList(obj, email, False)
            obj.reindexObjectSecurity()
    root_cop = getRootCoP(community)
    portal_groups = getToolByName(community, 'portal_groups')
    group_id = root_cop.UID()
    if group_id in portal_groups.getGroupIds():
        for user in user_list:
            portal_groups.addPrincipalToGroup(user, group_id)

def generateContentId(context, string):
    """ Normaliza string e verifica se e' valida para o contexto.
        Caso nao seja incrementa numeracao ao final da string.
    """
    normalizer = getUtility(IIDNormalizer)
    new_id = normalizer.normalize(string)

    v_id = 2
    while new_id in context.objectIds():
        new_id = normalizer.normalize(string + str(v_id))
        v_id += 1
    return new_id

def canDeleteCoPFolder(copfolder):
    """Verifica se a CoPFolder pode ser deletada pelo usuario logado
    """
    portal_membership = getToolByName(copfolder, "portal_membership")
    authenticated_member = portal_membership.getAuthenticatedMember()
    authorized = ['Moderador','Owner','Manager','Moderador_Master']
    roles = authenticated_member.getRolesInContext(copfolder)
    return bool([i for i in roles if i in authorized])

def exportCSVDownload(request, header, data, filename):
    """ Exporta em CSV e faz download na maquina local, com um cabecalho
        'header', uma lista de dados 'data' com nome de arquivo 'filename'
    """
    writer = csv.writer(request.response)
    request.response.setHeader('Content-Type', 'text/csv', 'charset=UTF-8')
    request.response.setHeader('Content-Disposition', 'attachment; filename="%s"' % filename)
    writer.writerow(header)
    for row in data:
        writer.writerow(row)
    return False

def hasConfigurationFile(cop):
    """ Tests if cop has configuration file
    """
    from communities.practice.content.CoPMenu import CoPMenu
    has_file = False
    copmenu_config = getattr(cop, "configuracoes", None)
    if copmenu_config and isinstance(copmenu_config, CoPMenu):
        has_file = "arquivo_configuracao" in copmenu_config.objectIds()
    return has_file

def createConfigurationFile(cop):
    """ Creates file with participants email for cop
    """
    copmenu_config = getattr(cop, "configuracoes", None)
    if copmenu_config:
        allowTypes(copmenu_config, ["CoPFile"])
        arquivo_configuracao = _createObjectByType("CoPFile",
                                                   copmenu_config,
                                                   id="arquivo_configuracao")
        portal_workflow = getToolByName(cop, "portal_workflow")
        portal_workflow.doActionFor(arquivo_configuracao, "privado")
        allowTypes(copmenu_config, []) 
        arquivo_configuracao.setTitle(u'configuracao')
        users_by_role = getUsersByRole(cop, ["Participante","Moderador","Moderador_Master","Observador","Owner"])
        for users in users_by_role.values():
            for user in users:
               setUserMailList(arquivo_configuracao, user["email"], True)
        arquivo_configuracao.reindexObject()
        arquivo_configuracao.unmarkCreationFlag()

    return True

def canCutObjects(context, ids=None, REQUEST=None):
    """ Verifica se os objetos com ids podem ser excluidos pelo
        usuario logado no context
    """
    #Se nao for container da CoP, retorna True para manter o 
    #padrao do Plone
    if context.portal_type not in ["CoPMenu", "CoPFolder"]:
        return True
    if type(ids) is str:
        ids = [ids]
    portal_membership = getToolByName(context, "portal_membership")
    checkPermission = portal_membership.checkPermission
    for id in ids:
        cut_object = context._getOb(id, None)
        #se o objeto nao existe, retorna True e deixa o Plone se virar
        if not cut_object:
            return True
        #se nao tiver permissao de Modify no conteudo recortado,
        #retorna False para impedir a acao
        if not checkPermission('Modify portal content', cut_object):
            return False
    return True
