# -*- coding: utf-8 -*-
from time import time
from zope.component import getMultiAdapter

from Products.CMFCore.utils import getToolByName
from plone.memoize import ram
from zope.component.hooks import getSite
from communities.practice.generics.generics import getMemberData
from communities.practice.config import USE_MEMCACHED

def getCoPLocalRolesCacheKey(fun, cop, brain=True):
    if brain:
        return cop.UID
    return cop.UID()

COP_CACHE_ROLES = ["Participante", "Moderador", "Owner", "Aguardando", "Bloqueado", "Observador"]
@ram.cache(getCoPLocalRolesCacheKey)
def getCoPLocalRoles(cop, brain=True):
    """ Receives CoP brain and returns list of participants.
        Returns: {cop.UID : {"Role x": [["member1_id","member1_fullname"], ["member2_id","member2_fullname"], ...], "Role Y": [["member3_id","member3_fullname"], ["member4_id","member4_fullname"]], "Role W": []}}
    """
    cop_obj = cop.getObject() if brain else cop
    local_roles = {}
    local_roles[cop_obj.UID()] = {}
    for role in COP_CACHE_ROLES:
        local_roles[cop_obj.UID()][role] = []
    list_local_roles = cop_obj.get_local_roles()
    for roles in list_local_roles:        
        role = roles[1][0]
        if role in local_roles[cop_obj.UID()].keys():
            member_id = roles[0]
            fullname = member_id
            member_data = getMemberData(cop_obj, member_id)
            if member_data:
                fullname = member_data[0]
                local_roles[cop_obj.UID()][role].append([member_id, fullname])
    return local_roles

def communitiesParticipatingCacheKey(fun, member_id):
    return member_id

@ram.cache(communitiesParticipatingCacheKey)
def listMemberRoleCommunitiesParticipating(member_id):
    """Gera lista com as comunidades em que o membro participa
       Retorna: {member_id : {cop1.UID: role, cop2.UID: role, cop3.UID: role, ...}}
    """
    communities_participating = {}
    communities_participating[member_id] = {}
    portal = getSite()
    catalog = getToolByName(portal, 'portal_catalog')    
    catalog_path = '/'.join(portal.getPhysicalPath())
    communities = catalog(path = catalog_path, 
                          portal_type = "CoP")
    for brain_community in communities:
        community = brain_community.getObject()
        local_roles = community.get_local_roles_for_userid(member_id)
        if local_roles:
            communities_participating[member_id][brain_community.UID] = local_roles[0]
    return communities_participating

def updateCommunityCache(cop, member_id, role=None):
    """Atualiza o cache da CoP e dos Members
    """
    cached_function = None
    for f in listMemberRoleCommunitiesParticipating.func_closure:
        if f.cell_contents.__name__ == "listMemberRoleCommunitiesParticipating":
            cached_function = f.cell_contents
            break
    if cached_function:
        key = "%s.%s:%s" % (cached_function.__module__, cached_function.__name__, member_id)
        cache_storage = ram.store_in_cache(cached_function)
        cached_value = cache_storage.get(key)
        if not cached_value:
            cached_value = listMemberRoleCommunitiesParticipating(member_id)
        member_communities = cached_value.get(member_id)
        if role:
            member_communities[cop.UID()] = role
        else:
            member_communities.pop(cop.UID(), None)
        #faz a atualizacao no cache_storage
        cached_value[member_id] = member_communities
        cache_storage.__setitem__(key, cached_value)

    catalog = getToolByName(cop, "portal_catalog")
    community_brain = catalog(UID=cop.UID())
    community_brain = community_brain[0]    
    cached_function = None
    for f in getCoPLocalRoles.func_closure:
        if f.cell_contents.__name__ == "getCoPLocalRoles":
            cached_function = f.cell_contents
            break
    if cached_function:
        key = "%s.%s:%s" % (cached_function.__module__, cached_function.__name__, community_brain.UID)
        cache_storage = ram.store_in_cache(cached_function)
        cached_value = cache_storage.get(key)
        if not cached_value:
            cached_value = getCoPLocalRoles(community_brain)
        communities_local_roles = cached_value.get(community_brain.UID)
        if role:
            #adicionar member no novo role
            members_with_role = [member[0] for member in communities_local_roles[role]]
            if member_id not in members_with_role:
                fullname = member_id
                member_data = getMemberData(cop, member_id)
                if member_data:
                    fullname = member_data[0]
                    communities_local_roles[role].append([member_id, fullname])
            #remover roles anteriores, caso existam
            for cop_role in COP_CACHE_ROLES:    
                if role != cop_role and role != "Owner":
                    old_value = filter(lambda item:item[0]==member_id, communities_local_roles[cop_role])
                    if old_value:                    
                        communities_local_roles[cop_role].remove(old_value[0])
        else:
            for cop_role in COP_CACHE_ROLES:
                old_value = filter(lambda item:item[0]==member_id, communities_local_roles[cop_role])
                if old_value:                    
                    communities_local_roles[cop_role].remove(old_value[0])
        #faz a atualizacao no cache_storage
        cached_value[community_brain.UID] = communities_local_roles
        cache_storage.__setitem__(key, cached_value)
    return False

def cleanMemberCache(member_id):
    """Limpa o cache dos members
    """
    if USE_MEMCACHED:
        cached_function = None
        for f in listMemberRoleCommunitiesParticipating.func_closure:
            if f.cell_contents.__name__ == "listMemberRoleCommunitiesParticipating":
                cached_function = f.cell_contents
                break
        if cached_function:
            key = "%s.%s:%s" % (cached_function.__module__, cached_function.__name__, member_id)
            cache_storage = ram.store_in_cache(cached_function)
            cached_value = cache_storage.get(key)
            if cached_value:
                cache_storage.client.delete(key)

    return False
