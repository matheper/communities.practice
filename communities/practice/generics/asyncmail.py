# -*- coding:utf-8 -*-
from zope.sendmail.maildir import Maildir
from zope.sendmail.maildir import MaildirMessageWriter
from zope.sendmail.queue import QueueProcessorThread
from zope.sendmail.interfaces import IMailDelivery
from zope.sendmail.mailer import SMTPMailer
from communities.practice.generics import asyncmail_config
from email.header import Header
import os
import logging


class queueProcess(QueueProcessorThread):
    """Classe herda de QueueProcessorThread para implementar as seguintes funcoes:
       Salvar log em arquivo, diferente da superclasse que apenas exibe log de erro no terminal
       Encerrar thread apos envio dos email que estao na fila
    """
    def run(self):
        #configuracao do log
        if not self.log.handlers:
            directory = asyncmail_config.DIRECTORY
            logger = directory + '/logger.info'
            file_log = logging.FileHandler(logger)
            file_log.setLevel(logging.INFO)
            formatter = logging.Formatter(fmt='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
            file_log.setFormatter(formatter)
            #seta arquivo de log e verbose
            self.log.addHandler(file_log)
            self.log.setLevel(logging.INFO)
        #chama run da superclasse especificando forever=False
        #para terminar thread apos execucao
        QueueProcessorThread.run(self,False)


def sendAsyncMail():
    maildir = configureMaildir()
    mailer = configureSMTPMailer()

    queue = queueProcess()
    queue.setMaildir(maildir)
    queue.setMailer(mailer)
    queue.start()


def configureMaildir():
    """Le diretorio do arquivo asyncmail_config.py
       Retorna objeto tipo sendmail.maildir.Maildir
    """
    if not asyncmail_config.DIRECTORY:
        asyncmail_config.DIRECTORY = '/tmp/digestmail'
    directory = asyncmail_config.DIRECTORY
    if not os.path.exists(directory):
    #Caso nao exista o diretorio especificado, parametro 'True' e passado por parametro
    #sendmail.maildir.Maildir cria diretorio e suas subpastas (cur, new, tmp)
        maildir = Maildir(directory,True)
    else:
    #Caso diretorio ja exista, sao criadas pastas cur,new,tmp.
    #Apos sendmail.maildir.Maildir recebe o diretorio pronto.
        if not os.path.exists(directory + '/cur'):
            os.makedirs(directory + '/cur')
        if not os.path.exists(directory +'/new'):
            os.makedirs(directory + '/new')
        if not os.path.exists(directory + '/tmp'):
            os.makedirs(directory + '/tmp')
        maildir = Maildir(directory)
    return maildir


def configureSMTPMailer():
    """Le de asyncmail_config.py configuracoes de SMTP,
       retorna objeto do tipo sendmail.mailer.SMTPMailer
    """
    hostname = asyncmail_config.HOSTNAME
    port = asyncmail_config.PORT
    username = asyncmail_config.USERNAME
    password = asyncmail_config.PASSWORD

    mailer = SMTPMailer(hostname=hostname, port=port, username=username,
                        password=password, no_tls=False, force_tls=True)
    return mailer


def createMessage(mail_from, list_mail_to, subject, message):
    """Cria email compativel com zope.sendmail
        mail_from recebe string com email de origem
            ex. 'plone@plone.com'
        list_mail_to recebe lista de strings com email dos destinatarios
            ex. ['plone@plone.com', 'plone@plone.com']
        subject recebe string com titulo do email
        message recebe string com corpo do email
    """
    subject = Header(subject, 'utf-8')
    email_message = 'Subject: %s\n%s\n'%(subject, message)
    mail_from = 'X-Zope-From: %s\n' %(mail_from)
    mail_to = 'X-Zope-To: '
    for email in list_mail_to:
        mail_to += '%s, '%(email)
    mail_to += '\n'

    mail_dir = configureMaildir()
    new_message = mail_dir.newMessage()
    new_message.writelines([mail_from, mail_to, email_message])
    new_message.commit()
