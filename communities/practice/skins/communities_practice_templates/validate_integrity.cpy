## Script (Python) "validate_integrity"
##title=Validate Integrity
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind state=state
##bind subpath=traverse_subpath
##parameters=
##

from Products.Archetypes import PloneMessageFactory as _
from Products.Archetypes.utils import addStatusMessage

request = context.REQUEST
errors = {}
errors = context.validate(REQUEST=request, errors=errors, data=1, metadata=0)

if errors:
    message = _(u'Please correct the indicated errors.')
    addStatusMessage(request, message, type='error')
    return state.set(status='failure', errors=errors)
else:
    message = _(u'Changes saved.')
    addStatusMessage(request, message)
    context_type = getattr(context, 'portal_type', None)
    if context_type: 
        if context_type in ['CoPDocument', 'CoPFile', 'CoPImage', 'CoPProfileDocument', 'CoPProfileFile', 'CoPProfileImage', 'CoPProfilePost']:
            portal = context.portal_url.getPortalObject()
            last_referer = portal.REQUEST.form['last_referer']
            last_referer_path = last_referer[last_referer.find(portal.id) - 1:]
            if portal.portal_catalog(path={'query': last_referer_path, 'depth': 0}, portal_type="CoPDashboard"):
                state.setNextAction('redirect_to:string:' + last_referer_path)
                return state.set(status='created')
        elif context_type == 'BFVisitActivity':
            url = context.absolute_url()[:context.absolute_url().rfind('/')]
            state.setNextAction('redirect_to:string:' + url)
            return state.set(status='created')
            
    return state.set(status='success')
