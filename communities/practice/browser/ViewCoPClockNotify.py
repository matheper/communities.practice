# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from Acquisition import aq_parent, aq_inner

from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail_config import USERNAME
from communities.practice.generics.generics import *
from datetime import datetime
from DateTime import DateTime

"""
Para ativar as novidades de forma automatica e necessario adicionar
um 'clock-server' ao buidout.cfg do plone conforme exemplo abaixo.

[instance]
zope-conf-additional =
        <clock-server>
           method /Plone-ComunidadePratica/viewCoPClockNotify
           period 84600
           user administrador
           password 12345
           host 192.168.0.27:8080
        </clock-server>

"""
class ViewCoPClockNotify(BrowserView):
    """Default view para notificacoes.
    """

    def sendMail(self):
        """
            Busca pelas comunidades do portal que sao publicas ou restritas
            e envia mensagem com as atualizações para todos seus participantes.
        """

        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        self.portalPath = '/'.join(self.portal.getPhysicalPath())
        self.catalog = getToolByName(self.portal, 'portal_catalog')
        self.portal_membership = getToolByName(self.context, 'portal_membership')

        comunidades = self.catalog( portal_type = "CoP",
                                    path=self.portalPath,
                                    review_state = ['restrito', 'publico']
                                    )

        for comunidade in comunidades:
            objComunidade = comunidade.getObject()
            assunto = "Novidades %s"%(objComunidade.Title())
            mensagem = self.getHTMLMessage(objComunidade)
            if mensagem:
                mailTo = self.getListaEmail(objComunidade)
                mailFrom = USERNAME
                if mailTo:
                    createMessage(mailFrom, mailTo,assunto,mensagem)
        sendAsyncMail()
        return 0

    def getListaEmail(self, comunidade):
        """ Busca arquivo de configuracao que contem email dos usuarios que recebem novidades.
            Faz interseccao desses emails com os emails dos participantes da comunidade.
            (Remove email de usuarios que deixam de participar da comunidade)
        """
        emails_usuarios = ""
        txt_usuarios = getUserMailList(comunidade)
        if txt_usuarios:
            emails_usuarios = txt_usuarios.strip()
            emails_usuarios = emails_usuarios.split("\n")
        return emails_usuarios

    def getHTMLMessage(self, comunidade):
        """Cria mensagem em html com as novidades das comunidades de pratica."""
        conteudo = False
        portal_url = self.context.portal_url()
        date = datetime.now()
        nomeComunidade = encodeUTF8(comunidade.Title())
        path = '/'.join(comunidade.getPhysicalPath())
        novidades = []
        imgPortal = '%s/logo.png'%(self.portal.absolute_url())
        comunidade_url = comunidade.absolute_url()
        mensagem = 'Content-Type: text/html; charset="UTF-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\n\n'
        mensagem += """
                    <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    </head>
                    <body style="background-color: fff;">
                    <div style="background-color:rgb(242, 242, 242); border-radius: 0.85em; padding: 20px 20px 40px 20px;">
                    <div width="98%%" border="0" cellspacing="0" style="border:thin solid #d4d4d4;border-radius:0.85em;background-color:#fff;padding: 5px 5px 5px 5px">
                        <table width="100%%" border="0" cellspacing="0" style="font-family:serif;font-size:0.7em;background-color:#fff;padding: 5px 5px 5px 5px; border-bottom:thin solid  #d4d4d4">
                        <tbody>
                            <tr>
                                <td style="width:100px">
                                    <a href="%s">
                                        <img style="float:right;width:auto;min-height:auto;max-width:100px;max-height:100px" alt="LOGO PORTAL" src="%s">
                                    </a>
                                </td>
                                <td style="font-size:1.6em; font-family: serif; font-weight:bold; padding-left: 12px;">
                                    <span class="il">%s - %s/%s/%s</span>
                                </td>
                            </tr>
                        </tbody>
                        </table>
                        <table width="100%%" border="0" cellspacing="0" style="font-family:serif;font-size:0.7em;background-color:#fff;padding: 5px 5px 5px 5px">
                        <tbody>
                        """%(portal_url, imgPortal, nomeComunidade, date.day - 1, date.month, date.year)

        novidades = self.getNovidades(comunidade)
        tipos = ["Eventos","Documentos","Forum"]
        for tipo in tipos:
            if novidades[tipo]:
                conteudo = True
                mensagem+=  """ <tr>
                                    <td style="padding:2px 80px; " colspan="3">
                                        <br/><span style="font-size:16px; font-weight:bold;">%s</span>
                            """%(tipo)

                for novidade in novidades[tipo]:
                    titulo = novidade['titulo']
                    criador = novidade['criador']
                    descricao = novidade['descricao']
                    url = novidade['url']
                    if descricao:
                        mensagem += """
                                    <div style="line-height:1.4em;padding-left:16px;padding-top:6px;border-left:2px solid;">
                                        <span style="font-size:16px;color:#333">
                                        <a style="color:#15c" href="%s" target="_blank">%s</a>
                                        </span><br>
                                        <span style="font-family: serif;font-size:15px;line-height:100%%;"> %s </span>
                                        <span style="font-size:11px;color:#666">por %s</span><br><br>
                                    </div>"""%(url, titulo, descricao, criador) 
                    else:
                        mensagem += """
                                    <div style="line-height:1.4em;padding-left:16px;padding-top:6px;border-left:2px solid;">
                                        <span style="font-size:16px;color:#333">
                                        <a style="color:#15c" href="%s" target="_blank">%s</a>
                                        </span><br>
                                        <span style="font-size:11px;color:#666">por %s</span><br><br>
                                    </div>"""%(url, titulo, criador) 
                mensagem += """</td></tr>"""

        comentarios = self.getComentarios(comunidade)
        if comentarios:
            conteudo = True
            mensagem+=  """ <tr>
                                <td style="padding:2px 80px; " colspan="3">
                                    <br/>
                                    <span style="font-size:16px; font-weight:bold;">%s</span>
                        """%("Comentários")
            mensagem += comentarios
            mensagem += """</td></tr>"""


        mensagem+="""<tr>
                        <td colspan="3" height="50px"></td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    <span style="font-size: 11px; color: #666">
                        Gerencie a maneira como recebe suas notificações em:
                        <a href="%s/configuracoes">%s/configuracoes</a>
                    </span>
                    </div>
                    </body>
                    </html>
                    """%(comunidade_url,comunidade_url)
        if not conteudo:
            mensagem = ''
        return mensagem

    def getNovidades(self, comunidade, periodo=1):
        """ Retorna um dicionario com Tipo de novidade como chave e uma lista de dicionarios
            com as chaves 'titulo', 'criador', 'descricao' e 'url'.
            Ex.:
            {
            'Forum':[{'titulo':'Forum1','criador':'criador1','descricao':'descricao1','url':'url1'},
                    {'titulo':'Forum2','criador':'criador2','descricao':'descricao2','url':'url2'},
                    {...}
                    ],
            'Documentos':[...]
            }
        """
        cop_depth = getSubCoPDepth(comunidade)
        path = '/'.join(comunidade.getPhysicalPath())
        novidades = {}
        date = datetime.now()
        date = DateTime("%s/%s/%s" % (date.year,date.month,date.day))
        start = date - periodo
        end = date
        tipos = ["Eventos","Documentos","Forum"]
        novidades[tipos[0]]= self.catalog(
            path=path, review_state = ['publico','restrito', 'compartilhado_na_arvore'],
            created = { 'query': [start,end], 'range':'min:max'},
            portal_type='CoPEvent', subCoPDepth=cop_depth,
            )
        novidades[tipos[1]]= self.catalog(
            path=path, review_state = ['publico','restrito', 'compartilhado_na_arvore'],
            created = { 'query': [start,end], 'range':'min:max'},
            portal_type=['CoPFile','CoPImage','CoPLink','CoPShare','CoPDocument'],
            subCoPDepth=cop_depth
            )
        novidades[tipos[2]]= self.catalog(
                path=path, review_state = ['publico','restrito', 'compartilhado_na_arvore'],
                created = { 'query': [start,end], 'range':'min:max'},
                portal_type=['PloneboardForum', 'PloneboardConversation'],
                subCoPDepth=cop_depth
                )
        dicNovidades={}
        for tipo in tipos:
            dicNovidades[tipo]=[]
            for item in novidades[tipo]:
                title = encodeUTF8(item.Title)
                if not title:
                    title = item.id
                descricao = encodeUTF8(item.Description)
                criador = item.Creator
                url = item.getURL()
                user = getMemberData(self.context, item.Creator)
                if user:
                    criador = user[0]
                criador = encodeUTF8(criador)
                title = encodeUTF8(title)
                descricao = encodeUTF8(descricao)
                url = encodeUTF8(url)
                if tipo=="Documentos":
                    url+="/view"
                dicNovidades[tipo].append({ "titulo": title,
                                            "criador": criador,
                                            "descricao": descricao,
                                            "url": url,
                                          })
        return dicNovidades

    def getComentarios(self, comunidade, periodo=1):
        """
            Busca por comentarios na comunidade.
            Retorna mensagem hmtl.
        """
        cop_depth = getSubCoPDepth(comunidade)
        path = '/'.join(comunidade.getPhysicalPath())
        novidades = []
        date = datetime.now()
        date = DateTime("%s/%s/%s" % (date.year,date.month,date.day))
        start = date - periodo
        end = date
        portal_types = [
            'CoPEvent', 'CoPFile','CoPImage', 'CoPLink',
            'CoPShare', 'CoPDocument', 'PloneboardConversation',
            ]

        novidades = self.catalog(
            path = path, review_state = ['publico', 'restrito', 'compartilhado_na_arvore'],
            portal_type = portal_types, subCoPDepth = cop_depth,
            sort_on = 'copLastModification',
            sort_order = 'reverse',
            )

        mensagem = ""
        dicNovidades = {}
        for novidade in novidades:
            path = novidade.getPath()
            url = novidade.getURL()
            if novidade.portal_type in ['CoPDocument', 'CoPImage']:
                url = "%s/view"%(novidade.getURL())
            url = encodeUTF8(url)
            title = novidade.Title if novidade.Title else novidade.id
            title = encodeUTF8(title)

            comentarios = self.catalog(
                path=path, review_state = ['active','published', 'publico', 'restrito'],
                created = { 'query': [start,end], 'range':'min:max'},
                portal_type=['Discussion Item','PloneboardComment']
                )

            if comentarios:
                dicNovidades[title] = []
                mensagem += """
                    <div style="line-height:1.4em;padding-left:16px;padding-top:6px;border-left:2px solid;">
                    <span style="font-size:16px;color:#333">
                        <a style="color:#15c" href="%s" target="_blank">%s
                        </a>
                    </span>
                    <br/>
                    """%(url, title) 

            for item in comentarios:
                descricao = encodeUTF8(item.Description)
                criador = item.Creator
                url = item.getURL()
		url = url.replace('++conversation++default/','view#')
                user = getMemberData(self.context, item.Creator)
                if user:
                    criador = user[0]
                criador = encodeUTF8(criador)
                descricao = encodeUTF8(descricao)
                url = encodeUTF8(url)
                mensagem += """
                            <div style="line-height:1em;padding-left:16px;padding-top:6px;border-left:1px solid #d4d4d4;">
                                <span style="font-family: serif;font-size:15px;line-height:100%%;">
                                    %s
                                    <a style="color:#15c" href="%s" target="_blank">
                                        ver
                                    </a>
                                </span>
                                <br/>
                                <span style="font-size:11px;color:#666">
                                    por %s
                                </span>
                                <br><br>
                            </div>"""%(descricao, url, criador)
            if comentarios:
                mensagem += """</div>"""
        return mensagem
