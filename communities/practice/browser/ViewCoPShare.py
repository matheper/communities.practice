from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPShare(ViewCoPBase):
    """Default View for CoPShare"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPShare, self).__call__()
