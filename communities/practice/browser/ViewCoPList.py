from Products.CMFCore.utils import getToolByName
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from zope.component.hooks import getSite

class ViewCoPList(ViewCoPBase):
    """ Default list CoP View used into ViewCoPCommunityFolder and ViewCoPSubCoP
    """

    def get_cop_list(self):
        """ Retorna as comunidades/subcomunidades
            do primeiro nivel do contexto."""
        portal = getSite()
        portal_membership = getToolByName(portal, 'portal_membership')
        portal_workflow = getToolByName(portal, 'portal_workflow')
        authenticated_member = portal_membership.getAuthenticatedMember()
        authenticated_member_id = authenticated_member.getId()
        path = '/'.join(self.context.getPhysicalPath())
        if self.context.portal_type == 'CoP':
            path = "%s/subcop"%(path)
        query = {}
        query['path'] = {'query': path, 'depth': 1}
        query['portal_type'] = "CoP"
        query['sort_on'] = 'Date'
        query['sort_order'] = 'reverse'

        catalog = getToolByName(self.context, 'portal_catalog')
        communities_brain = catalog(query)
        communities_dict = []
        member_communities = listMemberRoleCommunitiesParticipating(authenticated_member_id)
        communities_participating = member_communities.get(authenticated_member_id)
        super_cop_participation = True
        if communities_participating:
            super_cop_role = communities_participating.get(self.context.UID())
            if self.context.portal_type == 'CoP' and not super_cop_role in ['Owner', 'Moderador', 'Participante', 'Observador']:
                super_cop_participation = False

        for community in communities_brain:
            participar = ""
            if community.acceptsShares and super_cop_participation:
                participar = "%s/viewCoPJoin"%(community.getURL())
            user_id = community.Creator
            user_name, email = getMemberData(portal, user_id)
            user_url = "%s/author/%s"%(portal.absolute_url(), user_id)
            role = ""
            if communities_participating:
                role = communities_participating.get(community.UID)

            participant_role = "Autenticado"
            if role == "Owner":
                participant_role = "Owner"
            elif role in ["Participante", "Moderador", "Observador"]:
                participant_role = "Participante"
            elif role == "Bloqueado":
                participant_role = "Bloqueado"
            elif "Aguardando" == role:
                participant_role = "Aguardando"

            review_state = community.review_state
            workflow_id = portal_workflow.getChainFor(community.portal_type)[0]
            workflow = portal_workflow.getWorkflowById(workflow_id)
            state = workflow.states.get(review_state)
            if state:
                review_state = state.title

            cop_local_roles = getCoPLocalRoles(community)
            cop_local_roles = cop_local_roles.get(community.UID)
            members_number = len(cop_local_roles['Participante']) + len(cop_local_roles['Moderador']) + len(cop_local_roles['Owner'])

            communities_dict.append({
                "titulo": community.Title,
                "id": community.id,
                "descricao": community.Description,
                "url": community.getURL(),
                "nome_criador": user_name or user_id,
                "id_criador": user_id,
                "url_criador": user_url,
                "url_imagem": community.imageCoP,
                "estado": review_state,
                "papel_participante": participant_role,
                "participar": participar,
                "num_participantes": members_number,
                "num_subcop": community.subCoPNumber,
            })

        return communities_dict
