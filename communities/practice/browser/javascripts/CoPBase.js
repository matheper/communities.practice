var slideTime = 0;

jq(document).ready(function(){
   jq("#cop_content").css("margin-left", jq("#cop_menu").width() + 5);
   jq("#cop_footer").css("margin-left", jq("#cop_menu").width() + 33);
   jq("#cop_footer_Rename, #cop_share_email,  #cop_add_conversation_forum").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
   if(jq("#cop_action_Delete").hasClass("cop_action_enabled") ){
         jq("#cop_action_Delete").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
   }
   jq(".cop_listing_join_overlay").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
   jq("#cop_footer_print").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
   jq("#cop_invitation").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
   jq(".cop_prep_overlay").prepOverlay({ subtype: 'ajax', filter: common_content_filter } );

    showRefusedMessage()
    jq('#cop_part_pendentes input[type=radio]').click(function(event){
        showRefusedMessage();
    });

    //CoP Tutors
    if( jq("#coptutors-base-edit").length > 0){
       initializeCoPTutorsForm(); 
    }else if(jq("#CoPTutors").length > 0){
       document.addEventListener("CoPTutorsFormLoaded", initializeCoPTutorsForm); 
    }

    //Carrega ViewCoPList em div com class cop_communities_list
    jq(".cop_communities_list").load("viewCoPList #content", function() {
        jq(".cop_listing_join_overlay").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
    });

    //Carrega ViewCoPFolderContents em div com class cop_folder_contents
    jq(".cop_folder_contents").load("viewCoPFolderContents .cop_news_page");
    //Recarrega tab_files ao ordenar tabela
    jq(document).delegate(".cop_folder_contents .file_header a", "click", function(e){
        e.preventDefault();
        var th_index = jq(this).parent().index();
        var has_arrow_down = jq(this).children("span").hasClass("arrow_down");
        jq(".cop_folder_contents").load(jq(this).attr("href")+" .cop_news_page", function() {
            jq(".file_header span").removeClass("arrow_down").removeClass("arrow_up");
            if ( has_arrow_down ) {
                jq(jq(".file_header")[th_index]).find("span").first().addClass("arrow_up");
            }
            else {
                jq(jq(".file_header")[th_index]).find("span").first().addClass("arrow_down");
            }
        });
        return false;
    });
    //Recarrega tab_files ao trocar de pagina, sem alterar url.
    jq(document).delegate(".cop_folder_contents .cop_file_pagination_static a", "click", function(e){
        e.preventDefault();
        var arrow_down_index = jq(jq("#file_table .arrow_down")[0]).parent().parent().index()
        var arrow_up_index = jq(jq("#file_table .arrow_up")[0]).parent().parent().index()
        jq(".cop_folder_contents").load(jq(this).attr("href")+" .cop_news_page", function() {
            jq("#file_table .file_header span").removeClass("arrow_down").removeClass("arrow_up");
            jq(jq("#file_table .file_header")[arrow_down_index]).find("span").first().addClass("arrow_down");
            jq(jq("#file_table .file_header")[arrow_up_index]).find("span").first().addClass("arrow_up");
            jq('html, body').animate({
                scrollTop: jq(".cop_news_page").offset().top
            }, 200);
        });
        return false;
    });
    //Recarrega news ao trocar de pagina, sem alterar url.
    jq(document).delegate(".cop_pagination_static_url a", "click", function(e){
        e.preventDefault();
        jq(jq(this).closest(".cop_tabular_sub")).load(jq(this).attr("href")+" .cop_news_page", function() {
            jq('html, body').animate({
                scrollTop: jq(".cop_news_page").offset().top
            }, 200);
        });
        return false;
    });

    //Recarrega news ao trocar de pagina alterando url e mantendo historico
    //Utilizado nas comunidades e no dashboard
    jq(".cop_news_page .cop_pagination_transition a").live("click", function(e){
        e.preventDefault()
        window.history.pushState("", "", jq(this).attr("href"));
        jq("<div/>").load(jq(this).attr("href") + " .cop_news_page", function(){
            page = jq(this);
            jq(".cop_news_container .cop_news_page").replaceWith(page.find(".cop_news_page"));
            jq('html, body').animate({
                scrollTop: jq(".cop_news_container").offset().top
                }, 200);
        });
        return 0;
    });

    //Carrega following form na ViewCoPMembers
    jq(".cop_members_button").each(function(){
        jq(this).load("viewCoPFollowingButton/?member_id="+jq(this).attr("id").split("member_")[1], function(){
                following = jq(this).find("input[name='transition']").first().attr("value");
                setFollowingClass(following);
            });
    });

    //Carrega following form na ViewCoPMembers
    jq(document).delegate(".cop_members_button input[type='submit']", "click", function(e){
        e.preventDefault();
        jq(this).attr("disabled", "disabled");
        form = jq(this).closest("form");
        str = form.serialize();

        jq.post("viewCoPFollowingButton",
                str,
                function(data){
                    form.replaceWith( jq(data).find("form").first());
                    following = jq(data).find("input[name='transition']").first().attr("value");
                    setFollowingClass(following);
                })
    });
    //
    
    function setFollowingClass(following){
        if (following == "unfollow"){
            jq(".wrapper").addClass("following");
        }
        if (following == "follow"){
            jq(".wrapper").removeClass("following");
        }
    }

    //Tab icons:
    jq(".cop_tabular_container .cop_tabular_abas a").each(function(){
        text = jq(this).text();
        text = text.replace(/{(.*)}/g,
        "<img src='++resource++communities.practice.images/$1' />") 
        jq(this).html(text);
    });
 
    //On Tab Click:
    jq(".cop_tabular_container .cop_tabular_abas ul li a").click(function(e, params){
        e.preventDefault();
        if( jq(this).parents(".cop_portlet").length == 0 &&  params == undefined)
            window.location.hash = "tab_" + jq(this).attr("href").split("#/")[1];
        var item_id = "#" + jq(this).attr("name");
        jq(this).parents(".cop_tabular_container").find(".cop_tabular_abas ul li").removeClass("cop_selected");
        jq(this).parents("li").addClass("cop_selected");
        jq(this).parents(".cop_tabular_container").find(".cop_tabular_sub").hide();
        jq(this).parents(".cop_tabular_container").find(item_id).show();

        return 0;
    });

    //Simula clique na primeira aba logo que abre a página
    jq(".cop_tabular_container").each(function(){
        tab = jq(this).find(".cop_tabular_abas a[name='"+window.location.hash.replace("tab_","").split("#")[1]+"']");
        if(window.location.hash == "" || tab.length == 0){
           jq(this).find(".cop_tabular_abas ul li a").first().trigger("click", [true]);
        }else{
           tab.trigger("click");
        }
    });

    //Description:
    jq(".cop_wall_description .cop_wall_button").click(function(e){
        var image = jq(this);
        image.siblings().slideToggle(slideTime, function(){
            if( jq(this).is(":hidden")) {
                image.attr("src", base_url + "/++resource++communities.practice.images/wall_expand.png");
            }else{
                image.attr("src", base_url + "/++resource++communities.practice.images/wall_collapse.png");
            }
        });
    });

    /*Notificações*/
    jq('#selectAll').click(function() {
        if(this.checked == true){
            jq("input[type=checkbox]").each(function() { 
                this.checked = true; 
            });
        } else {
            jq("input[type=checkbox]").each(function() { 
                this.checked = false; 
            });
        }
    }); 
    jq('#submit_send_mail_notificacao').click(function(e){
        teste = false
        jq("input[type=checkbox]").each(function() { 
            if(this.checked == true)
                teste = true
        })
        
        if(jq('#assunto').val().length == 0 || jq('#mensagem').val().length == 0 || teste == false){
            jq('#retorno').text("Preencha todos os campos.")
            jq('#retorno').addClass("textoAlerta");
            e.preventDefault()
        }
    })


    /*Atividades*/
    jq(".cop_container_tabelas a").click(function(e){
        clicked_user = $(this).attr("user");
        clicked_type = $(this).attr("type");
        clicked_folder = $(this).attr("folder");
        clicked_period_start = $(this).attr("period_start");
        clicked_period_end = $(this).attr("period_end");
    }); 
    jq(".cop_link_ativo a").prepOverlay({
        subtype: 'ajax',
        filter: common_content_filter,
        urlmatch: $,
        urlreplace: '/viewCoPConteudoAtividade',
        config: {
                onLoad : function (e) {
                    $(".pb-ajax").load( "viewCoPConteudoAtividade",
                                        {user: clicked_user, 
                                         type: clicked_type, 
                                         folder: clicked_folder,
                                         period_start: clicked_period_start,
                                         period_end: clicked_period_end});
                    }   
                }   
    });

    try { 
        jq(".cop_menu_selected").prepend("<img src='" + base_url + "/++resource++communities.practice.images/menu_indicador.png' class='cop_menu_indicador' />");
    } catch (e) {
        console.warn(e);
    }
    //Simular clique no botão de descrição caso o usuário participe da comunidade
    jq(".cop_wall_description #cop_hidden").click();
    slideTime = 200;
    //
    //jq("#portal-breadcrumbs").append( jq("#cop_workflow" ));

    copShowLoaderIcon(jq(".cop_tabular_sub#cop_participantes"))
    jq(".cop_tabular_sub#cop_participantes").load("viewCoPConfigManageUsers .cop_config_manage_users", function(){
        copHideLoaderIcon(jq(".cop_tabular_sub#cop_participantes"))
    });


});

/*Tabs Loading*/
function copShowLoaderIcon(caller){
    caller.parents(".cop_tabular_container").find(".cop_ajax_loader").show();
}

function copHideLoaderIcon(caller){
    caller.parents(".cop_tabular_container").find(".cop_ajax_loader").hide();
}


function copLoadForm(caller) {
    callerObject = jq(caller);
    callerObject.unbind('onfocus');
    var callerId = callerObject.attr("id");
    link = callerObject.attr("href");
    container = callerObject.parents("#"+callerObject.attr("id")+" fieldset");
    copShowLoaderIcon(callerObject);
    //remove Tabbing from already shown forms
    container.parents(".cop_tabular_contents").find("form").removeClass("enableFormTabbing");
    jq("<div />").load(link + " #content", function(){
        //evt = new CustomEvent("formLoaded", {detail: {id:callerId}, bubbles:false, cancelable:false});
        //document.dispatchEvent(evt);
        jq(this).find("#content .documentFirstHeading").hide();
        text = callerObject.attr("value");
        form = jq(this);
        copHideLoaderIcon(callerObject);
        container.replaceWith(form);
        form.find("#title").val(text);
        ploneFormTabbing.initialize();

        //Load TinyMCE on RichEdit fields
        jq(".mce_editable").each(function(){
            id = jq(this).attr("id");
            if(id){
                InitializedTinyMCEInstances[id] = undefined;
                var textBox = new TinyMCEConfig(id);
                textBox.init();
            }
        });

        switch(callerId){
            case("DABExperience"):
                document.dispatchEvent( new CustomEvent("DABExperienceFormLoaded") );
                break;
            case("CoPTutors"):
                document.dispatchEvent( new CustomEvent("CoPTutorsFormLoaded") );
                break;
        }

        //Load reference field pop-up
        jq('.addreference').overlay({
             onBeforeLoad: function() {
                 ov = jq('div#content').data('overlay');
                 // close overlay, if there is one already
                 // we only allow one referencebrowser per time
                 if (ov) {ov.close(); }
                 var wrap = this.getOverlay().find('.overlaycontent');
                 var src = this.getTrigger().attr('src');
                 var srcfilter = src + ' >*';
                 wrap.data('srcfilter', srcfilter);
                 jq('div#content').data('overlay', this);
                 resetHistory();
                 wrap.load(srcfilter, function() {
                     var fieldname = wrap.find('input[name=fieldName]').attr('value');
                     check_referenced_items(fieldname);
                     });
                 },
             onLoad: function() {
                 widget_id = this.getTrigger().attr('rel').substring(6);
                 disablecurrentrelations(widget_id);
             }});
        $('.plone-jscalendar-popup').each(function() {
            var jqt = $(this),
            widget_id = this.id.replace('_popup', ''),
            year_start = $('#' + widget_id + '_yearStart').val(),
            year_end = $('#' + widget_id + '_yearEnd').val();
            if (year_start && year_end) {
                jqt.css('cursor', 'pointer')
                .show()
                .click(function(e) {
                return plone.jscalendar.show('#' + widget_id, year_start, year_end);
                });
            }
        });
    });
}

function showActions(popup) {
        jq(".cop_action_menu").not(popup).find(".cop_action_popup").hide("fast");
        jq(popup).find(".cop_action_popup").not(":animated").toggle("fast");
}

function showRefusedMessage(){
    //Exibição do textarea para envio de mensagem a usuarios que tiveram a participação negada
    if(jq("input[value=negar]:checked").length){
        jq(".refused_message").show();
    }
    else{
        jq(".refused_message").hide();
    }
}

jq('html').click(function(event){
    jq(".cop_action_popup").hide("fast");
});

/*CoP Tutors Form JS */
function initializeCoPTutorsForm() {
    jq.getScript( "++resource++communities.practice.javascripts/jquery.maskedinput-ben.js", function(data, textStatus, jqxhr ){
        translateCoPTutorsFormFields();
        maskDateFields();
        jq(document).delegate("#datagridwidget-add-button, .datagridwidget-manipulator img[title=Adicionar linha]", "click", function(){
         setTimeout(function(){maskDateFields(); translateCoPTutorsFormFields();}, 200);
        });
      });
}
function translateCoPTutorsFormFields(){
    jq("#datagridwidget-add-button").text("Adicionar nova linha");
    jq(".datagridwidget-manipulator img").each(function(){
      switch( jq(this).attr("title") ) {
       case "Add row":
       jq(this).attr("title", "Adicionar linha");
       break;
       case "Delete row":
       jq(this).attr("title", "Remover linha");
       break;
       case "Move row up":
       jq(this).attr("title", "Mover linha para cima");
       break;
       case "Move row down":
       jq(this).attr("title", "Mover linha para baixo");
       break;
      }
    });
}
function maskDateFields(){
jq("[name=Tutores.data_inicio:records], [name=Tutores.data_fim:records]").mask(
         '99/99/9999',
         { validate: function (fld,cur) {
               // 0 == day; 1 == month; 2 == year
               var dd = parseInt(fld[0]),
                   mm = parseInt(fld[1]),
                   yy = parseInt(fld[2]),
                   vl = true;

               if (!(mm >= 0 && mm < 13) && cur == 1) {
                  fld[1] = '12';
                  vl = false;
               }

               if (!(dd >= 0 && dd < 31) && cur == 0) {
                  fld[0] = '01';
                  vl = false;
               }

               if (!(yy >= 1976 && yy < 2199) && cur == 2 && fld[2].replace('_','').length == 4) {
                  fld[2] = '2014';
                  vl = false;
               }

               return vl;
            }
        });
}

/*Code snippet for placeholders under older browsers *cough* ie *cough*
/*source: http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html */
jq('[placeholder]').focus(function() {
  var input = $(this);
  if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
  }
}).blur(function() {
  var input = $(this);
  if (input.val() == '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));
  }
}).blur();
jq('[placeholder]').parents('form').submit(function() {
  jq(this).find('[placeholder]').each(function() {
    var input = jq(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
    }
  })
});
/*End snippet */
