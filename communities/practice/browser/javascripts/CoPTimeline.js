jq(document).ready(function(){
    //Carrega prepOverlay para actions da timeline
    jq(document).delegate(".cop_timeline_popaction", "click", function(e){
        e.preventDefault();
        if(jq(this).hasClass("cop_timeline_action_comment")) return;
        if( jq(this).find("div").length <= 0){
            jq(".cop_timeline_popaction div").hide(200).delay(200).remove();
            jq(this).append("<div class='cop_timeline_popup'></div>").find("div").show(200);
            var container = jq(this).find("div");
            jq("<div/>").load( jq(this).attr("href"), function(){
                jq(this).find("div").first().contents().appendTo(container);
            } ); 
        }
        return 0;
    });

    jq(document).delegate(".cop_timeline_action_comment", "click", function(e){
        e.preventDefault();
        if( jq(this).find("div").length <= 0){
            jq(".cop_timeline_popaction div").hide(200).delay(200).remove();
            jq(this).append("<div class='cop_comment_popup cop_timeline_popup''></div>").find("div").show(200);
            var container = jq(this).find("div");
            jq("<div/>").load(jq(this).attr("href") + ' #commenting', function(){
                jq(this).find("div").first().contents().appendTo(container);
            } ); 
        }
        return 0;
    });

    jq(document).delegate(".cop_timeline_popaction select", "change", function(e){
        val = jq(this).val();
        jq(this).closest(".cop_timeline_popaction").find(".cop_timeline_popaction_searchresults").each(function(){
            if( jq(this).attr("id") == val)
                jq(this).addClass("active");
            else
                jq(this).removeClass("active");
        });
    });

    jq(document).delegate(".cop_timeline_popaction_searchresults span", "click", function(e){
        jq(this).toggleClass("selected");

        jq(this).closest(".cop_timeline_popaction").find(".cop_timeline_popaction_footer").first().show();
    });

    jq(document).delegate(".cop_timeline_popaction .search", "keyup", function(e){
        if( jq(this).val().length <= 0 ) {
            jq(this).closest(".cop_timeline_popaction").find("span").show();
        }else{
            var text = jq(this).val().toLowerCase();
            jq(this).closest(".cop_timeline_popaction").find(".cop_timeline_popaction_searchresults span").each(function(){
                ind = jq(this).text().toLowerCase().indexOf(text);
                if( ind  >= 0 || jq(this).hasClass("selected")){
                    jq(this).show();
                }else{
                    jq(this).hide();
                }
            });
        }
    });

    jq(document).delegate(".cop_timeline_popaction form[name='edit_form'] [name='form.button.save']", "click", function(e){
        e.preventDefault();
        var form = jq(this).closest("form");
        var action_url = form.attr("action");
        jq(this).hide();

        form.attr( "enctype", "multipart/form-data" ).attr( "encoding", "multipart/form-data" );
        str = form.serialize();
        pop_parent = jq(this).closest(".cop_timeline_popaction").find("div").first();
        jq.post(action_url,
                str,
                function(data){
                  pop_parent.hide(200, function(){ pop_parent.remove()});
                })
                
        return false;
    });

    jq(document).delegate(".cop_timeline_popaction form[name='edit_form'] [name='form.button.cancel']", "click", function(e){
        e.preventDefault();
        pop_parent = jq(this).closest(".cop_timeline_popaction").find("div").first();
        pop_parent.hide(200, function(){ pop_parent.remove()});
    });

    jq(document).delegate(".cop_timeline_action_comment fieldset #form-buttons-comment", "click", function(e){
        e.preventDefault();
        var container = jq(this).closest('.cop_timeline_news_item').find('.cop_timeline_commentaries')
        jq(this).attr("disabled", "disabled");
        jq(jq(this).closest('.formControls')).hide();
        form = jq(this).closest("form");
        array = jq(form[0]).serializeArray();
        params = {'form.buttons.comment' : "Comment"};
        jq(array).each(function(){ params[this.name] = this.value })
        jq.post(form[0].action,
                params,
                function(data){
                    jq(".cop_timeline_popaction div").hide(200).delay(200).remove();
                    content_url = e.currentTarget.href + 'CoPComment';
                    jq('<div/>').load(content_url + ' .cop_timeline_commentaries', function(){
                            container.replaceWith(jq(this));
                        })
                }) 
    }); 

    jq(document).delegate(".cop_timeline_action_comment fieldset #form-buttons-cancel", "click", function(e){
        e.preventDefault();
        e.stopPropagation();
        jq(this).attr("disabled", "disabled");
        jq(jq(this).closest('.formControls')).hide();
        jq(".cop_timeline_popaction div").hide(200).delay(200).remove();
    }); 

    jq('html').click(function(event){
        if(jq(event.target).closest(".cop_timeline_popup").length <= 0){
            jq(".cop_timeline_popaction div").hide(200, function(){
                this.remove();
                return 0;
            });
        }
    });

});

