jq(document).ready(function(){
    jq("#content").width(622);
    jq(document).delegate(".cop_sub_source a", "click", function(e){
            e.preventDefault();
            return 0;
        })
    jq(document).delegate(".sub_cop_more", "click", function(e){
        jq(this).closest(".sub_cop_commmunity").find(".sub_cop_container").toggleClass("sub_cop_hide");
        jq(this).closest(".sub_cop_commmunity").find(".sub_cop_container").toggleClass("sub_cop_show");              
        
        if (jq(this).closest(".sub_cop_commmunity").find(".sub_cop_container").find("#content").length == 0) {
            jq(this).closest(".sub_cop_commmunity").find(".sub_cop_container").load(
                jq(this).find(".cop_sub_source a").attr('href') + " #content", function(){
                    if(! jq(this).closest("#content").hasClass("sub_cop_odd")){
                        jq(this).find("#content").first().addClass("sub_cop_odd");
                    }
                    jq(".cop_listing_join_overlay").prepOverlay( { subtype: 'ajax', filter: common_content_filter } );
                });
        } else {
            jq(this).closest(".sub_cop_commmunity").find(".sub_cop_container").toggle(100);
        }

        jq(this).find(".cop_seta").toggleClass("cop_seta_direita");
        jq(this).find(".cop_seta").toggleClass("cop_seta_baixo");
    });

});
