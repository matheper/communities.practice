jq(document).ready(function(){
    function docontentadd(response){
        var error = jq(response).find('.field.error');
        if (!(error.length)){
            location.reload();
        }
    }
    jq('.comment_button').prepOverlay({
        subtype: 'ajax',
        formselector:'form',
        filter:common_content_filter,
        closeselector:'#form-buttons-cancel',
        afterpost: function(response) {return docontentadd(response);}
    });

});
