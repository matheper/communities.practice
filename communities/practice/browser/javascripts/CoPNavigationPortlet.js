jq(document).ready(function(){
//Custom Page Styles
       jq(".CoPBox").click(function(e){
           e.preventDefault();
           jq(jq(this).attr("href")).toggle(200);
           return(0);
       });

       jq(".CoPBoxPortlet").click(function(e){
           e.preventDefault();
           jq(jq(this).attr("href")).toggle(200);
           return(0);
       });
       
       jq("a.CoPYoutube").each(function(){
           this.href = this.href.replace(new RegExp("watch\\?v=", "i"), 'v/')
       });
       
       jq("a.CoPYoutube").prepOverlay( {
           subtype: 'iframe'
       } );
       
       jq(".CoPPopup").hide();
       
       jq(".CoPPortletNavegacao > li ul").hide();
       jq(".CoPPortletNavegacao > li").click(function(){ jq(this).toggleClass("selected").find("ul").toggle(200) });
});
