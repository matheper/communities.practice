jq(document).ready(function(){
var originalWidth = jq(".cop_participante").width();
var originalHeight = jq(".cop_participante").height();

     //Twitter
     jQuery(function($){
         if( cop_twitter == undefined) return;
                var cop_twitter = cop_twitter.replace("#","");
                $(".cop_tweet").tweet({
                    query: cop_twitter,
                    join_text: "auto",
                    count: 2,
                    template: "{text}<br /><span class='cop_tweet_time'> {format_time} </>"
                });
     });
    //Facebook
    (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=302640433183450";
          fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    //YouTube
   $(window).load(function() {
       if( cop_youtube == undefined) return;
        $('#cop_videos').youTubeChannel({ 
            userName: cop_youtube
            ,hideNumberOfRatings: true
            ,loadingText: "Carregando"
            ,hideVideoLength: true
            ,hideViews: true
            ,hideRating: true
        });
    setTimeout(function(){ 
        jq("#channel_div a").each(function(){
            this.href = this.href.replace(new RegExp("watch\\?v=", "i"), 'v/')
        });
        jq("#channel_div a").prepOverlay( { subtype: 'iframe' } );
    }, 2000);
    }); 
    //====
    jq(".cop_portlet_search").keyup( function(){
        input = jq(this).val();
        if(input == ""){
            jq(".cop_participante").removeClass("glow");
            if(! jq('.cop_portlet_showall input[type="checkbox"]').is(":checked")){
                jq(".cop_participante").not(".dontshow").show(200);
                jq(".cop_participante.dontshow").hide();
            }else{
                jq(".cop_participante").show(200);
            }
        }else{
            jq(".cop_participante").each(function(){
                name = jq(this).attr("name");
                regexp = new RegExp(input, "gi");
                    if(name.search(regexp) >= 0){
                    jq(this).addClass("glow");
                    jq(this).show(200);
                }else{
                    jq(this).removeClass("glow");
                    jq(this).hide(200);
                }
            });
        }
    });

    jq('.cop_portlet_showall input[type="checkbox"]').mousedown(function() {
        if (jq(this).is(':checked')) {
            jq(".cop_participante").width(originalWidth);
            jq(".cop_participante").height(originalHeight);
            if (jq(".cop_portlet_search").first().val() != "") return;
            jq(".cop_participante.dontshow").css("display", "none");
        } else {
            jq(".cop_participante.dontshow").css("display", "block");
            jq(".cop_participante").width(originalWidth/2);
            jq(".cop_participante").height(originalHeight/2);
        }
    });


    jq(".sub_cop_container").not(".leaf").addClass("closed");
    jq(".sub_cop_current").parents(".sub_cop_container").removeClass("closed");

    jq(".cop_portlet .sub_cop_container").each(function(){
            if( jq(this).find(".sub_cop_container").length == 0) 
             jq(this).addClass("leaf");
    });

    jq(".cop_portlet .sub_cop_container").not(".leaf").click(function(e){
          e.stopPropagation();
          //jq(this).children("div").toggle();
          jq(this).toggleClass("closed");
    });

});
