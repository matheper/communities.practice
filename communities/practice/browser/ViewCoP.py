# -*- coding:utf8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getCoPUpdates, getBrainValues, getCommentaries
from communities.practice.generics.generics import getCoPContext, isDiscussionAllowed, getCoPRole
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.generics import isSubCoP

class ViewCoP(ViewCoPBase):
    """Default View for Communities of Practice"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_menu_status['inicio'] = 'cop_menu_selected'
        self.cop_title = self.context.Title()
        self.cop_description = self.context.Description()
        self.cop_participation = False
        cop_image = self.context.getImagem()
        if not cop_image:
            parent = getCoPContext(self.context, ['CoPCommunityFolder'])
            cop_image = parent.getImagem()
        self.cop_image = cop_image.absolute_url()
        self.user = self.context.portal_membership.getAuthenticatedMember()
        roles = self.user.getRolesInContext(self.context)
        authorized = ['Participante','Moderador','Owner','Manager']
        if [i for i in roles if i in authorized]:
            self.cop_participation = True
        return super(ViewCoP, self).__call__()

    def get_cop_updates(self, limit = 0):
        """
            Get CoP news to show on timeline.
        """
        return getCoPUpdates(self.context, limit)

    def get_brain_values(self, news):
        """
            Get values for a brain list.
        """
        return getBrainValues(self.context, news)

    def get_commentaries(self, path, qtd = 0):
        """
            Busca Comentarios a partir do path. Pode ser limitado atraves do parametro qtd.
            Retorna no formato lista de dicionarios.
        """
        self.see_more = False
        commentaries = getCommentaries(self.context, path)
        if qtd and len(commentaries) > qtd:
            self.see_more = True
            return commentaries[len(commentaries)-qtd:]
        return commentaries

    def is_discussion_allowed(self, cop_uid):
        return isDiscussionAllowed(self.context, cop_uid)

    def participation_allowed(self):
        super_cop_participation = True
        if isSubCoP(self.context):
            super_cop = getSuperCoP(self.context)
            portal_membership = getToolByName(self.context, 'portal_membership')
            authenticated_member_id = portal_membership.getAuthenticatedMember().getId()
            member_communities = listMemberRoleCommunitiesParticipating(authenticated_member_id)
            communities_participating = member_communities.get(authenticated_member_id)
            if not communities_participating or not communities_participating.get(super_cop.UID()) in ['Owner', 'Moderador', 'Participante']:
                super_cop_participation = False
        participar = ""
        if self.context.get_participar_habilitado() and super_cop_participation:
            participar = "%s/viewCoPJoin"%(self.context.absolute_url())
        return participar

    def get_participation_role(self):
        role = getCoPRole(self.context.UID(), self.user.getId())
        participant_role = "Autenticado"
        if "Owner" in role or "Participante" in role or "Moderador" in role or \
            "Moderador_Master" in role or "Bloqueado" in role:
            participant_role = "Participante"
        elif "Aguardando" in role:
            participant_role = "Aguardando"
        return participant_role
