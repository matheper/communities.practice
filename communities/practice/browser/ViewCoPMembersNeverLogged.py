# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from Acquisition import aq_parent
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.generics import *
from communities.practice.generics.asyncmail_config import USERNAME
from DateTime import DateTime

class ViewCoPMembersNeverLogged(ViewCoPBase):
    """Default view para relatorio de MembersNeverLogged.
    """

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        self.set_user_permissions()
        self.set_user_permissions()
        self.cop_menu_status = {"criar":"cop_menu_disabled"}
        self.cop_menu_titles = {}
        if self.cop_addable_types.has_key('CoP'):
            self.cop_menu_status = {"criar":"cop_menu_item"}
            self.cop_menu_links = {"criar":self.cop_addable_types['CoP']}

        #Manager e Owner
        authenticated_user = self.context.portal_membership.getAuthenticatedMember()
        role = authenticated_user.getRoles()
        local_role = getCoPRole(self.context.UID(), authenticated_user.getId())
     
        if 'Manager' in role or 'Owner' in local_role or 'Moderador_Master' in local_role:
            self.cop_menu_status['moderadores']="cop_menu_item"
            self.cop_menu_links['moderadores'] = self.context.absolute_url() + "/viewCoPModeradorMaster"
            self.cop_menu_status['nunca_logaram']="cop_menu_selected"
            self.cop_menu_links['nunca_logaram'] = self.context.absolute_url() + "/viewCoPMembersNeverLogged"
            self.cop_menu_status['atividade_folder']="cop_menu_item"
            self.cop_menu_links['atividade_folder'] = self.context.absolute_url() + "/viewCoPCommunityFolderAtividade"

        self.community_folder = aq_parent(aq_inner(self))
        self.communities = []
        self.my_communities = []
        return super(ViewCoPMembersNeverLogged, self).__call__()

    def send_mail_members(self):
        """Envia mensagem para os membros.
        """
        form = self.request.form
        if 'submit_send_mail_members' in form.keys():
            portal = self.context.portal_url.getPortalObject()
            moderador_nome = encodeUTF8(getMemberData(self.context, portal.portal_membership.getAuthenticatedMember().getId())[0])
            url = encodeUTF8(portal.absolute_url())

            assunto = encodeUTF8(form.get("assunto"))
            mensagem = ""
            if form.get("mensagem"):
                mensagem = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
                mensagem += encodeUTF8(form.get("mensagem"))
                mensagem += '\n\n\nLink para acesso ao portal:\n%s'%(url)
                mensagem += '\n\nMensagem enviada por: %s'%(moderador_nome)
            if assunto and mensagem:
                mail_to = []
                for user_id, valor in form.items():
                    if valor == 'Sim':
                        mail = getMemberData(self.context, user_id)[1]
                        if mail:
                            mail_to.append(mail)
                if mail_to:
                    mail_from = USERNAME
                    createMessage(mail_from, mail_to, assunto, mensagem)
                    sendAsyncMail()
                else:
                    return u"Selecione emails para envio."
                return u"Email enviado com sucesso."
            return u"Campos obrigatórios não preenchidos"
        return ""

    def get_members_never_logged(self):
        """Retorna quem nao se logou ainda
        """
        membership_tool = getToolByName(self, 'portal_membership')
        members = membership_tool.listMembers()
        mail_to = []
        for member in members:    
            if member.getProperty('last_login_time').equalTo(DateTime('2000/01/01 00:00:00 GMT-2')):
                mail_to.append({'id': member.getProperty('id',""),
                                'nome': member.getProperty('fullname',"")})
        if mail_to:
            mail_to.sort(key=lambda item: item['nome'])
        return mail_to
