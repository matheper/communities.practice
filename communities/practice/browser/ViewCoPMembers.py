# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.generics import getMembersData
import re

class ViewCoPMembers(ViewCoPBase):
    """Default view for search into CoPs."""
    def __call__(self):
        """
            Set user permissions for CoP structure and
            get community members.
        """
        self.set_user_permissions()
        if self.context.portal_type == u'CoP':
            self.cop_menu_status['inicio'] = 'cop_menu_selected'

        return super(ViewCoPMembers, self).__call__()

    def get_cop_members(self):
        """
            Get community members. Filtering by MemberSearchableText.
        """
        members = []
        catalog = getToolByName(self.context, 'portal_catalog')
        brain = catalog(UID = self.context.UID())
        allow_role = ['Owner','Moderador', 'Participante']
        if brain:
            cop_brain = brain[0]
            membersByRole = getCoPLocalRoles(cop_brain).get(cop_brain.UID)
            for role in allow_role:
                members += membersByRole[role]

        form = self.request.form
        if 'MemberSearchableText' in form.keys():
            searchableText = form.get('MemberSearchableText')
            filter_list = []
            for member in members:
                filtered = re.search(searchableText, member[1], re.I)
                if filtered:
                    filter_list.append(member)
            return filter_list
        return members

    def get_members_data(self, member_list):
        """
            Gets members data only for the batch list.
        """
        members_data = []
        members_data += getMembersData(self.context, [user[0] for user in member_list])
        return members_data

