from Products.CMFCore.utils import getToolByName
from Acquisition import aq_inner, aq_parent
from zope.component import getMultiAdapter
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getLocalRole
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from zope.component.hooks import getSite

class ViewCoPCommunityFolder(ViewCoPBase):
    """Default View for Community Folder"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_menu_status = {"criar":"cop_menu_disabled"}
        self.cop_menu_links = {}
        self.cop_menu_titles = {}
        if self.cop_addable_types.has_key('CoP'):
            self.cop_menu_status = {"criar":"cop_menu_item"}
            self.cop_menu_links = {"criar":self.cop_addable_types['CoP']}

        #Manager e Owner
        authenticated_user = self.context.portal_membership.getAuthenticatedMember()
        role = authenticated_user.getRoles()
        local_role = getLocalRole(self.context, authenticated_user.getId())
 
        if 'Manager' in role or 'Owner' in local_role or 'Moderador_Master' in local_role:
            self.cop_menu_status['moderadores']="cop_menu_item"
            self.cop_menu_links['moderadores'] = self.context.absolute_url() + "/viewCoPModeradorMaster"
            self.cop_menu_status['nunca_logaram']="cop_menu_item"
            self.cop_menu_links['nunca_logaram'] = self.context.absolute_url() + "/viewCoPMembersNeverLogged"
            self.cop_menu_status['atividade_folder']="cop_menu_item"
            self.cop_menu_links['atividade_folder'] = self.context.absolute_url() + "/viewCoPCommunityFolderAtividade"

        self.community_folder = aq_parent(aq_inner(self))
        self.communities = []
        self.my_communities = []
        self.communities_alphabetical = []
        self.communities_activity = []
        self.busca_comunidades()
        return super(ViewCoPCommunityFolder, self).__call__()

    def busca_comunidades(self):
        portal = getToolByName(self, 'portal_url').getPortalObject()
        portal_membership = getToolByName(self.context, 'portal_membership')
        portal_workflow = getToolByName(self.context, 'portal_workflow')
        participant = portal_membership.getAuthenticatedMember()
        catalog = getToolByName(self.context, 'portal_catalog')
        community_folder_path = '/'.join(self.context.getPhysicalPath())

        communities_brain = catalog(  path = community_folder_path,
                                sort_on = 'Date',
                                sort_order = 'reverse',
                                portal_type = 'CoP')

        for community in communities_brain:
            community_owner_id = community.Creator
            community_owner = portal_membership.getMemberById(community_owner_id)
            community_owner_name = community_owner.getProperty('fullname')
            if not community_owner_name:
                community_owner_name = community_owner_id
            image = community.imageCoP

            community_owner_url ="%s/author/%s" % (portal.absolute_url(), community_owner_id)
            participar = ""
            if community.acceptsShares:
                participar = "%s/viewCoPJoin"%(community.getURL())

            review_state = community.review_state
            workflow_id = portal_workflow.getChainFor(community.portal_type)[0]
            workflow = portal_workflow.getWorkflowById(workflow_id)
            state = workflow.states.get(review_state)
            if state:
                review_state = state.title

            cop_local_roles = getCoPLocalRoles(community)
            cop_local_roles = cop_local_roles.get(community.UID)
            members = len(cop_local_roles['Participante']) + len(cop_local_roles['Moderador']) + len(cop_local_roles['Owner'])

            member_communities = listMemberRoleCommunitiesParticipating(participant.getId())
            role = ""
            communities_participating = member_communities.get(participant.getId())
            if communities_participating:
                role = communities_participating.get(community.UID)
            
            participant_role = "Autenticado"
            if "Owner" == role or "Participante" == role or "Moderador" == role or "Observador" == role or \
               "Moderador_Master" == role == "Bloqueado" in role:
                participant_role = "Participante"
            elif "Aguardando" == role:
                participant_role = "Aguardando"

            community_path = community.getPath()
            last_activity = catalog(path = community_path,
                                    sort_on = 'modified',
                                    sort_order = 'reverse',
                                    sort_limit = 1,
                                    portal_type = ['CoPDocument', 'CoPATA', 'CoPFile', 'CoPEvent', 'CoPImage', 'CoPLink'])[:1]
    
            last_activity_date = 0
            if last_activity:
                last_activity_date = last_activity[0].modified

            community_dict = {"titulo": community.Title,
                              "criador": community_owner_name,
                              "criadorUrl":community_owner_url,
                              "estado": review_state,
                              "imagem": image,
                              "membros": members,
                              "participar":participar,
                              "descricao": community.Description,
                              "url": community.getURL(),
                              "participante": participant_role,
                              "ultima_atividade": last_activity_date}

            self.communities.append(community_dict)
            if participant_role == "Participante":
                self.my_communities.append(community_dict)

        self.communities_alphabetical = list(self.communities)
        self.communities_alphabetical.sort(key=lambda comunidade: comunidade['titulo'])
        self.communities_activity = list(self.communities)
        self.communities_activity.sort(key=lambda comunidade: comunidade['ultima_atividade'])
        self.communities_activity.reverse()
