# -*- coding: utf-8 -*-
from Acquisition import aq_inner, aq_parent
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import getLocalRole
from communities.practice.generics.generics import propagateMasterRole
from communities.practice.generics.asyncmail_config import USERNAME
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage

class ViewCoPModeradorMaster(ViewCoPBase):
    """Default View for Community Folder"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_menu_status = {"criar":"cop_menu_disabled"}
        self.cop_menu_titles = {}
        if self.cop_addable_types.has_key('CoP'):
            self.cop_menu_status = {"criar":"cop_menu_item"}
            self.cop_menu_links = {"criar":self.cop_addable_types['CoP']}

        #Manager e Owner
        authenticated_user = self.context.portal_membership.getAuthenticatedMember()
        role = authenticated_user.getRoles()
        local_role = getLocalRole(self.context, authenticated_user.getId())

        if 'Manager' in role or 'Owner' in local_role or 'Moderador_Master' in local_role:
            self.cop_menu_status['moderadores']="cop_menu_selected"
            self.cop_menu_links['moderadores'] = self.context.absolute_url() + "/viewCoPModeradorMaster"
            self.cop_menu_status['nunca_logaram']="cop_menu_item"
            self.cop_menu_links['nunca_logaram'] = self.context.absolute_url() + "/viewCoPMembersNeverLogged"
            self.cop_menu_status['atividade_folder']="cop_menu_item"
            self.cop_menu_links['atividade_folder'] = self.context.absolute_url() + "/viewCoPCommunityFolderAtividade"

        self.community_folder = aq_parent(aq_inner(self))
        self.communities = []
        self.my_communities = []
        return super(ViewCoPModeradorMaster, self).__call__()


    def get_moderadores_master(self):
        """ Retorna os membros com role de Moderador_Master ou Observador_Master
        """
        portal = self.community_folder.portal_url.getPortalObject()
        local_roles = self.community_folder.get_local_roles()
        moderadores_master = []
        self.moderadores_master = []
        for user_id, roles in local_roles:
            master_role = [role for role in roles if role in ["Moderador_Master", "Observador_Master"]]
            if master_role:
                member_data = getMemberData(self.community_folder, user_id)
                if member_data:
                    nome, email = member_data 
                    moderadores_master.append({"nome":nome.capitalize(),
                                               "login":user_id,
                                               "papel":master_role[0],
                                               "author":"%s/author/%s" % (portal.absolute_url(), user_id)})
                    self.moderadores_master.append(user_id)

        if moderadores_master:
            moderadores_master.sort(key=lambda item: item['nome'])

        return moderadores_master


    def set_moderadores_master(self):
        """ Altera o status da participacao master local e das comunidades recursivamente.
            Form retorna dicionario com id do usuario como chave, e uma lista com o papel atual
            e o papel anterior do usuario como valor.
            Caso as permissoes sejam diferentes, novo papel e' atribuido ao usuario.
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_moderadores_master" in form_keys:
            if not self.moderadores_master:
                self.get_moderadores_master()
            form_items = form.items()
            for user, roles in form_items:
                new_role = roles[0]
                old_role = roles[1]
                if new_role != old_role:
                    if user in self.moderadores_master:
                        propagateMasterRole(self.context, user, new_role)
            portal = self.community_folder.portal_url.getPortalObject()
            url = self.context.absolute_url() + "/viewCoPModeradorMaster"
            portal.REQUEST.RESPONSE.redirect(url)
        return None


    def get_adicionar_moderadores(self):
        """Retorna os membros do portal que ainda nao sao Moderador_Master ou Observador_Master
        """ 
        portal = self.community_folder.portal_url.getPortalObject()

        adicionar = []
        self.adicionar = []

        form = self.request.form
        form_keys = form.keys()

        filtro = ""
        if 'text_busca_adicionar_moderadores' in form_keys:
            filtro = form.get('text_busca_adicionar_moderadores')

        if len(filtro) > 1:
            #busca os membros do portal, filtrando pelo que for digitado no form
            users = portal.portal_memberdata.searchFulltextForMembers(filtro)

            local_roles = dict(self.community_folder.get_local_roles())
            users_community_folder = []
            for user_id in local_roles.keys():
                users_community_folder.append(user_id)

            for user in users:
                user_id = user.getId()
                if not user_id in users_community_folder:
                    nome, email = getMemberData(self.community_folder, user_id)
                    adicionar.append({"nome":nome.capitalize(),
                                      "login":user_id,
                                      "author":"%s/author/%s" % (portal.absolute_url(), user_id)})
                    self.adicionar.append(user_id)

            if adicionar:
                adicionar.sort(key=lambda item: item['nome'])
        return adicionar


    def set_adicionar_moderadores(self):
        """ Adiciona os participantes selecionados como Moderador_Master ou Observador_Master
            Envia um e-mail notificando que usuario foi inserido
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_adicionar_moderadores" in form_keys:
            form_items = form.items()
            mail_to_observadores = []
            mail_to_moderadores = []

            for user_id, role in form_items:
                propagateMasterRole(self.context, user_id, role)
                if role in ["Moderador_Master", "Observador_Master"]:
                    nome, email = getMemberData(self.community_folder, user_id)
                    if role == "Observador_Master":
                        mail_to_observadores.append(email)
                    else: 
                        mail_to_moderadores.append(email)

            mail_lists = ( (mail_to_observadores, "Observador"),
                           (mail_to_moderadores, "Moderador Master"),
                         )
            mail_from = USERNAME
            for mail_to, role in mail_lists:
                if mail_to:
                    assunto = encodeUTF8("%s em Pasta" % role)
                    mensagem = encodeUTF8("Você foi adicionado como %s na pasta %s.\n" % (role, self.community_folder.Title()))
                    mensagem += encodeUTF8("Para acessar a pasta clique em %s" % (self.community_folder.absolute_url()))
                    createMessage(mail_from, mail_to, assunto, mensagem)
                    sendAsyncMail()

            url = "%s/viewCoPModeradorMaster#tab_cop_moderadores" % self.context.absolute_url()
            portal = self.community_folder.portal_url.getPortalObject()
            portal.REQUEST.RESPONSE.redirect(url)

        return ""
