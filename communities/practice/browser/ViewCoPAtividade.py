# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from Acquisition import aq_parent
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.atividade_generics import TYPES_ATIVIDADE
from communities.practice.generics.atividade_generics import getCoPAtividadeMembers
from communities.practice.generics.atividade_generics import getCoPAtividadeContent
from communities.practice.generics.atividade_generics import getCoPAtividadeTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeTotalTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeTabs
from communities.practice.generics.atividade_generics import getCoPAtividadeLenMembers
from communities.practice.generics.atividade_generics import getStartEndDate
from communities.practice.content.CoPMenu import CoPMenu
from zope.security import checkPermission
from zope.component.hooks import getSite
import re

class ViewCoPAtividade(ViewCoPBase):
    """Default view for users content.
    """

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPAtividade, self).__call__()

    def get_community_members(self, input_filter = "MemberSearchableText"):
        """Retorna os membros da comunidade
        """
        community = getCoPContext(self.context)
        memberlist = getCoPAtividadeMembers(community, self.request.form, input_filter)
        return memberlist

    def get_community_content(self, member_list, folder=""):
        """Retorna conteudo da comunidade ordenado por participante.
           Pode ser passado por parametro pasta onde sera feita a pesquisa (tarefas, portfolio).
        """
        community = getCoPContext(self.context)
        period = getStartEndDate(self.request.form)
        content = getCoPAtividadeContent(community, member_list, folder, period)
        return content

    def get_types(self, folder=""):
        """Retorna os tipos de conteudo a serem considerados na view de atividades
        """
        types = getCoPAtividadeTypes(folder, True, self.context)
        return types

    def check_permission(self):
        return checkPermission('communities.practice.ObserveCoP', self.context)

    def get_total_types(self, folder=""):
        """Retorna os totais de cada tipo de conteudo da comunidade
           Pode ser passado por parametro o local onde sera feita a pesquisa (tarefas, portfolio, acervo).
        """
        community = getCoPContext(self.context)
        period = getStartEndDate(self.request.form)
        total_content = getCoPAtividadeTotalTypes(community, folder, period)
        return total_content

    def get_tabs(self):
        """Retorna as abas que serao exibidas na view de atividades
        """
        tabs = getCoPAtividadeTabs(self.context)
        return tabs

    def get_len_members(self):
        len_members = getCoPAtividadeLenMembers(self.context)
        return len_members
