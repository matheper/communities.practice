from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPImage(ViewCoPBase):
    """Default View for CoPImage"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPImage, self).__call__()
