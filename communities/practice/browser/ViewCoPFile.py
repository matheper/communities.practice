from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPFile(ViewCoPBase):
    """Default View for CoPFile"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPFile, self).__call__()
