from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPATA(ViewCoPBase):
    """Default view for Community Atas."""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPATA,self).__call__()
