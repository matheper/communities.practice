# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import getCoPRole, getCoPUpdates, getMemberData, getUsersByRole, setUserMailList
from communities.practice.generics.asyncmail import createMessage, sendAsyncMail
from communities.practice.generics import asyncmail_config
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import propagateRole


class ViewCoPLeaveButton(BrowserView):
    """Default View for Communities of Practice"""

    def __call__(self):
        self.portal_workflow = getToolByName(self.context, 'portal_workflow')
        self.review_state = self.portal_workflow.getInfoFor(self.context, "review_state", "")
        return super(ViewCoPLeaveButton, self).__call__()

    def send_request(self):
        form = self.request.form
        form_keys = form.keys()
        cop = self.context
        portal_membership = getToolByName(cop, 'portal_membership')
        if 'submit' in form.keys() and not portal_membership.isAnonymousUser():
            if self.review_state == "publico":
                authenticated_id = portal_membership.getAuthenticatedMember().getId()
                if cop.getOwner().getId() != authenticated_id:
                    propagateRole(cop, authenticated_id, "Excluir")
            else:
                message = form.get("message")
                self.send_notification_mail(message)
            community_folder = getCoPContext(self.context, ['CoPCommunityFolder'])
            if community_folder:
                return self.request.response.redirect(community_folder.absolute_url())

    def send_notification_mail(self, requestor_message):
        """Send message for moderators."""
        community = self.context
        portal = community.portal_url.getPortalObject()
        community_title = community.Title()
        community_url = community.absolute_url()
        #Requestor data
        requestor_id = portal.portal_membership.getAuthenticatedMember().getId()
        requestor_data = getMemberData(community, requestor_id)
        #Moderators data
        roles = ['Owner','Moderador','Moderador_Master']
        list_moderators = getUsersByRole(community, roles)
        list_mail_to = []
        for role in roles:
            for user in list_moderators[role]:
                list_mail_to.append(user['email'])
        #message
        mail_from = asyncmail_config.USERNAME
        subject = "Pedido para deixar a comunidade %s"% (community_title)
        message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'

        message += """O usuário %s solicitou permissão para deixar de participar da comunidade %s.\nPara permitir que ele deixe de participar clique no link abaixo.\n %s/configuracoes""" % (requestor_data[0], community_title, community_url)
        if requestor_message:
            message += "\n\nMensagem deixada por %s:\n%s\n"%(requestor_data[0], requestor_message)
        createMessage(mail_from, list_mail_to, subject, message)
        sendAsyncMail()
