# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import getBrainValues, getCommentaries, isDiscussionAllowed
from Products.CMFPlone.utils import getToolByName

class ViewCoPFolderContents(BrowserView):
    """Default View for Communities of Practice Profile Files tab."""

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        self.catalog = getToolByName(self.portal, 'portal_catalog')
        self.portal_membership = getToolByName(self.portal, 'portal_membership')

    def get_content(self, limit=0, sort_on = '', sort_order=''):
        """ Get context content."""
        query = {}
        if limit:
            query['sort_limit'] = limit
        if sort_on:
            query['sort_on'] = sort_on
        if sort_order:
            query['sort_order'] = sort_order
        folder_path = '/'.join(self.context.getPhysicalPath())
        query['path'] = {'query': folder_path, 'depth': 1}
        show_inactive = self.portal_membership.checkPermission('Access inactive portal content', self.context)

        results = self.catalog(query, show_all=1, show_inactive=show_inactive)
        return results

    def get_state_name(self, portal_type, review_state):
        """Get object state name from state id."""
        portal_workflow = getToolByName(self.context, 'portal_workflow')
        state_title = portal_workflow.getTitleForStateOnType(review_state, portal_type)
        return state_title

    def get_brain_values(self, news):
        """
            Get values for a brain list.
        """
        return getBrainValues(self.context, news, 'modified')
