from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPUpload(ViewCoPBase):
    """Default View for CoPUpload"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPUpload, self).__call__()
