# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import getCoPRole, getCoPUpdates, getMemberData, getUsersByRole, setUserMailList
from communities.practice.generics.asyncmail import createMessage, sendAsyncMail
from communities.practice.generics import asyncmail_config
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.cache import updateCommunityCache


class ViewCoPJoinButton(BrowserView):
    """Default View for Communities of Practice"""

    def __call__(self):
        self.portal_workflow = getToolByName(self.context, 'portal_workflow')
        self.review_state = self.portal_workflow.getInfoFor(self.context, "review_state", "")
        return super(ViewCoPJoinButton, self).__call__()

    def send_participation_request(self):
        form = self.request.form
        form_keys = form.keys()
        portal_membership = getToolByName(self.context, 'portal_membership')
        if ('submit' in form.keys() or self.review_state == "publico") and not portal_membership.isAnonymousUser():
            portal_membership = getToolByName(self.context, 'portal_membership')
            user = portal_membership.getAuthenticatedMember()
            user_id = user.getId()
            if not getCoPRole(self.context.UID(), user_id):
                message = form.get("message")
                review_state = self.portal_workflow.getInfoFor(self.context, "review_state", "")
                role = ""
                if review_state =="publico":
                    self.context.manage_setLocalRoles(user_id,["Participante"])
                    role = "Participante"
                    self.context.reindexObjectSecurity()
                    name, email = getMemberData(self.context, user_id)
                    setUserMailList(self.context, email, True)
                    #futuramente o envio podera ser configurado nas configuracoes de uma CoP
                    #self.send_notification_mail(message, publica = True)
                if review_state =="restrito":
                    self.context.manage_setLocalRoles(user_id,["Aguardando"])
                    role = "Aguardando"
                    self.context.reindexObjectSecurity()
                    self.send_notification_mail(message)
                if role:
                    updateCommunityCache(self.context, user_id, role)
            return self.request.response.redirect(self.context.absolute_url())

    def send_notification_mail(self, requestor_message, publica = False):
        """Send message for moderators."""
        community = self.context
        portal = community.portal_url.getPortalObject()
        community_title = community.Title()
        community_url = community.absolute_url()
        #Requestor data
        requestor_id = portal.portal_membership.getAuthenticatedMember().getId()
        requestor_data = getMemberData(community, requestor_id)
        #Moderators data
        roles = ['Owner','Moderador','Moderador_Master']
        list_moderators = getUsersByRole(community, roles)
        list_mail_to = []
        for role in roles:
            for user in list_moderators[role]:
                list_mail_to.append(user['email'])
        #message
        mail_from = asyncmail_config.USERNAME
        subject = "Novo membro da comunidade %s"% (community_title)
        message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
        
        #futuramente o envio podera ser configurado nas configuracoes de uma CoP
        #if publica:
        #    message += """O usuário %s é o mais novo membro da comunidade %s\nPara acessar a comunidade clique em: %s""" % (
        #                   requestor_data[0], community_title, community_url)
        #else:
        #    message += """O usuário %s solicitou permissão para participar da comunidade %s.\nPara permitir o acesso clique no link abaixo.\n %s/configuracoes#tab_cop_part_pendentes""" % (requestor_data[0], community_title, community_url)

        message += """O usuário %s solicitou permissão para participar da comunidade %s.\nPara permitir o acesso clique no link abaixo.\n %s/configuracoes#tab_cop_part_pendentes""" % (requestor_data[0], community_title, community_url)
        if requestor_message:
            message += "\n\nMensagem deixada por %s:\n%s\n"%(requestor_data[0], requestor_message)
        createMessage(mail_from, list_mail_to, subject, message)
        sendAsyncMail()
