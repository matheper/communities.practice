# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import getCoPUpdates, getMemberData, getUsersByRole, getCommunitiesParticipating, getCoPContext
from communities.practice.generics.asyncmail import createMessage, sendAsyncMail
from communities.practice.generics import asyncmail_config
from communities.practice.generics.generics import encodeUTF8
from Products.CMFCore.utils import getToolByName


class ViewCoPInvitation(BrowserView):
    """Default View for Communities of Practice"""

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.portal_membership = getToolByName(self.context, 'portal_membership')

    def get_cop_context(self):
        self.cop_context = getCoPContext(self.context,['CoP',])
        return self.cop_context

    def get_communities_participating(self):
        portal = self.context.portal_url.getPortalObject()
        user = self.portal_membership.getAuthenticatedMember()
        communities = getCommunitiesParticipating(user, portal)
        if self.cop_context:
            communities.remove(self.cop_context)
        return communities

    def send_invitation_request(self):
        form = self.request.form
        form_keys = form.keys()
        if 'submit' in form.keys():
            form.pop('submit')
            portal_membership = getToolByName(self.context, 'portal_membership')
            user = portal_membership.getAuthenticatedMember()
            message = form.pop("message")
            mail_list = form.pop("email_list")
            mail_list = mail_list.replace(' ','')
            if mail_list:
                mail_list = mail_list.split(',')
            if form and mail_list:
                self.send_invitation_mail(message, mail_list, form)
            return self.request.response.redirect(self.context.absolute_url())

    def send_invitation_mail(self, user_message, mail_list, cop_list):
        """Send message for moderators."""
        portal = self.context.portal_url.getPortalObject()
        #User data
        user_id = portal.portal_membership.getAuthenticatedMember().getId()
        user_name = encodeUTF8(getMemberData(self.context, user_id)[0])
        portal_title = encodeUTF8(portal.Title())
        num_cop = len(cop_list)
        #message
        mail_from = asyncmail_config.USERNAME
        message = 'Content-Type: text/html; charset="UTF-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\n\n'
        message += """
                    <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    </head>
                    <body style="background-color: fff;">
                    <div style="background-color:#CCFFCC; border-radius: 0.85em; padding: 20px 20px 40px 20px;">
                    <div width="98%%" border="0" cellspacing="0" style="border:thin solid #d4d4d4;border-radius:0.85em;background-color:#fff;padding: 5px 5px 5px 5px">
                    """
        subject = "%s convidou você a participar do Portal %s"%(user_name, portal_title)
        message += "%s<br>" % (subject)

        if user_message:
            message += "<br>Mensagem deixada por %s:<br> %s <br><br>"%(user_name, user_message)
            
        message += """A Comunidade de Prática é uma plataforma para a construção de comunidades virtuais de práticas voltadas para aplicações de aprendizagem social. Oferece um conjunto de ferramentas de comunicação e colaboração integrados em um ambiente comum para compartilhar conhecimentos.<br><br>"""
        message += "Cadastre-se no portal <a href=%s>%s</a> <br><br>" % (portal.absolute_url() ,portal_title)

        if num_cop == 1:
            message += "Visite a Comunidade:<br>"
            message += "<a href=%s>%s</a><br><br>" % (cop_list.values()[0], cop_list.keys()[0])

        if num_cop > 1:
            message += "Visite as Comunidades:<br>"
            for cop_title, cop_url in cop_list.items():
                message += "<a href=%s>%s</a><br>" % (cop_url, cop_title)

        message+=  """
                    </div>
                    </div>
                    </body>
                    </html>
                    """
        createMessage(mail_from, mail_list, subject, message)
        sendAsyncMail()
