from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPSubCoPMenu(ViewCoPBase):
    """Default View for CoPMenu"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_footer_links['Manage'] =  "%s/folder_contents"%(self.cop_url)
        return super(ViewCoPSubCoPMenu, self).__call__()
