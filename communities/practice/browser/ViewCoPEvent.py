from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPEvent(ViewCoPBase):
    """Default view for Community Events."""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPEvent,self).__call__()
