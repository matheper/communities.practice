# -*- coding: utf-8 -*-
from Acquisition import aq_parent
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.atividade_generics import exportCoPCommunityFolderAtividade
from communities.practice.generics.atividade_generics import getCoPAtividadeTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeTotalTypes
from communities.practice.generics.atividade_generics import getCoPCommunityFolderAtividadeTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeCommunities
from communities.practice.generics.atividade_generics import getStartEndDate
from communities.practice.generics.generics import getCoPRole
from zope.component.hooks import getSite
from zope.security import checkPermission

class ViewCoPCommunityFolderAtividade(ViewCoPBase):
    """Default view for users content.
    """

    def __call__(self):
        self.set_user_permissions()
        self.cop_menu_status = {"criar":"cop_menu_disabled"}
        self.cop_menu_titles = {}
        if self.cop_addable_types.has_key('CoP'):
            self.cop_menu_status = {"criar":"cop_menu_item"}
            self.cop_menu_links = {"criar":self.cop_addable_types['CoP']}

        #Manager e Owner
        authenticated_user = self.context.portal_membership.getAuthenticatedMember()
        role = authenticated_user.getRoles()
        local_role = getCoPRole(self.context.UID(), authenticated_user.getId())
     
        if 'Manager' in role or 'Owner' in local_role or 'Moderador_Master' in local_role:
            self.cop_menu_status['moderadores'] = "cop_menu_item"
            self.cop_menu_links['moderadores'] = self.context.absolute_url() + "/viewCoPModeradorMaster"
            self.cop_menu_status['nunca_logaram'] = "cop_menu_item"
            self.cop_menu_links['nunca_logaram'] = self.context.absolute_url() + "/viewCoPMembersNeverLogged"
            self.cop_menu_status['atividade_folder'] = "cop_menu_selected"
            self.cop_menu_links['atividade_folder'] = self.context.absolute_url() + "/viewCoPCommunityFolderAtividade"

        return super(ViewCoPCommunityFolderAtividade, self).__call__()

    def get_types(self):
        types = getCoPCommunityFolderAtividadeTypes()
        return types

    def check_permission(self):
        return checkPermission('cmf.ReviewPortalContent', self.context)

    def get_communities(self):
        """Retorna os brains das comunidades do folder
        """
        return getCoPAtividadeCommunities(self.context)

    def get_total_types(self):
        """Retorna o total de cada tipo de conteudo da comunidade
        """
        communities_content = []
        communities = self.get_communities()
        period = getStartEndDate(self.request.form)
        for community in communities:
            total_content = getCoPAtividadeTotalTypes(community["brain"], "copcommunityfolder", period)
            if total_content.get(("total",u"Total"),False):
                community_content = {}
                community_content["community"] = community
                community_content["total_content"] = total_content
                communities_content.append(community_content)
        return communities_content

    def export_data(self):
        """Exporta os dados da view
        """
        form = self.request.form
        if "submit_export_atividades" in form.keys():
            exportCoPCommunityFolderAtividade(self.request, self.context)
        return False
