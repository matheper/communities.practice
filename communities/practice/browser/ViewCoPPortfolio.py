from Products.CMFPlone.utils import getToolByName
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getBrainValues
from communities.practice.generics.generics import getCommentaries
from communities.practice.generics.generics import isDiscussionAllowed

class ViewCoPPortfolio(ViewCoPBase):
    """Default View for CoPPortfolio"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPPortfolio, self).__call__()

    def get_content(self):
        """ Get context content."""
        catalog = getToolByName(self.context, 'portal_catalog')
        query = {}
        query['sort_on'] = 'copLastModification'
        query['sort_order'] = 'reverse'
        folder_path = '/'.join(self.context.getPhysicalPath())
        query['path'] = {'query': folder_path, 'depth': 1}

        results = catalog(query)
        return results

    def get_brain_values(self, news):
        """
            Get values for a brain list.
        """
        return getBrainValues(self.context, news)

    def get_commentaries(self, path, qtd = 0):
        """
            Busca Comentarios a partir do path. Pode ser limitado atraves do parametro qtd.
            Retorna no formato lista de dicionarios.
        """
        self.see_more = False
        commentaries = getCommentaries(self.context, path)
        if qtd and len(commentaries) > qtd:
            self.see_more = True
            return commentaries[len(commentaries)-qtd:]
        return commentaries

    def is_discussion_allowed(self, cop_uid):
        return isDiscussionAllowed(self.context, cop_uid)
