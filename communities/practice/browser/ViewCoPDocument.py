from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPDocument(ViewCoPBase):
    """Default view for Community Events."""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPDocument,self).__call__()
