# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getMemberData, encodeUTF8
from communities.practice.generics.generics import setUserMailList, getUserMailList
from communities.practice.generics.generics import propagateRole
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail_config import USERNAME
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.generics.generics import hasSuperRole
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import getCoPParticipants
from communities.practice.generics.generics import getRootCoP
from Acquisition import aq_parent
from Products.CMFPlone.utils import getToolByName
from zope.security import checkPermission
import re

class ViewCoPConfig(ViewCoPBase):
    """Default View for CoPConfig"""

    participantes = []

    def __call__(self):
        self.set_user_permissions()
        self.comunidade = self.aq_parent.aq_parent
        self.portal = self.comunidade.portal_url.getPortalObject()
        self.portal_membership = getToolByName(self.context, 'portal_membership')
        return super(ViewCoPConfig, self).__call__()

    def get_participantes(self):
        """Retorna os participantes da comunidade
        """
        #cria um dicionario onde o id eh a chave e os roles sao o valor
        portal_url = self.portal.absolute_url()
        local_roles = self.comunidade.get_local_roles()
        participantes = []
        self.participantes = []
        for user_id, roles in local_roles:
            if not ('Aguardando' in roles):
                member_data = self.portal_membership.getMemberInfo(user_id)
                if member_data:
                    nome = encodeUTF8(member_data.get('fullname') if member_data.get('fullname') else member_data.get('username'))
                    member_roles = list(roles)
                    if 'Moderador' in roles or 'Observador' in roles or 'Bloqueado' in roles:
                        if hasSuperRole(self.comunidade, user_id):
                            member_roles.append('Super')
                    participantes.append({"nome":nome.capitalize(),
                                          "login":user_id,
                                          "papel":member_roles,
                                          "author":"%s/author/%s" % (portal_url, user_id)})
                    self.participantes.append(user_id)
        if participantes:
            participantes.sort(key=lambda item: item['nome'])

        return participantes

    def set_participantes(self):
        """ Altera o status da participacao dos membros da comunidade conforme as marcacoes no form
            Form retorna dicionario com id do usuario como chave, e uma lista com o papel atual
            e o papel anterior do usuario como valor.
            Caso as permissoes sejam diferentes, novo papel e' atribuido ao usuario.
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_participantes" in form_keys:
            if not self.participantes:
                self.get_participantes()
            form_items = form.items()
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = self.comunidade.UID()
            is_sub_cop = isSubCoP(self.comunidade)
            for user, roles in form_items:
                new_role = roles[0]
                old_role = roles[1]
                if new_role != old_role:
                    if user in self.participantes and not hasSuperRole(self.comunidade, user):
                        propagateRole(self.comunidade, user, new_role)
                        if new_role == 'Excluir' and not is_sub_cop and \
                           group_id in portal_groups.getGroupIds():
                            portal_groups.removePrincipalFromGroup(user, group_id)

            url = self.context.absolute_url()
            self.portal.REQUEST.RESPONSE.redirect(url)
        return None

    def get_participacoes_pendentes(self):
        """Retorna os membros com participacao pendente na comunidade
        """
        local_roles = self.comunidade.users_with_local_role('Aguardando')
        aguardando = []
        self.aguardando = []
        for user_id in local_roles:
            nome, email = getMemberData(self.comunidade, user_id)
            aguardando.append({"nome":nome.capitalize(),
                               "login":user_id,
                               "papel":'Aguardando',
                               "author":"%s/author/%s" % (self.portal.absolute_url(), user_id)})
            self.aguardando.append(user_id)
    
        if aguardando:
            aguardando.sort(key=lambda item: item['nome'])
    
        return aguardando

    def set_participacoes_pendentes(self):
        """Altera o status da participacao dos membros da comunidade conforme as marcacoes no form
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_participacoes_pendentes" in form_keys:
            if not self.aguardando:
                self.get_participacoes_pendentes()

            #lista dos inputs do form. Aqui estao os radio buttons que controlam a aprovacao
            #da participacao. O id do membro e o nome do radio button, e a permissao e a opcao selecionada
            #form_items e uma lista de tuplas (id,permissao)
            lista_aprovados = []
            lista_negados = []
            form_items = form.items()
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = self.comunidade.UID()
            is_sub_cop = isSubCoP(self.comunidade)
            for user,permissao in form_items:
                if user in self.aguardando:
                    #Se estiver marcado para Aprovar a participacao, adiciona o membro como Participante
                    if permissao == 'aprovar':
                        if not is_sub_cop and group_id in portal_groups.getGroupIds():
                            portal_groups.addPrincipalToGroup(user, group_id)
                        self.comunidade.manage_setLocalRoles(user,['Participante'])
                        updateCommunityCache(self.comunidade, user, 'Participante')
                        nome, email = getMemberData(self.comunidade, user)
                        lista_aprovados.append(email)
                        setUserMailList(self.context, email, True)
                    #Se estiver marcado para Negar a participacao, remove o membro dos local_roles da comunidade
                    elif permissao == 'negar':
                        self.comunidade.manage_delLocalRoles([user])
                        updateCommunityCache(self.comunidade, user)
                        nome, email = getMemberData(self.comunidade, user)
                        lista_negados.append(email)

            self.comunidade.reindexObjectSecurity()

            moderador_id = self.portal.portal_membership.getAuthenticatedMember().getId()
            moderador_nome, email = getMemberData(self.comunidade, moderador_id)
            moderador_nome = encodeUTF8(moderador_nome)
            mail_from = USERNAME
            mensagem = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
            if lista_aprovados:
                assunto = encodeUTF8("Participação aprovada")
                mensagem += encodeUTF8("Sua participação na comunidade %s foi aprovada." % (self.comunidade.Title()))
                mensagem += encodeUTF8('\n\n\nLink para acesso a comunidade:\n%s'%(self.comunidade.absolute_url()))
                createMessage(mail_from, lista_aprovados, assunto, mensagem)
                sendAsyncMail()

            if lista_negados:
                mensagem_moderador = form.get('refused_message')
                assunto = encodeUTF8("Participação não aprovada")
                mensagem = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
                mensagem += encodeUTF8("Sua participação na comunidade %s não foi aprovada pelo Moderador." % (self.comunidade.Title()))
                if mensagem_moderador:
                    mensagem += encodeUTF8("\nMotivo:\n%s" % (mensagem_moderador))
                mensagem += encodeUTF8("\n\nMensagem enviada por: %s" % (moderador_nome))
                createMessage(mail_from, lista_negados, assunto, mensagem)
                sendAsyncMail()

            url = "%s#tab_cop_part_pendentes" % (self.context.absolute_url())
            self.portal.REQUEST.RESPONSE.redirect(url)
        
        return None

    def get_adicionar_participantes(self):
        """ Retorna os membros do portal que ainda nao participam da comunidade para RootCoPs
            Retorna os membros da SuperCoP que ainda nao participam da comunidade para SubCoPs
        """
        adicionar = []
        self.adicionar = []

        form = self.request.form
        form_keys = form.keys()

        filtro = ""
        if 'text_busca_adicionar_participantes' in form_keys:
            filtro = form.get('text_busca_adicionar_participantes')

        if len(filtro) > 1:
            master_roles = ["Moderador_Master", "Observador_Master"]
            local_roles = self.comunidade.get_local_roles()
            users_comunidade = []
            for id, roles in local_roles:
                if [role for role in roles if not role in master_roles]:
                    users_comunidade.append(id)

            if isSubCoP(self.comunidade):
                superCoP = getSuperCoP(self.comunidade)
                users = superCoP.get_local_roles()
                for id, roles in users:
                    ignored_roles = ["Moderador_Master", "Observador_Master",
                                     "Bloqueado", "Aguardando", "Observador"]
                    parent_roles =  [role for role in roles if not role in ignored_roles]
                    if parent_roles and not id in users_comunidade:
                        member_data = self.portal_membership.getMemberInfo(id)
                        if member_data:
                            nome = member_data.get('fullname') if member_data.get('fullname') else member_data.get('username')
                            filtered = re.search(filtro, nome, re.I) or re.search(filtro, id, re.I)
                            if filtered:
                                adicionar.append({"nome":nome.capitalize(),
                                                "login":id,
                                                "author":"%s/author/%s" % (self.portal.absolute_url(),id)})
                                self.adicionar.append(id)
            else:
                #busca os membros do portal, filtrando pelo que for digitado
                users = self.portal.portal_memberdata.searchFulltextForMembers(filtro)
                for user in users:
                    id = user.getId()
                    if not id in users_comunidade:
                        member_data = self.portal_membership.getMemberInfo(id)
                        if member_data:
                            nome = member_data.get('fullname') if member_data.get('fullname') else member_data.get('username')
                            adicionar.append({"nome":nome.capitalize(),
                                            "login":id,
                                            "author":"%s/author/%s" % (self.portal.absolute_url(),id)})
                            self.adicionar.append(id)

            if adicionar:
                #ordena a lista por nome
                adicionar.sort(key=lambda item: item['nome'])
        return adicionar

    def set_adicionar_participantes(self):
        """Adiciona os participantes selecionados como Participantes e envia um e-mail
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_adicionar_participantes" in form_keys:
            form_items = form.items()
            mail_to_participantes = []
            mail_to_observadores = []
            is_sub_cop = isSubCoP(self.comunidade)
            portal_groups = getToolByName(self, 'portal_groups')
            group_id = self.comunidade.UID()
            if is_sub_cop:
                #verifica se usuario esta no local roles da supercop
                super_cop = getSuperCoP(self.comunidade)
                super_roles = getCoPParticipants(super_cop)
            for id, role in form_items:
                if role in ["Participante", "Observador"]:
                    add_allowed = True
                    if is_sub_cop and id not in super_roles:
                        add_allowed = False
                    if add_allowed:
                        if not is_sub_cop and group_id in portal_groups.getGroupIds():
                            portal_groups.addPrincipalToGroup(id, group_id)
                        nome, email = getMemberData(self.comunidade, id)
                        if role == "Participante":
                            mail_to_participantes.append(email)
                        else:
                            mail_to_observadores.append(email)
                        #Coloca o membro como Participante da comunidade
                        self.comunidade.manage_setLocalRoles(id, [role])
                        updateCommunityCache(self.comunidade, id, role)
                        if role == 'Observador':
                            propagateRole(self.comunidade, id, role)
                        setUserMailList(self.context, email, True)

            self.comunidade.reindexObjectSecurity()

            mail_lists = (
                    (mail_to_participantes, "Participante"),
                    (mail_to_observadores, "Observadores"),
                )

            mail_from = USERNAME
            assunto = encodeUTF8("Participação em Comunidade")
            for mail_to, role in mail_lists:
                if mail_to:
                    mensagem = encodeUTF8("Você foi adicionado como %s da comunidade %s.\n" % (role, self.comunidade.Title())) 
                    mensagem += encodeUTF8("Para acessar a comunidade clique em %s" % (self.comunidade.absolute_url()))
                    createMessage(mail_from, mail_to, assunto, mensagem)
                    sendAsyncMail()

            url = self.context.absolute_url()
            self.portal.REQUEST.RESPONSE.redirect(url)

        return None

    def get_cop_role(self, user_id):
        master_roles = ["Moderador_Master", "Observador_Master"]
        roles = self.comunidade.get_local_roles_for_userid(user_id)
        local_role = [role for role in roles if not role in master_roles]
        return local_role[0] if local_role else False

    def check_permission(self, permission):
        return checkPermission(permission, self.context)

    def set_configuracao_email(self):
        """ Adiciona ou remove usuario autenticado da lista de emails de novidades."""
        form = self.request.form
        form_keys = form.keys()
        if 'submit' in form_keys:
            usuario_corrente = self.portal.portal_membership.getAuthenticatedMember()
            email = usuario_corrente.getProperty('email',None)
            if form.get("cdp_periodicidade_email") == 'Sim':
                setUserMailList(self.context, email, True)
            else:
                setUserMailList(self.context, email, False)
        return True

    def get_configuracao_email(self):
        """ Retorna true quando usuario esta na lista para receber emails.
            Funcao utilizada para marcar checkbox de configuracao."""
        lista_usuarios = getUserMailList(self.context)
        usuario_corrente = self.portal.portal_membership.getAuthenticatedMember()
        email_usuario_corrente = usuario_corrente.getProperty('email',None)
        if email_usuario_corrente and lista_usuarios and email_usuario_corrente in lista_usuarios:
            return True
        return False
