# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPSearch(ViewCoPBase):
    """Default view for search into CoPs."""
    def __call__(self):
        self.set_user_permissions()
        if self.context.portal_type == u'CoP':
            self.cop_menu_status['inicio'] = 'cop_menu_selected'
        return super(ViewCoPSearch, self).__call__()
