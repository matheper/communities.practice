# -*- coding: utf-8 -*-
from datetime import datetime
from datetime import timedelta
from DateTime import DateTime
from Products.Five.browser import BrowserView
from Acquisition import aq_parent
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.atividade_generics import getCoPAtividadeTypes

class ViewCoPConteudoAtividade(BrowserView):
    """ View padrao para modal da Atividade das comunidades."""

    def get_conteudo(self):
        """ Busca conteudo da comunidade.
            Recebe requisicao com id do usuario(string), tipo de arquivo(("File","Arquivos"))
            e local a ser pesquisado(string).
            Retorna brains do catalog.
        """
        self.usuario = self.request['user']
        self.tipo_conteudo = eval(self.request['type'])
        self.local = self.request['folder']
        start_date = DateTime("2000/01/01")
        end_date = DateTime(datetime.today() + timedelta(days=1))
        if self.request["period_start"]:
            start_date = DateTime(self.request["period_start"])
        if self.request["period_end"]:
            end_date = DateTime(self.request["period_end"]) + 1
        community = getCoPContext(self.context)
        community_path = "/".join(community.getPhysicalPath())
        tipos_consulta = self.tipo_conteudo[0]
        if self.local:
            community_path = "%s/%s"%(community_path, self.local)
        if tipos_consulta == 'total':
            types = getCoPAtividadeTypes(self.local, False, community)
            tipos_consulta = []
            for tipo, descricao in types:
                if isinstance(tipo, tuple):
                    for item in tipo:
                        tipos_consulta.append(item)
                else:
                    tipos_consulta.append(tipo)

        query = {}
        query['path'] = community_path
        if self.usuario:
            query['Creator'] = self.usuario
        query['portal_type'] = tipos_consulta
        query['created'] = {"query": [start_date,end_date], 'range':'min:max'}
        brains = self.context.portal_catalog(query)
        content = []
        for brain in brains:
            if brain.id == "arquivo_configuracao":
                continue
            if brain.portal_type == "Discussion Item":
                obj = brain.getObject()
                parent = obj.aq_parent.aq_parent
                title = parent.title_or_id()
                url = "%s/view#%s" % (parent.absolute_url(), obj.getId())
            else:
                title = brain.Title
                if not title:
                    title = brain.getId
                url = "%s/view" % (brain.getURL())
            description = brain.Description
            content.append({'title': title,
                            'description': description,
                            'url': url})
        return content
