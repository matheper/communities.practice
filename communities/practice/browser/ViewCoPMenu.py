from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPMenu(ViewCoPBase):
    """Default View for CoPMenu"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_footer_links['Manage'] =  "%s/folder_contents"%(self.cop_url)
        return super(ViewCoPMenu, self).__call__()
