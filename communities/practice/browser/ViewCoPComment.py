# -*- coding:utf8 -*-
from Products.Five.browser import BrowserView
from Products.CMFPlone.utils import getToolByName
from communities.practice.generics.generics import getCommentaries
from communities.practice.generics.generics import isDiscussionAllowed
from communities.practice.generics.generics import getBrainValues

class ViewCoPComment(BrowserView):
    """View for commentaries on Dashboard and CoP Timeline."""

    def get_context_values(self):
        """ Retorna o contexto em forma de dicionario
            para ser utilizado na macro cop_commentaries
        """
        catalog = getToolByName(self.context, 'portal_catalog')
        brain = catalog(UID=self.context.UID())
        values = getBrainValues(self.context, brain)
        return values[0]

    def get_commentaries(self, path, qtd = 0):
        """ Busca Comentarios a partir do path. Pode ser limitado atraves do parametro qtd.
            Retorna no formato lista de dicionarios.
        """
        self.see_more = False
        commentaries = getCommentaries(self.context, path)
        if qtd and len(commentaries) > qtd:
            self.see_more = True
            return commentaries[len(commentaries)-qtd:]
        return commentaries

    def is_discussion_allowed(self, cop_uid):
        return isDiscussionAllowed(self.context, cop_uid)
