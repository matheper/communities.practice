# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from zope.component import getMultiAdapter, getUtility
from Products.CMFCore.utils import getToolByName 
from zope.browsermenu.interfaces import IBrowserMenu
from communities.practice.generics.generics import getCoPContext

class ViewCoPBase(BrowserView):
    """Default View for Communities of Practice"""

    def set_user_permissions(self):
        """ Set contextual attributes for CoPMenu."""
        self.cop_menu_status = {}
        self.cop_menu_links = {}
        self.cop_menu_titles = {}
        self.cop_addable_types = {}
        self.cop_addable_types_list = []
        self.cop_footer_links = {}
        self.cop_footer_icons = {}
        self.cop_transitions = {}
        self.cop_state = ''
        #CoPMenu
        self.cop_url = self.context.absolute_url()
        self.cop_menu_links['inicio'] = self.cop_url
        cop = getCoPContext(self.context)
        self.community_title = cop.Title()
        self.community_url = cop.absolute_url()
        if not self.context.portal_type == 'CoPCommunityFolder':
            self.cop_menu_links['inicio'] = cop.absolute_url()
            self.cop_menu_titles['inicio'] = u'Início'
            cop_menu = cop.listFolderContents()
            for item in cop_menu:
                self.cop_menu_status[item.titlemenu] = 'cop_menu_item'
                self.cop_menu_links[item.titlemenu] = item.absolute_url()
                self.cop_menu_titles[item.titlemenu] = item.Title()
            cop_menu_selected = getCoPContext(self,['CoPMenu','CoP','CoPCommunityFolder'])
            if cop_menu_selected.Type() == u'CoPMenu':
                self.cop_menu_status[cop_menu_selected.titlemenu] = 'cop_menu_selected'
        #Addadble types
        browser_menu = getUtility(IBrowserMenu, name='plone_contentmenu_factory')
        items = browser_menu.getMenuItems(self.context, self.request)
        for cop_type in items:
            if not cop_type['title'] in [u'folder_add_settings', u'folder_add_more']:
                self.cop_addable_types[cop_type['id']] = cop_type['action']
                self.cop_addable_types_list.append([cop_type['id'],cop_type['title']])
        #Object Actions
        context_state = getMultiAdapter((self.context, self.request),
                        name='plone_context_state')
        if context_state.is_editable():
            self.cop_footer_links['Edit'] = "%s/edit"%(self.cop_url)
        edit_actions = context_state.actions('object_buttons')
        for action in edit_actions:
            self.cop_footer_links[action['title']] = action['url']
        #Object state
        tools = getMultiAdapter((self.context, self.request), name='plone_tools')
        state_id = context_state.workflow_state()
        self.cop_state = state_id
        workflows = tools.workflow().getWorkflowsFor(self.context)
        for w in workflows:
            if w.states.has_key(state_id):
                self.cop_state = w.states[state_id].title
        #Object transitions
        wf_tool = getToolByName(self.context, 'portal_workflow')
        transitions = wf_tool.listActionInfos(object=self.context)
        for transition in transitions:
            self.cop_transitions[transition['title']] = transition['url']

        self.cop_footer_icons = { 'email': '%s/sendto_form'%(self.cop_url),
                                  'imprimir':'javascript:this.print();',
                                  'twitter': 'http://twitter.com/home?status=%s %s'%(self.context.Title(),self.cop_url),
                                  'facebook': 'https://facebook.com/sharer.php?u=%s&t=%s'%(
                                                self.cop_url,self.context.Title()),
                                }

    def edit_cop_menu(self):
        """ Set CoPMenu for viewCoPEdit."""
        self.cop_menu_status = {}
        self.cop_menu_links = {}
        self.cop_menu_titles = {}
        self.cop_url = self.context.absolute_url()
        if not self.context.portal_type == 'CoPCommunityFolder' and not self.context.portal_type == 'CoP':
            cop = getCoPContext(self.context)
            self.cop_menu_links['inicio'] = cop.absolute_url()
            self.cop_menu_titles['inicio'] = u'Início'
            cop_menu = cop.listFolderContents()
            for item in cop_menu:
                self.cop_menu_status[item.titlemenu] = 'cop_menu_item'
                self.cop_menu_links[item.titlemenu] = item.absolute_url()
                self.cop_menu_titles[item.titlemenu] = item.Title()
            cop_menu_selected = getCoPContext(self,['CoPMenu','CoP','CoPCommunityFolder'])
            self.cop_menu_status[cop_menu_selected.titlemenu] = 'cop_menu_selected'
        elif self.context.portal_type =='CoP':
            cop = getCoPContext(self.context)
            self.cop_menu_links['inicio'] = cop.absolute_url()
            self.cop_menu_status['inicio'] = "cop_menu_selected"
            self.cop_menu_titles['inicio'] = u"Início"
        else:
            self.cop_menu_status = {"criar":"cop_menu_disabled"}
