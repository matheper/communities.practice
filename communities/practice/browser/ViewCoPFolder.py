from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import canDeleteCoPFolder

class ViewCoPFolder(ViewCoPBase):
    """Default View for CoPFolder"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPFolder, self).__call__()


    def set_user_permissions(self):
        super(ViewCoPFolder, self).set_user_permissions()
        can_delete = self.can_delete_CoPFolder()
        if self.cop_footer_links.get("Delete") and not can_delete:
            self.cop_footer_links.pop("Delete")
        if self.cop_footer_links.get("Cut") and not can_delete:
            self.cop_footer_links.pop("Cut")


    def can_delete_CoPFolder(self):
        return canDeleteCoPFolder(self.context)
