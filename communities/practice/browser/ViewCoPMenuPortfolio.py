#-*-coding:utf-8-*-
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import _createObjectByType
from zope.component.hooks import getSite

from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.asyncmail_config import USERNAME
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getUsersByRole
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import getCoPRole
from communities.practice.generics.generics import generateContentId
from communities.practice.subscribers import transitionParentState
from communities.practice.generics.generics import encodeUTF8

class ViewCoPMenuPortfolio(ViewCoPBase):
    """Default View for CoPMenuPortfolio"""

    def __call__(self):
        self.set_user_permissions()
        self.cop_footer_links['Manage'] =  "%s/folder_contents"%(self.cop_url)
        return super(ViewCoPMenuPortfolio, self).__call__()

    def get_authenticated_user_portfolio(self):
        """Checks if authenticated user has a Portfolio
        """
        path = '/'.join(self.context.getPhysicalPath())
        catalog = getToolByName(self.context, 'portal_catalog')
        portal_membership = getToolByName(self.context, 'portal_membership')
        portfolio = catalog(path={'query':path, 'depth': 1},
                                  Creator=portal_membership.getAuthenticatedMember().getId(),
                                  portal_type = 'CoPPortfolio')
        if portfolio:
            return portfolio[0]
        return False

    def create_portfolio(self):
        """ Cria portfolio com id do usuario autenticado.
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_criar_portfolio" in form_keys:
            portal_membership = getToolByName(self.context, 'portal_membership')
            user_name = getMemberData(self.context, portal_membership.getAuthenticatedMember().getId())[0]
            portfolio_id = generateContentId(self.context, user_name)
            portfolio = _createObjectByType('CoPPortfolio',
                                            self.context,
                                            id=portfolio_id)
            portfolio.unmarkCreationFlag()
            transitionParentState(portfolio, False)

            portal = self.context.portal_url.getPortalObject()
            url = portfolio.absolute_url()
            portal.REQUEST.RESPONSE.redirect(url)

    def unlock_portfolio(self):
        """ Envia mensagem aos moderadores solicitando o desbloqueio do portfolio.
        """
        form = self.request.form
        form_keys = form.keys()
        if "submit_desbloquear_portfolio" in form_keys:
            portfolio = self.get_authenticated_user_portfolio()
            portfolio = portfolio.getObject()
            portal_workflow = getToolByName(portfolio, "portal_workflow")
            portal_workflow.doActionFor(portfolio, 'aguardando')
            self.send_mail()
            return self.request.response.redirect(self.context.absolute_url())
        return ""

    def get_mail_list(self):
        """ Retorna a lista de email dos moderadores da comunidade.
        """
        community = getCoPContext(self.context, ['CoP'])
        participants = getUsersByRole(community,['Moderador'])
        users_mail = []
        for participant in participants['Moderador']:
            users_mail.append(participant['email'])
        return users_mail

    def send_mail(self):
        """ Cria mensagem para enviar aos moderadores da comunidade
            solicitando o desbloqueio do portfolio do usuario autenticado.
        """
        portal = getSite()
        portal_url = portal.absolute_url()
        portal_membership = getToolByName(self.context, 'portal_membership')
        authenticated_member = portal_membership.getAuthenticatedMember()
        member_id = authenticated_member.getId()
        user_name, email = getMemberData(portal, member_id)
        user_url = encodeUTF8("%s/author/%s"%(portal_url, member_id))
        portfolio_url = encodeUTF8(self.context.absolute_url())
        community = getCoPContext(self.context, ['CoP'])
        cop_url = encodeUTF8(community.absolute_url())
        cop_title = community.Title()
        notification_url = encodeUTF8("%s/notificacoes"%(cop_url))

        assunto = "Solicitação para desbloqueio do portfólio de %s"%(user_name)

        mensagem = 'Content-Type: text/html; charset="UTF-8"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\n\n'
        mensagem += """
                    <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                    </head>
                    <span>
                        O participante
                        <a href="%s">%s</a> solicitou o desbloqueio do seu <a href="%s">portfólio</a>
                        na Comunidade <a href="%s">%s</a>.<br/>
                        Para desbloquear acesse o <a href="%s">portfólio</a> e
                        mude seu estado para Restrito ou Público.<br/>
                        Caso o desbloqueio seja recusado, mantenha o estado bloqueado e
                        comunique o participante através de <a href="%s"> notificação individual.
                    </span>"""%(user_url, user_name, portfolio_url,
                                cop_url, cop_title, portfolio_url,
                                notification_url)
        mailTo = self.get_mail_list()
        mailFrom = USERNAME
        if mailTo:
            createMessage(mailFrom, mailTo,assunto,mensagem)
        sendAsyncMail()

    def is_participant(self):
        community = getCoPContext(self.context, ['CoP'])
        portal_membership = getToolByName(self.context, 'portal_membership')
        authenticated_member = portal_membership.getAuthenticatedMember()
        member_id = authenticated_member.getId()
        cop_role = getCoPRole(community.UID(), member_id)
        return cop_role
