# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from Acquisition import aq_parent
from communities.practice.generics.asyncmail import sendAsyncMail
from communities.practice.generics.asyncmail import createMessage
from communities.practice.generics.generics import *
from communities.practice.generics.asyncmail_config import USERNAME

class ViewCoPNotificacao(ViewCoPBase):
    """Default view para notificacoes.
    """

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.comunidade = self.aq_parent.aq_parent

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPNotificacao, self).__call__()

    def send_mail_notificacao(self):
        """Envia mensagem para os participantes da comunidade.
        """
        form = self.request.form
        if 'submit_send_mail_notificacao' in form.keys():
            portal = self.comunidade.portal_url.getPortalObject()
            moderador_nome = encodeUTF8(getMemberData(self.comunidade, portal.portal_membership.getAuthenticatedMember().getId())[0])
            title = encodeUTF8(self.comunidade.Title())
            url = encodeUTF8(self.comunidade.absolute_url())

            assunto = encodeUTF8(form.get("assunto"))
            mensagem = ""
            if form.get("mensagem"):
                mensagem = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
                mensagem += encodeUTF8(form.get("mensagem"))
                mensagem += '\n\n\nLink para acesso a comunidade:\n%s'%(url)
                mensagem += '\n\nMensagem enviada por: %s'%(moderador_nome)
            if assunto and mensagem:
                mail_to = []
                for user_id, valor in form.items():
                    if valor == 'Sim':
                        mail = getMemberData(self.comunidade, user_id)[1]
                        if mail:
                            mail_to.append(mail)
                if mail_to:
                    mail_from = USERNAME
                    createMessage(mail_from, mail_to, assunto, mensagem)
                    sendAsyncMail()
                else:
                    return u"Selecione emails para envio."
                return u"Email enviado com sucesso."
            return u"Campos obrigatórios não preenchidos"
        return ""


    def get_participantes(self):
        tipos = getUsersByRole(self.comunidade,['Participante','Moderador','Moderador_Master'])
        mail_to = []
        for papel in ['Participante', 'Moderador', 'Moderador_Master']:
            for participante in tipos[papel]:
                mail_to.append({'id':participante['id'],
                                'nome':participante['full_name']})
        mail_to.sort(key=lambda item: item['nome'])
        return mail_to
