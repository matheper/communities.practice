from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPLink(ViewCoPBase):
    """Default View for CoPLink"""

    def __call__(self):
        self.set_user_permissions()
        return super(ViewCoPLink, self).__call__()
