# -*- coding:utf8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import getBrainValues
from communities.practice.generics.generics import getCommentaries
from communities.practice.generics.generics import isDiscussionAllowed

class formCoPSearch(BrowserView):

    def get_brain_values(self, news):
        """Get values for a brain list."""
        return getBrainValues(self.context, news)

    def catalog_search(self):
        """Searchs content using queryCatalog."""
        self.request.form.update({'sort_order': 'modified'})
        results = self.context.queryCatalog(REQUEST=self.request,use_types_blacklist=True, use_navigation_root=True)
        return results

    def get_commentaries(self, path, qtd = 0):
        """
            Busca Comentarios a partir do path. Pode ser limitado atraves do parametro qtd.
            Retorna no formato lista de dicionarios.
        """
        self.see_more = False
        commentaries = getCommentaries(self.context, path)
        if qtd and len(commentaries) > qtd:
            self.see_more = True
            return commentaries[len(commentaries)-qtd:]
        return commentaries

    def is_discussion_allowed(self, cop_uid):
        return isDiscussionAllowed(self.context, cop_uid)
