# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.app.container.interfaces import INameChooser
from zope.interface import implements
from zope.component import adapter, getMultiAdapter, getUtility

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema
from Products.CMFPlone.utils import _createObjectByType
from Products.Archetypes.interfaces import IObjectInitializedEvent
from Acquisition import aq_inner, aq_parent

from plone.portlets.interfaces import IPortletManager
from plone.portlets.interfaces import IPortletAssignmentMapping

from communities.practice.interfaces import ICoPCommunityFolder
from communities.practice.config import *
from communities.practice.portlets import portletCoPSocialNetworks
from Products.CMFCore.utils import getToolByName
from DateTime import DateTime

schema = Schema((

    StringField(
        name= 'criacao_comunidades',
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Disponibilizar a criação de comunidades por membros do portal?',
            format="radio",
            label_msgid='ComunidadePratica_label_criacao_comunidades',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),  

    ImageField(
        name='imagem',
        widget=ImageField._properties['widget'](
            label="Imagem",
            description='Imagem padrão para ser utilizada quando não é cadastrada uma imagem para Comunidade.',
            label_msgid='ComunidadePratica_label_imagem',
            description_msgid='ComunidadePratica_help_imagem',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        storage=AnnotationStorage(),
        original_size=(100, 100),
    ), 

    StringField(
        name='twitter',
        widget=StringField._properties['widget'](
            label=u'Termo de busca do Twitter',
            description=u'Digite um termo ou hashtag para buscar e exibir os resultados',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name='facebook',
        widget=StringField._properties['widget'](
            label='Página do Facebook',
            description='Nome da fan-page para exibir o conteúdo',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name='youtube',
        widget=StringField._properties['widget'](
            label=u'Perfil do Youtube',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

),
)

CoPCommunityFolder_schema = ATFolderSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoPCommunityFolder_schema[field].write_permission = "ManagePortal"

class CoPCommunityFolder(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPCommunityFolder)

    meta_type = 'CoPCommunityFolder'
    _at_rename_after_creation = True

    schema = CoPCommunityFolder_schema

    # Methods
    def at_post_create_script(self):
        """ Habilitar/Desabilitar criacao de comunidades
        """
        if self.get_criacao_habilitado():
            self.manage_setLocalRoles('AuthenticatedUsers',['Contributor',])

    def at_post_edit_script(self):
        """ Habilitar/Desabilitar criacao de comunidades
        """
        if self.get_criacao_habilitado():
            self.manage_setLocalRoles('AuthenticatedUsers',['Contributor',])
        else:
            self.manage_setLocalRoles('AuthenticatedUsers',['',])

    security.declarePrivate('get_criacao_habilitado')
    def get_criacao_habilitado(self):
        """Retorna True ou False se a opcao criacao_comunidaes esta habilitada ou desabilitada
        """
        if self.getCriacao_comunidades() == OPCOES[0]:
            return True
        else:
            return False


registerType(CoPCommunityFolder, PROJECTNAME)

@adapter(ICoPCommunityFolder, IObjectInitializedEvent)
def add_portletCoPSocialNetworks(obj, event):
    parent = aq_parent(aq_inner(obj))
    if ICoPCommunityFolder.providedBy(parent):
        return

    column = getUtility(IPortletManager, name=u"plone.rightcolumn")

    manager = getMultiAdapter((obj, column,), IPortletAssignmentMapping)
    assignment = portletCoPSocialNetworks.Assignment(some_field = "Redes Sociais")
    chooser = INameChooser(manager)
    manager[chooser.chooseName(None, assignment)] = assignment
