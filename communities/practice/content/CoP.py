# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from zope.interface import implements
from zope.app.container.interfaces import INameChooser
from zope.component import adapter, getMultiAdapter, getUtility

from Acquisition import aq_inner, aq_parent

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema
from Products.Archetypes.interfaces import IObjectInitializedEvent
from Products.Archetypes.atapi import *

from plone.portlets.interfaces import IPortletManager
from plone.portlets.interfaces import IPortletAssignmentMapping

from communities.practice.interfaces import ICoP
from communities.practice.config import *
from communities.practice.portlets import portletCoPMembers
from communities.practice.generics.generics import getCoPFormsInstalled
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import getRootCoP
from zope.component.hooks import getSite
from Products.CMFPlone.utils import getToolByName

schema = Schema((

    StringField(
        name='title',
        widget=StringField._properties['widget'](
            label=u'Nome da Comunidade',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        accessor="Title",
        searchable=True,
    ),

	TextField(
        name='description',
        widget=TextAreaWidget(
            label=u'Descrição',
            description=u'Escreva sobre o(s) assuntos a serem abordados pela comunidade.',
            label_msgid="ComunidadePratica_label_description",
            description_msgid="ComunidadePratica_help_description",
            i18n_domain='ComunidadePratica',
            maxlength=300,
        ),
        accessor="Description",
        searchable=True,
        required=1
    ),

	LinesField(
        name='subject',
        widget=KeywordWidget(
            label=u'Categorização',
            description=u'Defina Palavras-Chave para sua comunidade.',
            label_msgid="ComunidadePratica_label_categorization",
            description_msgid="ComunidadePratica_help_categorization",
            i18n_domain='ComunidadePratica',
        ),
        accessor="Subject",
        searchable=True,
        multiValued=1
    ),
	
    ImageField(
        name='imagem',
        widget=ImageField._properties['widget'](
            label="Imagem",
            description=u'Imagem simbolo da Comunidade',
            label_msgid='ComunidadePratica_label_imagem',
            description_msgid='ComunidadePratica_help_imagem',
            i18n_domain='ComunidadePratica',
        ),
        storage=AnnotationStorage(),
        original_size=(100, 100),
    ),

    StringField(
        name='twitter',
        widget=StringField._properties['widget'](
            label=u'Termo de busca do Twitter',
            description=u'Digite um termo ou hashtag para buscar e exibir os resultados',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name='facebook',
        widget=StringField._properties['widget'](
            label='Página do Facebook',
            description='Nome da fan-page para exibir o conteúdo',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name='youtube',
        widget=StringField._properties['widget'](
            label=u'Perfil do Youtube',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name= INPUTS_FORM_IDS[5],
        default = OPCOES[1],
        widget=SelectionWidget(
            label=u'Subcomunidades',
            format="radio",
            label_msgid='ComunidadePratica_label_subcop_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),

    IntegerField(
        name= 'subcop_level',
        widget=IntegerField._properties['widget'](
            label=u'Nivel da Comunidade',
            description=u'Nivel da Comunidade na hierarquia',
            label_msgid='ComunidadePratica_label_subcop_level',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name= INPUTS_FORM_IDS[0],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Acervo',
            format="radio",
            label_msgid='ComunidadePratica_label_acervo_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),

    StringField(
        name= INPUTS_FORM_IDS[1],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Calendario',
            format="radio",
            label_msgid='ComunidadePratica_label_calendario_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),
    
    StringField(
        name= INPUTS_FORM_IDS[2],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Forum',
            format="radio",
            label_msgid='ComunidadePratica_label_forum_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),
    
    StringField(
        name=INPUTS_FORM_IDS[3],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Portfolio',
            format="radio",
            label_msgid='ComunidadePratica_label_portfolio_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),
    
    StringField(
        name=INPUTS_FORM_IDS[4],
        default = OPCOES[1],
        widget=SelectionWidget(
            label=u'Tarefas',
            format="radio",
            label_msgid='ComunidadePratica_label_tarefas_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),

    LinesField(
        name='available_forms',
        widget=MultiSelectionWidget(
            label=u'Formulários habilitados nesta comunidade',
            format="checkbox",
            label_msgid='ComunidadePratica_label_tarefas_input',
            i18n_domain='ComunidadePratica',
        ),  
        vocabulary=AVAILABLE_FORMS,
    ),

    StringField(
        name= INPUTS_FORM_IDS[6],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Disponibilizar a opção aos membros efetuarem pedidos para participar de comunidades?',
            format="radio",
            label_msgid='ComunidadePratica_label_participar_input',
            i18n_domain='ComunidadePratica',
        ),  
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ), 

    StringField(
        name= INPUTS_FORM_IDS[7],
        default = OPCOES[0],
        widget=SelectionWidget(
            label=u'Disponibilizar a gestão de participação para os moderadores da comunidade?',
            format="radio",
            label_msgid='ComunidadePratica_label_gestao_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[OPCOES[0],
                    OPCOES[1],]
    ),

),
)

CoP_schema = ATFolderSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoP_schema[field].write_permission = "ManagePortal"

class CoP(ATFolder):
    """CoP type class
    """
    security = ClassSecurityInfo()

    implements(ICoP)

    meta_type = 'CoP'
    _at_rename_after_creation = True

    schema = CoP_schema

    #Hide field subcop_level
    SubCoPField = schema['subcop_level']
    SubCoPField.widget.visible = {'edit': 'invisible', 'view': 'invisible'}

    # Methods
    def __init__(self, id):        
        super(CoP, self).__init__(id)
        self.schema["available_forms"].widget.visible = getCoPFormsInstalled()

    security.declarePrivate('get_participar_habilitado')
    def get_participar_habilitado(self):
        """Retorna True ou False se a opcao participar_input esta habilitada ou desabilitada
        """
        if self.getParticipar_input() == OPCOES[0]:
            return True
        else:
            return False

    security.declarePrivate('get_gestao_habilitado')
    def get_gestao_habilitado(self):
        """Retorna True ou False se a opcao gestao_input esta habilitada ou desabilitada
        """
        if self.getGestao_input() == OPCOES[0]:
            return True
        else:
            return False

    def get_objects_titlemenu(self):
        objects  = {}
        for obj in self.objectValues(): 
            objects[obj.titlemenu] = obj
        return objects

registerType(CoP, PROJECTNAME)

@adapter(ICoP, IObjectInitializedEvent)
def add_portletCoPMembers(obj, event):
    parent = aq_parent(aq_inner(obj))
    if ICoP.providedBy(parent):
        return

    if not isSubCoP(obj):
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")

        manager = getMultiAdapter((obj, column,), IPortletAssignmentMapping)
        assignment = portletCoPMembers.Assignment(some_field = "Participantes")
        chooser = INameChooser(manager)
        manager[chooser.chooseName(None, assignment)] = assignment
