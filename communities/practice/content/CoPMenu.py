# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from AccessControl import Unauthorized
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema

from communities.practice.interfaces import ICoPMenu
from communities.practice.config import *
from communities.practice.generics.generics import canCutObjects


schema = Schema((
    StringField(
        name='titlemenu',
        widget=StringField._properties['widget'](
            label=u'Titulo do Menu',
            label_msgid='communitiespractice_label_titlemenu',
            i18n_domain='communitiespractice',
            visible=False,
        ),  
        accessor="Titlemenu",
        searchable=True,
    ),
),
)


CoPMenu_schema = ATFolderSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoPMenu_schema[field].write_permission = "ManagePortal"

class CoPMenu(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPMenu)

    meta_type = 'CoPMenu'
    _at_rename_after_creation = True

    schema = CoPMenu_schema

    # Methods
    def manage_cutObjects(self, ids=None, REQUEST=None):
        if not canCutObjects(self, ids, REQUEST):
            raise Unauthorized
        return super(CoPMenu, self).manage_cutObjects(ids, REQUEST)

registerType(CoPMenu, PROJECTNAME)
