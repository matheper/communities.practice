# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from AccessControl import Unauthorized
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema

from communities.practice.interfaces import ICoPFolder
from communities.practice.config import *
from communities.practice.generics.generics import canCutObjects


schema = Schema((


),
)


CoPFolder_schema = ATFolderSchema.copy() + \
    schema.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoPFolder_schema[field].write_permission = "ManagePortal"

class CoPFolder(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPFolder)

    meta_type = 'CoPFolder'
    _at_rename_after_creation = True

    schema = CoPFolder_schema

    # Methods
    def manage_cutObjects(self, ids=None, REQUEST=None):
        if not canCutObjects(self, ids, REQUEST):
            raise Unauthorized
        return super(CoPFolder, self).manage_cutObjects(ids, REQUEST)

registerType(CoPFolder, PROJECTNAME)
