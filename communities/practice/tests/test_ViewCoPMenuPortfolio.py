# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents
from zope.component import getMultiAdapter
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME
from zope.security.management import queryInteraction
from zope.security.management import newInteraction, endInteraction
from plone.app.testing import login, logout
from communities.practice.generics.generics import allowTypes
from communities.practice.generics.cache import updateCommunityCache
from Products.CMFPlone.utils import getToolByName

class ViewCoPMenuPortfolioTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMenuPortfolioTestCase, self).setUp()
        self.factoryCoP()
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.portfolio, self.cop, self.copcommunityfolder])
        self.view = getMultiAdapter((self.cop.portfolio, self.request), name='viewCoPMenuPortfolio')
        self.view.__call__()

    def test_ViewCoPMenuPortfolio_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPMenuPortfolio_methods(self):
        """Test method get_participantes
        """
        self.assertFalse(self.view.get_authenticated_user_portfolio())
        self.request.form = {'submit_criar_portfolio':'Criar meu portfolio na Comunidade'}
        self.view.create_portfolio()
        self.assertTrue(self.view.get_authenticated_user_portfolio())
    
    def test_ViewCoPMenuPortfolio_unlock_portfolio(self):
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":'test@test.com.br'})
        self.request.form = {'submit_criar_portfolio':'Criar meu portfolio na Comunidade'}
        self.view.create_portfolio()
        portfolio = self.view.get_authenticated_user_portfolio()
        portfolio_obj = portfolio.getObject()
        portal_workflow = getToolByName(self.cop, "portal_workflow")
        portal_workflow.doActionFor(portfolio_obj, 'privado')
        portal_workflow.doActionFor(portfolio_obj, 'bloqueado')
        portfolio = self.view.get_authenticated_user_portfolio()
        self.assertEqual(portfolio.review_state, 'bloqueado')
        self.request.form = {'submit_desbloquear_portfolio':'Desbloquear portfolio'}
        self.view.unlock_portfolio()
        portfolio = self.view.get_authenticated_user_portfolio()
        self.assertEqual(portfolio.review_state, 'aguardando')

    def test_ViewCoPMenuPortfolio_get_mail_list(self):
        self.request.form = {'submit_criar_portfolio':'Criar meu portfolio na Comunidade'}
        self.view.create_portfolio()
        member = self.portal.portal_membership.getAuthenticatedMember()
        email_1 = 'test@test.com.br'
        email_2 = 'test2@test.com.br'
        member.setMemberProperties(mapping={"email":email_1})
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        updateCommunityCache(self.cop, 'user2', 'Moderador')
        member = self.portal.portal_membership.getMemberById('user2')
        member.setMemberProperties(mapping={"email":email_2})
        mail_list = self.view.get_mail_list()
        self.assertEqual(len(mail_list), 2)
        self.assertIn(email_1, mail_list)
        self.assertIn(email_2, mail_list)

    def test_ViewCoPMenuPortfolio_is_participant(self):
        self.factoryCoP()
        allowTypes(self.cop, ['CoPMenu'])
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.factoryCoPPortfolio()
        #Criacao do conteudo da CoP
        view = getMultiAdapter((self.cop.portfolio, self.request), name='viewCoPMenuPortfolio')
        view.__call__()

        self.assertTrue(view.is_participant())
        logout()
        login(self.portal, 'user2')
        self.assertFalse(view.is_participant())

        logout()
        login(self.portal, TEST_USER_NAME)
        self.cop.manage_setLocalRoles('user2',['Participante'])
        updateCommunityCache(self.cop, 'user2', 'Participante')
        logout()
        login(self.portal, 'user2')
        self.assertTrue(view.is_participant())
