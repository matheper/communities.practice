# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

from zope.component import queryMultiAdapter

class ViewCoPSearchTestCase(IntegrationTestCase):

    def test_ViewCoP_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPSearch')
        self.assertTrue(view is not None)

    def test_ViewCoP_menu(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPSearch')
        view.__call__()
        self.assertEquals(view.cop_menu_status['inicio'],'cop_menu_selected')
