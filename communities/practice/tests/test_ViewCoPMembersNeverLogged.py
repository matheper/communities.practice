# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

class ViewCoPMembersNeverLoggedTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMembersNeverLoggedTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.request.set('PARENTS',[self.copcommunityfolder])
        self.view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPMembersNeverLogged')
        self.view.__call__()        

    def test_ViewCoPMembersNeverLogged_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPMembersNeverLogged_get_members_never_logged(self):
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":"test@test.com.br", "fullname":"test"})
        members_never_logged = self.view.get_members_never_logged()
        self.assertEquals(members_never_logged, [{'id': 'test_user_1_', 'nome': 'test'}])

    def test_ViewCoPMembersNeverLogged_send_mail_members(self):
        """Test method send_mail_notificacao
        """     
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":"test@test.com.br", "fullname":"test"})
        self.request.form = {}
        self.assertEquals(self.view.send_mail_members(), "") 

        self.request.form = {'submit_send_mail_members':'Enviar'}
        self.assertEquals(self.view.send_mail_members(), u"Campos obrigatórios não preenchidos")
     
        self.portal.acl_users.userFolderAddUser('user_sem_email','user_sem_email',[],[])
        self.request.form = {'submit_send_mail_members':'Enviar', 
                             'assunto':'Assunto',
                             'mensagem':'Mensagem',
                             'user_sem_email':'Sim'}
        self.assertEquals(self.view.send_mail_members(), u"Selecione emails para envio.")

        self.request.form = {'submit_send_mail_members':'Enviar',
                             'assunto':'Assunto',
                             'mensagem':'Mensagem',
                             TEST_USER_ID:'Sim'}
        self.assertEquals(self.view.send_mail_members(), u"Email enviado com sucesso.")
