# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPFileTestCase(IntegrationTestCase):

    def test_ViewCoPFile_is_registered(self):
        self.factoryCoPFolder()
        view = queryMultiAdapter((self.copfolder, self.request),
                                  name='viewCoPFolderContents')
        self.assertTrue(view is not None)

    def test_ViewCoPEven_Attributes(self):
        self.factoryCoPImage()
        self.factoryCoPLink()
        self.factoryCoPDocument()
        self.factoryCoPEvent()
        view = queryMultiAdapter((self.copfolder,self.request),
                                  name='viewCoPFolderContents')
        content = view.get_content(3, "modified", "reverse")
        self.assertEqual([brain.Title for brain in content],
                         ['CoPDocument Title', 'CoPLink Title','CoPImage Title'])
        content = view.get_content(3, "modified", "ascending")
        self.assertEqual([brain.Title for brain in content],
                         ['CoPImage Title','CoPLink Title', 'CoPDocument Title'])

        self.factoryCoPFile()
        content = view.get_content(0,"sortable_title")
        self.assertEqual([brain.Title for brain in content],
                        ['CoPDocument Title', 'CoPFile Title',
                         'CoPImage Title', 'CoPLink Title'])

    def test_ViewCoPFolderContents_get_state_name(self):
        self.factoryCoPFile()
        view = queryMultiAdapter((self.copfolder, self.request),
                                  name='viewCoPFolderContents')
        brain = view.get_brain_values(view.get_content())[0]
        state = view.get_state_name(brain['tipo'], brain['estado'])
        self.assertEqual(state, 'Restrito')
