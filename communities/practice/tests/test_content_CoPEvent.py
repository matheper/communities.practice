# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from DateTime import DateTime

class CoPEventTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPEventTestCase, self).setUp()
        self.factoryCoPEvent()

    def test_CoPEvent_addable(self):
        start = DateTime('GMT-3')
        end = DateTime('GMT-3')
        self.copevent.setStartDate(start)
        self.copevent.setEndDate(end)
        self.assertEquals(self.copevent.Title(), "CoPEvent Title")
        self.assertEquals(self.copevent.Description(), "CoPEvent Description")
        self.assertEquals(self.copevent.getLocation(), "CoPEvent Location")
        self.assertEquals(self.copevent.start(), start)
        self.assertEquals(self.copevent.end(), end)
        self.assertEquals(self.copevent.event_url(), "http://www.otics.org/otics")
        self.assertEquals(self.copevent.contact_name(), "CoPEvent Contact")
        self.assertEquals(self.copevent.contact_email(), "contact@mail.com")
