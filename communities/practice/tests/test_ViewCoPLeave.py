# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, queryMultiAdapter
from plone.app.testing import login, logout
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME
from communities.practice.generics.cache import updateCommunityCache

class ViewCoPLeaveTestCase(IntegrationTestCase):

    def test_ViewCoPLeave_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPLeave')
        self.assertTrue(view is not None)

    def test_ViewCoPLeave_send_participation_request(self):
        self.factoryCoP()
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.request.set('submit', 'submit')
        self.request.set('message','message')
        self.request.form = {'message': 'mensagem', 'submit': 'Enviar mensagem'}
        member = self.portal.portal_membership.getMemberById(TEST_USER_ID)
        member.setProperties(email="test@test.com.br")
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPLeave')
        view.__call__()
        view.send_request()
        self.assertEqual(self.cop.get_local_roles_for_userid(TEST_USER_ID),('Owner',))

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        member = self.portal.portal_membership.getMemberById('user2')
        member.setProperties(email="test@test.com.br")
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        updateCommunityCache(self.cop, 'user2', 'Moderador')
        logout()
        login(self.portal, 'user2')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPLeave')
        view.__call__()
        view.send_request()
        self.assertEqual(self.cop.get_local_roles_for_userid('user2'),('Moderador',))

        logout()
        login(self.portal, TEST_USER_NAME)
        self.portal.portal_workflow.doActionFor(self.cop,'publico')
        logout()
        login(self.portal, 'user2')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPLeave')
        view.__call__()
        view.send_request()
        self.assertFalse(self.cop.get_local_roles_for_userid('user2'))
