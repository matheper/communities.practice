# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents

from zope.component import queryMultiAdapter

class ViewCoPMenuTestCase(IntegrationTestCase):

    def test_ViewCoPFolder_is_registered(self):
        self.factoryCoPFolderPortfolio()
        view = queryMultiAdapter((self.copfolderportfolio, self.request), name='viewCoPMenu')
        self.assertTrue(view is not None)

    def test_ViewCoPMenu_addable_types(self):
        self.factoryCoP()
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.portfolio, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.cop.portfolio, self.request), name='viewCoPMenu')
        view.__call__()
        self.assertEqual(view.cop_addable_types_list, [['CoPPortfolio', u'Portfolio']])
