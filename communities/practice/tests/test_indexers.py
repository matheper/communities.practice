# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.indexers import subCoPDepth
from Products.CMFPlone.utils import getToolByName
from plone.app.testing import TEST_USER_ID
from plone.app.discussion.interfaces import IConversation
from zope.component import createObject

class IndexersTestCase(IntegrationTestCase):

    def test_subCoPDepth(self):
        self.factoryCoPMenu()
        self.cop.copmenu.invokeFactory('CoP', 'cop1', title='CoP1', description='CoP1 Description',)
        self.assertEqual(subCoPDepth(self.cop), 0)
        self.assertEqual(subCoPDepth(self.cop.copmenu.cop1), 1)

    def test_copLastModification(self):
        self.factoryCoPDocument()
        self.factoryCoPEvent()
        self.copevent.reindexObject()
        self.copdocument.reindexObject()
        catalog = getToolByName(self.cop, 'portal_catalog')
        brain = catalog(sort_on = 'copLastModification')
        self.assertEqual(brain[0].portal_type, 'CoPDocument')
        self.assertEqual(brain[1].portal_type, 'CoPEvent')
