# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPEventTestCase(IntegrationTestCase):

    def test_ViewCoPEvent_is_registered(self):
        self.factoryCoPEvent()
        view = queryMultiAdapter((self.copevent, self.request), name='viewCoPEvent')
        self.assertTrue(view is not None)

    def test_ViewCoPEvent_Attributes(self):
        self.factoryCoPEvent()
        self.request.set('PARENTS',[self.copevent, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copevent,self.request), name='viewCoPEvent')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
