# -*- coding: utf-8 -*-
from communities.practice.config import OPCOES
from communities.practice.config import INPUTS_FORM_IDS
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import allowTypes
from communities.practice.content.CoP import add_portletCoPMembers
from communities.practice.subscribers import AddContents, editionCreatedContents, transitionParentState
from communities.practice.subscribers import setSubCoPLevel, createCoPGroup, deleteCoPGroup
from communities.practice.tests.CoPUtils import installPseudoProduct
from zope.component import getMultiAdapter, getUtility
from plone.portlets.interfaces import IPortletManager, IPortletAssignmentMapping
from Products.CMFPlone.utils import getToolByName

class CoPTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.workflow = self.workflow_tool.getWorkflowById('communitypractice_CoP_workflow')

    def test_CoP_addable(self):
        self.copcommunityfolder.invokeFactory('CoP', 'cop',
                                              title="CoP Title",
                                              description="CoP Description",
                                              subject=("CoPSubject",),
                                              twitter = "CoP Twitter",
                                              facebook = "CoP Facebook",
                                              youtube = "CoP Youtube",
                                              acervo_input=OPCOES[0],
                                              calendario_input=OPCOES[0],
                                              forum_input=OPCOES[0],
                                              portfolio_input=OPCOES[0],
                                              tarefas_input=OPCOES[1],
                                              participar_input=OPCOES[0],)

        cop = self.copcommunityfolder.cop

        self.assertEquals(cop.Title(), "CoP Title")
        self.assertEquals(cop.Description(), "CoP Description")
        self.assertEquals(cop.Subject(), ("CoPSubject",))
        self.assertEquals(cop.getTwitter(), "CoP Twitter")
        self.assertEquals(cop.getFacebook(), "CoP Facebook")
        self.assertEquals(cop.getYoutube(), "CoP Youtube")
        self.assertEquals(cop.getAcervo_input(), OPCOES[0])
        self.assertEquals(cop.getCalendario_input(), OPCOES[0])
        self.assertEquals(cop.getForum_input(), OPCOES[0])
        self.assertEquals(cop.getPortfolio_input(), OPCOES[0])
        self.assertEquals(cop.getTarefas_input(), OPCOES[1])
        self.assertEquals(cop.getParticipar_input(), OPCOES[0])
        self.assertEquals(cop.getGestao_input(), OPCOES[0])
        self.assertTrue(cop.get_participar_habilitado())
        self.assertTrue(cop.get_gestao_habilitado())
        cop.setParticipar_input(OPCOES[1])
        cop.setGestao_input(OPCOES[1])
        self.assertFalse(cop.get_participar_habilitado())
        self.assertFalse(cop.get_gestao_habilitado())

    def test_CoP_methods(self):
        self.factoryCoP()

        cop = self.copcommunityfolder.cop
        cop_contents = AddContents(cop)

        self.assertTrue(cop_contents.get_habilitar(OPCOES[0]))
        self.assertFalse(cop_contents.get_habilitar(OPCOES[1]))

        allowTypes(cop, [])
        self.assertItemsEqual(cop.getLocallyAllowedTypes(), ())
        allowTypes(cop, ['CoPMenu'])
        self.assertItemsEqual(cop.getLocallyAllowedTypes(), ('CoPMenu',))
        
        installPseudoProduct("cop.forms", "new")

        #Criacao do conteudo da CoP
        editionCreatedContents(cop, False)

        content_obj = cop.get_objects_titlemenu()
        content = content_obj.keys()
        self.assertFalse('subcop' in content)
        self.assertTrue('acervo' in content)
        self.assertTrue('calendario' in content)
        self.assertTrue('portfolio' in content)
        self.assertFalse('tarefas' in content)
        self.assertFalse('formularios' in content)

        cop.setTarefas_input(OPCOES[0])
        cop.setSubcop_input(OPCOES[0])
        installPseudoProduct("cop.forms", "installed")
        cop.setAvailable_forms(('BFCheckList','BFGoodPracticeReport','BFVisitAcknowledge','DABExperience','DABChroniclesEvaluation'))

        cop_contents.edit_content()
        content_obj = cop.get_objects_titlemenu()
        content = content_obj.keys()
        self.assertTrue('tarefas' in content)
        self.assertTrue('subcop' in content)
        self.assertTrue('formularios' in content)

        review_state_cop = self.workflow_tool.getInfoFor(cop, 'review_state')

        #testa o estado atual do conteudo da comunidade
        for input_id in INPUTS_FORM_IDS:
            obj = content_obj.get(input_id[:input_id.find('_')])
            if obj:
                review_state = self.workflow_tool.getInfoFor(obj, 'review_state')
                self.assertEquals(review_state_cop, review_state)
            
        #testa o estado atual do menu formularios
        obj_formularios = content_obj['formularios']
        review_state = self.workflow_tool.getInfoFor(obj_formularios, 'review_state')
        self.assertEquals(review_state_cop, review_state)

        #desabilita tudo
        for input_id in INPUTS_FORM_IDS:
            setattr(cop, input_id, OPCOES[1])
        cop.setAvailable_forms(())
        installPseudoProduct("cop.forms", "new")

        cop_contents.edit_content()
        content_obj = cop.get_objects_titlemenu()
        
        #testa se todos foram inativados
        for input_id in INPUTS_FORM_IDS:
            obj = content_obj.get(input_id[:input_id.find('_')])
            if obj:
                review_state = self.workflow_tool.getInfoFor(obj, 'review_state')
                self.assertEquals(review_state, 'privado')   
            
        #testa o estado atual do menu formularios
        obj_formularios = content_obj['formularios']
        review_state = self.workflow_tool.getInfoFor(obj_formularios, 'review_state')
        self.assertEquals(review_state, 'privado')

        #habilita tudo
        for input_id in INPUTS_FORM_IDS:
            setattr(cop, input_id, OPCOES[0])
        installPseudoProduct("cop.forms", "installed")
        cop.setAvailable_forms(('BFCheckList','BFGoodPracticeReport','BFVisitAcknowledge','DABExperience','DABChroniclesEvaluation'))

        cop_contents.edit_content()        
        content_obj = cop.get_objects_titlemenu()

        #testa se todos passaram para o mesmo estado que a CoP
        for input_id in INPUTS_FORM_IDS:
            obj = content_obj.get(input_id[:input_id.find('_')])
            if obj:
                review_state = self.workflow_tool.getInfoFor(obj, 'review_state')
                self.assertEquals(review_state_cop, review_state)
            
        #testa o estado atual do menu formularios
        obj_formularios = content_obj['formularios']
        review_state = self.workflow_tool.getInfoFor(obj_formularios, 'review_state')
        self.assertEquals(review_state_cop, review_state)

        self.workflow_tool.doActionFor(cop, 'publico')
        transitionParentState(cop.acervo.boas_vindas, False)
        self.assertEqual(self.workflow_tool.getInfoFor(cop.acervo.boas_vindas, 'review_state'), 'publico')

        installPseudoProduct("cop.forms", "new")


    def test_CoP_portlets(self):
        self.factoryCoP()
        self.factoryCoPMenu()
        add_portletCoPMembers(self.copmenu, 'event')
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")
        manager = getMultiAdapter((self.cop, column,), IPortletAssignmentMapping)
        items = manager.items()
        self.assertEqual(len(items),0)

        add_portletCoPMembers(self.cop, 'event')
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")
        manager = getMultiAdapter((self.cop, column,), IPortletAssignmentMapping)
        items = manager.items()
        self.assertEqual(len(items), 1)
        self.assertIn('participantes', items[0])

    def test_setSubCoPLevel(self):
        self.factorySubCoP()
        setSubCoPLevel(self.cop, False)
        self.assertEqual(self.cop.subcop_level,0)
        setSubCoPLevel(self.subcop, False)
        self.assertEqual(self.subcop.subcop_level,1)

    def test_CoP_groups(self):
        self.factoryCoP()
        portal_groups = getToolByName(self.cop, 'portal_groups')
        group_id = self.cop.UID()
        self.assertFalse(group_id in portal_groups.getGroupIds())
        self.assertFalse(group_id in [role_id[0] for role_id in self.cop.get_local_roles()])
        createCoPGroup(self.cop)
        self.assertTrue(group_id in portal_groups.getGroupIds())
        self.assertTrue(group_id, ('Xita',)) in [role_id for role_id in self.cop.get_local_roles()]

        self.factorySubCoP()
        sub_id = self.subcop.UID()
        self.assertFalse(sub_id in portal_groups.getGroupIds())
        self.assertFalse(group_id in [role_id[0] for role_id in self.subcop.get_local_roles()])
        self.assertTrue(group_id, ('Xita',)) in [role_id for role_id in self.subcop.get_local_roles()]
        self.assertFalse(deleteCoPGroup(self.subcop, False))

        self.assertTrue(deleteCoPGroup(self.cop, False))
        self.assertFalse(group_id in portal_groups.getGroupIds())
