# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPUploadTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPUploadTestCase, self).setUp()
        self.factoryCoPTarefas()

    def test_CoPUpload_addable(self):
        self.coptarefas.invokeFactory('CoPUpload', 'copupload',
                                       title="CoPUpload Title",
                                       description="CoPUpload Description",)

        copupload = self.coptarefas.copupload
        
        self.assertEquals(copupload.Title(), "CoPUpload Title")
        self.assertEquals(copupload.Description(), "CoPUpload Description")
