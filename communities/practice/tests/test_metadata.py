# -*- coding: utf-8 -*-

from Products.CMFCore.utils import getToolByName
from StringIO import StringIO
from zptlogo import zptlogo

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.metadata import *

class MetadataTestCase(IntegrationTestCase):

    def setUp(self):
        super(MetadataTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.factoryCoP()
        self.factoryCoPMenu()
        self.factoryCoPFolder()
        self.factoryCoPPortfolio()
        self.factoryCoPTarefas()
        self.factoryCoPUpload()
        self.factoryCoPLink()
        self.factoryCoPImage()
        self.factoryCoPEvent()
        self.factoryCoPFile()
        self.factoryCoPDocument()
        self.factoryCoPATA()
        self.factorySubCoP()

        self.catalog = getToolByName(self.portal, 'portal_catalog')

    def get_search(self, obj, obj_type, metadata="partOfCommunity"):
        search = self.catalog(dict(path={'query':'/'.join(obj.getPhysicalPath())},
                                   portal_type=obj_type,
                                   UID=obj.UID()))
        if search:
            brain = search[0]
            return getattr(brain, metadata, False)
        return False
 
    def test_metadata_installed(self):
        self.assertTrue('partOfCommunity' in self.portal.portal_catalog.schema())
        self.assertTrue('acceptsShares' in self.portal.portal_catalog.schema())
        self.assertTrue('subCoPNumber' in self.portal.portal_catalog.schema())

    def test_metadata_partOfCommunity(self):
        self.assertFalse(partOfCommunity(self.copcommunityfolder))

    def test_metadata_partOfCommunityTypes(self):
        #CoPMenu
        self.assertEquals(self.get_search(self.copmenu,"CoPMenu"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoPFolder
        self.assertEquals(self.get_search(self.copfolder,"CoPFolder"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoPDocument
        self.assertEquals(self.get_search(self.copdocument,"CoPDocument"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoPFile
        self.assertEquals(self.get_search(self.copfile,"CoPFile"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoPLink
        self.assertEquals(self.get_search(self.coplink,"CoPLink"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))
        
        #CoPEvent
        self.assertEquals(self.get_search(self.copevent,"CoPEvent"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))
        
        #CoPImage
        self.assertEquals(self.get_search(self.copimage,"CoPImage"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))
        
        #CoPPortfolio
        self.assertEquals(self.get_search(self.copportfolio,"CoPPortfolio"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))
        
        #CoPUpload
        self.assertEquals(self.get_search(self.copupload,"CoPUpload"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoPATA
        self.assertEquals(self.get_search(self.copata,"CoPATA"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

        #CoP
        self.assertEquals(self.get_search(self.cop,"CoP"), "")

        #SubCoP
        self.assertEquals(self.get_search(self.subcop,"CoP"), (self.cop.UID(), self.cop.Title(), self.cop.absolute_url()))

    def test_metadata_after_change_cop(self):
        self.cop.setId("cop2")
        self.cop.reindexObject()
        self.test_metadata_partOfCommunityTypes()

    def test_acceptsShares(self):
        self.assertTrue(self.get_search(self.cop,"CoP",'acceptsShares'))
        self.cop.setParticipar_input('Desabilitar')
        self.cop.reindexObject()
        self.assertFalse(self.get_search(self.cop,"CoP",'acceptsShares'))

    def test_imageCoPICoP(self):
        stringio = StringIO(zptlogo)
        self.factoryCoP()
        self.factorySubCoP()
        self.copcommunityfolder.reindexObject()
        self.cop.reindexObject()
        self.subcop.reindexObject()
        
        self.assertFalse(self.get_search(self.cop,"CoP",'imageCoP'))
        self.assertFalse(self.get_search(self.subcop,"CoP",'imageCoP'))
        self.copcommunityfolder.setImagem(stringio)
        self.copcommunityfolder.reindexObject()
        self.cop.reindexObject()
        self.subcop.reindexObject()
        self.assertEqual(self.get_search(self.cop,"CoP",'imageCoP'),
                         self.copcommunityfolder.getImagem().absolute_url())
        self.assertEqual(self.get_search(self.subcop,"CoP",'imageCoP'),
                         self.copcommunityfolder.getImagem().absolute_url())
        self.cop.setImagem(stringio)
        self.cop.reindexObject()
        self.subcop.reindexObject()
        self.assertEqual(self.get_search(self.cop,"CoP",'imageCoP'),
                         self.cop.getImagem().absolute_url())
        self.assertEqual(self.get_search(self.subcop,"CoP",'imageCoP'),
                         self.cop.getImagem().absolute_url())

    def test_copSubCoPNumber(self):
        """test for subCoPNumber metadata."""
        self.factoryCoP()
        search = self.catalog(dict(path={'query':'/'.join(self.copcommunityfolder.getPhysicalPath()), 'depth':1},
                                   portal_type='CoP'))
        brain = search[0]
        self.assertEqual(brain.subCoPNumber, 0)
        self.factorySubCoP()
        self.cop.reindexObject()
        search = self.catalog(dict(path={'query':'/'.join(self.copcommunityfolder.getPhysicalPath()), 'depth':1},
                                   portal_type='CoP'))
        brain = search[0]
        self.assertEqual(brain.subCoPNumber, 1)
