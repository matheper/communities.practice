# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, getUtility
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

from plone.portlets.interfaces import IPortletManager, IPortletRenderer, IPortletAssignmentMapping, IPortletType
from plone.app.portlets.portlets import navigation
from zope.app.container.interfaces import INameChooser


from communities.practice.portlets import portletCoPSocialNetworks
from communities.practice.generics.generics import getMemberData

class portletCoPSocialNetworksTestCase(IntegrationTestCase):

    def test_invoke_add_view(self):
        portlet = getUtility(IPortletType, name='communities.practice.portlets.portletCoPSocialNetworks')
        mapping = self.portal.restrictedTraverse('++contextportlets++plone.rightcolumn')
        for m in mapping.keys():
            del mapping[m]
        addview = mapping.restrictedTraverse('+/' + portlet.addview)
        addview.createAndAdd(data={})
        self.assertEquals(len(mapping), 1)
        self.failUnless(isinstance(mapping.values()[0], portletCoPSocialNetworks.Assignment))

    def test_portletCoPSocialNetworks_renderPortlet(self):
        """Render test. """
        self.factoryCoP()
        self.cop.setFacebook('CoPFacebook')
        self.copcommunityfolder.setTwitter('CoPCommunityTwitter')
        self.cop.setYoutube('CoPYoutube')
        self.copcommunityfolder.setYoutube('CoPCommunityYoutube')

        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        manager = getUtility(IPortletManager, name='plone.rightcolumn',context=self.portal)
        assignment = portletCoPSocialNetworks.Assignment('Redes Sociais')
        renderer = getMultiAdapter((self.cop, self.request, view, manager, assignment), IPortletRenderer)

        self.assertEqual(renderer.twitter_user_name,'CoPCommunityTwitter')
        self.assertEqual(renderer.facebook_user_name, 'CoPFacebook')
        self.assertEqual(renderer.youtube_user_name, 'CoPYoutube')
        self.assertEqual(assignment.title, 'Redes Sociais')
        self.assertIsNotNone(renderer.render())


        self.cop.setFacebook('')
        self.cop.setTwitter('')
        self.cop.setYoutube('')
        renderer = getMultiAdapter((self.cop, self.request, view, manager, assignment), IPortletRenderer)
        self.assertEqual(renderer.twitter_user_name,'CoPCommunityTwitter')
        self.assertEqual(renderer.facebook_user_name, '')
        self.assertEqual(renderer.youtube_user_name, 'CoPCommunityYoutube')
