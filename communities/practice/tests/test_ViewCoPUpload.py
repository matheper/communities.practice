# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPUploadTestCase(IntegrationTestCase):

    def test_ViewCoPUpload_is_registered(self):
        self.factoryCoPUpload()
        view = queryMultiAdapter((self.copupload, self.request), name='viewCoPUpload')
        self.assertTrue(view is not None)

    def test_ViewCoPUpload_Attributes(self):
        self.factoryCoPUpload()
        self.request.set('PARENTS',[self.copupload, self.coptarefas, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copupload,self.request), name='viewCoPUpload')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.coptarefas.titlemenu], 'cop_menu_selected')
