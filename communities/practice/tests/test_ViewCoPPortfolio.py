# -*- coding: utf-8 -*-
from plone.app.testing import TEST_USER_ID
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings

from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from communities.practice.generics.cache import updateCommunityCache

class ViewCoPPortfolioTestCase(IntegrationTestCase):

    def test_ViewCoPPortfolio_is_registered(self):
        self.factoryCoPPortfolio()
        view = queryMultiAdapter((self.copportfolio, self.request), name='viewCoPPortfolio')
        self.assertTrue(view is not None)

    def test_ViewCoPPortfolio_Attributes(self):
        self.factoryCoPPortfolio()
        self.request.set('PARENTS',[self.copportfolio, self.copfolderportfolio, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copportfolio,self.request), name='viewCoPPortfolio')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copfolderportfolio.titlemenu], 'cop_menu_selected')

    def test_get_brain_values(self):
        self.factoryCoPPortfolio()
        self.copportfolio.invokeFactory('CoPDocument', 'copdocument',
                                        title="CoPDocument Title",
                                        description="CoPDocument Description",
                                        )
        document_portfolio = self.copportfolio.copdocument
        view = queryMultiAdapter((self.copportfolio, self.request), name='viewCoPPortfolio')
        brain = view.get_content()
        itens = view.get_brain_values(brain)
        self.assertEqual(len(itens), 1)
        self.assertEqual(itens[0]['titulo'], 'CoPDocument Title')

    def test_get_commentaries(self):
        self.factoryCoPPortfolio()
        self.copportfolio.invokeFactory('CoPDocument', 'copdocument',
                                        title="CoPDocument Title",
                                        description="CoPDocument Description",
                                        )
        document_portfolio = self.copportfolio.copdocument
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        view = queryMultiAdapter((self.copportfolio, self.request), name='viewCoPPortfolio')
        self.assertFalse(view.is_discussion_allowed(self.cop.UID()))
        settings.globally_enabled = True
        self.assertTrue(view.is_discussion_allowed(self.cop.UID()))
        conversation = IConversation(document_portfolio)
        comment = createObject('plone.Comment')
        comment.creator = TEST_USER_ID
        conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(document_portfolio.getPhysicalPath()))
        self.assertEqual(len(comments), 1)
        self.assertFalse(view.see_more)
        for i in range(5):
            comment = createObject('plone.Comment')
            comment.creator = TEST_USER_ID
            conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(document_portfolio.getPhysicalPath()), 4)
        self.assertEqual(len(comments), 4)
        self.assertTrue(view.see_more)
        comments = view.get_commentaries('/'.join(document_portfolio.getPhysicalPath()))
        self.assertEqual(len(comments), 6)
        self.assertFalse(view.see_more)
