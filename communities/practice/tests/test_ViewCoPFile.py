# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPFileTestCase(IntegrationTestCase):

    def test_ViewCoPFile_is_registered(self):
        self.factoryCoPFile()
        view = queryMultiAdapter((self.copfile, self.request), name='viewCoPFile')
        self.assertTrue(view is not None)

    def test_ViewCoPEven_Attributes(self):
        self.factoryCoPFile()
        self.request.set('PARENTS',[self.copfile, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copfile,self.request), name='viewCoPFile')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
