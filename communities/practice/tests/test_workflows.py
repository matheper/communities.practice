# -*- coding: utf-8 -*-
from communities.practice.tests.CoPWorkflow import CoPWorkflow
from communities.practice.tests.CoPUtils import get_workflow_xml

class TestWorkflowsTestCase(CoPWorkflow):

    def test_run_tests_CoPATA(self):
        self.configure('communitypractice_CoPATA_workflow', 'CoPATA')
        self.run_tests()

    def test_run_tests_CoPDocument(self):
        self.configure('communitypractice_CoPDocument_workflow', 'CoPDocument')
        self.run_tests()

    def test_run_tests_CoPEvent(self):
        self.configure('communitypractice_CoPEvent_workflow', 'CoPEvent')
        self.run_tests()

    def test_run_tests_CoPFile(self):
        self.configure('communitypractice_CoPFile_workflow', 'CoPFile')
        self.run_tests()

    def test_run_tests_CoPFolder(self):
        self.configure('communitypractice_CoPFolder_workflow', 'CoPFolder')
        self.run_tests()

    def test_run_tests_CoPImage(self):
        self.configure('communitypractice_CoPImage_workflow', 'CoPImage')
        self.run_tests()

    def test_run_tests_CoPLink(self):
        self.configure('communitypractice_CoPLink_workflow', 'CoPLink')
        self.run_tests()

    def test_run_tests_CoPMenu(self):
        self.configure('communitypractice_CoPMenu_workflow', 'CoPMenu')
        self.run_tests()

    def test_run_tests_CoPPortfolio(self):
        self.configure('communitypractice_CoPPortfolio_workflow', 'CoPPortfolio')
        self.run_tests()

    def test_run_tests_CoPUpload(self):
        self.configure('communitypractice_CoPUpload_workflow', 'CoPUpload')
        self.run_tests()

    def test_run_tests_CoP(self):
        self.configure('communitypractice_CoP_workflow', 'CoP')
        self.run_tests()

    def test_run_tests_CoPCommunityFolder(self):
        self.configure('communitypractice_CoPCommunityFolder_workflow', 'CoPCommunityFolder')
        self.run_tests()

    def test_run_tests_CoPShare(self):
        self.configure('communitypractice_CoPShare_workflow', 'CoPShare')
        self.run_tests()
