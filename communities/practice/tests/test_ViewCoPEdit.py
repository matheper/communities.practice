# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents
from zope.component import queryMultiAdapter
from communities.practice.tests.CoPUtils import installPseudoProduct
from communities.practice.config import OPCOES

class ViewCoPMenuTestCase(IntegrationTestCase):

    def test_ViewCoPEdit_menu(self):
        self.factoryCoP()
        installPseudoProduct("cop.forms", "installed")
        self.cop.setAvailable_forms(('BFCheckList','BFGoodPracticeReport','BFVisitAcknowledge','DABExperience','DABChroniclesEvaluation'))
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.portfolio, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.cop.portfolio, self.request), name='viewCoPMenu')
        view.edit_cop_menu()
        self.assertEqual(view.cop_menu_status, {u'calendario': 'cop_menu_item',
                                                u'portfolio': 'cop_menu_selected',
                                                u'acervo': 'cop_menu_item',
                                                u'formularios': 'cop_menu_item',
                                                u'notificacoes': 'cop_menu_item',
                                                u'configuracoes': 'cop_menu_item',
                                                u'forum': 'cop_menu_item',
                                                u'atividade': 'cop_menu_item'})
        view = queryMultiAdapter((self.cop, self.request), name='viewCoP')
        view.edit_cop_menu()
        self.assertEqual(view.cop_menu_status, {'inicio': 'cop_menu_selected'})
        view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolder')
        view.edit_cop_menu()
        self.assertEqual(view.cop_menu_status, {'criar': 'cop_menu_disabled'})
        installPseudoProduct("cop.forms", "new")
