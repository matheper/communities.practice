# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter, getUtility
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

from plone.portlets.interfaces import IPortletManager, IPortletRenderer, IPortletAssignmentMapping, IPortletType
from plone.app.portlets.portlets import navigation
from zope.app.container.interfaces import INameChooser


from communities.practice.portlets import portletCoPMembers
from communities.practice.generics.generics import getMemberData

class ViewCoPMembersTestCase(IntegrationTestCase):

    def testViewCoPMembers_get_cop_members(self):
        """Render test. """
        self.factoryCoP()
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.request.set('ACTUAL_URL',self.cop.absolute_url())

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.cop.manage_setLocalRoles('user3',['Participante'])
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        self.cop.manage_setLocalRoles('user4',['Moderador'])

        self.view = queryMultiAdapter((self.cop, self.request), name='viewCoPMembers')
        self.view.__call__()
        members = self.view.get_cop_members()
        self.assertEqual(len(members),4)

        self.request.form = {'MemberSearchableText':'2'}
        self.view = queryMultiAdapter((self.cop, self.request), name='viewCoPMembers')
        self.view.__call__()
        members = self.view.get_cop_members()
        self.assertEqual(len(members),1)

        self.request.form = {'MemberSearchableText':'nao encontrar'}
        self.view = queryMultiAdapter((self.cop, self.request), name='viewCoPMembers')
        self.view.__call__()
        members = self.view.get_cop_members()
        self.assertEqual(len(members),0)
