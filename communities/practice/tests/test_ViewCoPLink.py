# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPLinkTestCase(IntegrationTestCase):

    def test_ViewCoPLink_is_registered(self):
        self.factoryCoPLink()
        view = queryMultiAdapter((self.coplink, self.request), name='viewCoPLink')
        self.assertTrue(view is not None)

    def test_ViewCoPLink_Attributes(self):
        self.factoryCoPLink()
        self.request.set('PARENTS',[self.coplink, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.coplink,self.request), name='viewCoPLink')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
