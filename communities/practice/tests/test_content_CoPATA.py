# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPATATestCase(IntegrationTestCase):

    def test_CoPATA_addable(self):
        self.factoryCoPMenu()
        self.copmenu.invokeFactory('CoPATA', 'copata',
                                   title="CoPATA Title",
                                   description="CoPATA Description",
                                   pauta="<b>CoPATA</b> Pauta",
                                   discussao="<b>CoPATA</b> Discussao",
                                   encaminhamentos="<b>CoPATA</b> Encaminhamentos",)

        copata = self.copmenu.copata
        self.assertEquals(copata.Title(), "CoPATA Title")
        self.assertEquals(copata.Description(), "CoPATA Description")
        self.assertEquals(copata.getPauta(), "<b>CoPATA</b> Pauta")
        self.assertEquals(copata.getDiscussao(), "<b>CoPATA</b> Discussao")
        self.assertEquals(copata.getEncaminhamentos(), "<b>CoPATA</b> Encaminhamentos")

    def test_CoPATA_methods(self):
        self.factoryCoPATA()
        directory = self.copata.getStartupDirectory()
        self.assertEquals(directory, "/".join(self.copata.getParentNode().getPhysicalPath()))
