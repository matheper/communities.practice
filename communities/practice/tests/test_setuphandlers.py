# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from Products.CMFCore.utils import getToolByName

class SetuphandlersTestCase(IntegrationTestCase):

    def test_createIndexes(self):
        wanted = ['subCoPDepth']
        catalog = getToolByName(self.portal, 'portal_catalog')
        indexes = catalog.indexes()
        for index in wanted:
            self.assertIn(index, indexes)
