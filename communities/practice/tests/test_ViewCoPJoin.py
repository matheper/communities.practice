# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, queryMultiAdapter
from plone.app.testing import login, logout
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME
from communities.practice.generics.cache import updateCommunityCache

class ViewCoPJoinTestCase(IntegrationTestCase):

    def test_ViewCoPJoin_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPJoin')
        self.assertTrue(view is not None)

    def test_ViewCoPJoin_send_participation_request(self):
        self.factoryCoP()
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.request.set('submit', 'Enviar solicitacao')
        self.request.set('message','dsada')
        self.request.form = {'message': 'mensagem', 'submit': 'Enviar solicitacao'}
        member = self.portal.portal_membership.getMemberById(TEST_USER_ID)
        member.setProperties(email="test@test.com.br")
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPJoin')
        view.__call__()
        view.send_participation_request()
        self.assertEqual(self.cop.get_local_roles_for_userid(TEST_USER_ID),('Owner',))

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        member = self.portal.portal_membership.getMemberById('user2')
        member.setProperties(email="test@test.com.br")
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        updateCommunityCache(self.cop, 'user2', 'Moderador')
        self.portal.portal_workflow.doActionFor(self.cop,'publico')
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        logout()
        login(self.portal, 'user3')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPJoin')
        view.__call__()
        view.send_participation_request()
        self.assertEqual(self.cop.get_local_roles_for_userid('user3'),('Participante',))

        logout()
        login(self.portal, TEST_USER_NAME)
        self.portal.portal_workflow.doActionFor(self.cop,'restrito')
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        logout()
        login(self.portal, 'user4')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPJoin')
        view.__call__()
        view.send_participation_request()
        self.assertEqual(self.cop.get_local_roles_for_userid('user4'),('Aguardando',))
