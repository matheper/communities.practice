# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter
from plone.app.testing import login, logout

class ViewCoPFolderTestCase(IntegrationTestCase):

    def test_ViewCoPFolder_is_registered(self):
        self.factoryCoPFolder()
        view = queryMultiAdapter((self.copfolder, self.request), name='viewCoPFolder')
        self.assertTrue(view is not None)

    def test_ViewCoPFolder_Attributes(self):
        self.factoryCoPFolder()
        self.factoryCoPLink()
        self.request.set('PARENTS',[self.copfolder, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copfolder,self.request), name='viewCoPFolder')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')

    def test_can_delete_CoPFolder(self):        
        self.factoryCoPFolder()
        view = queryMultiAdapter((self.copfolder,self.request), name='viewCoPFolder')
        view.__call__()
        view.can_delete_CoPFolder()
        self.assertTrue(view.cop_footer_links.get("Delete", False))
        self.assertTrue(view.cop_footer_links.get("Cut", False))
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        login(self.portal, 'user2')
        view.__call__()
        view.can_delete_CoPFolder()
        self.assertFalse(view.cop_footer_links.get("Delete", False))
        self.assertFalse(view.cop_footer_links.get("Cut", False))
        logout()
