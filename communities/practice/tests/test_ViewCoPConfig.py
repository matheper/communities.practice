# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents
from communities.practice.generics.generics import allowTypes
from communities.practice.subscribers import AddContents
from communities.practice.subscribers import setCoPGroup
from communities.practice.subscribers import createCoPGroup
from communities.practice.config import OPCOES
from zope.component import getMultiAdapter
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME
from plone.app.testing import login, logout
from zope.security.management import queryInteraction
from zope.security.management import newInteraction, endInteraction
from Products.CMFPlone.utils import getToolByName

class ViewCoPConfigTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPConfigTestCase, self).setUp()
        self.factoryCoP()
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.configuracoes, self.cop, self.copcommunityfolder])
        newInteraction()
        interaction = queryInteraction()
        self.view = getMultiAdapter((self.cop.configuracoes, self.request), name='viewCoPConfig')
        self.view.__call__()

    def tearDown(self):
        endInteraction()

    def test_ViewCoPConfig_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPConfig_set_configuracao_email(self):
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":"test@test.com.br"})
        self.request.form = {'submit':'Confirmar', 
                             'cdp_periodicidade_email':'Sim'}
        self.view.set_configuracao_email()
        email_list = self.cop.configuracoes.arquivo_configuracao.getFile().data
        self.assertEqual(email_list, "test@test.com.br\n")
        self.assertTrue(self.view.get_configuracao_email())

        self.request.form = {'submit':'Confirmar'} 
        self.view.set_configuracao_email()
        email_list = self.cop.configuracoes.arquivo_configuracao.getFile().data
        self.assertEqual(email_list, "")
        self.assertFalse(self.view.get_configuracao_email())

    def test_ViewCoPConfig_get_cop_role(self):
        self.addUsersCoP()
        self.cop.manage_setLocalRoles('user_bloqueado',['Bloqueado', 'Observador_Master'])
        self.cop.manage_setLocalRoles('user_moderador',['Moderador', 'Moderador_Master'])
        self.assertEqual(self.view.get_cop_role(TEST_USER_ID),'Owner')
        self.assertEqual(self.view.get_cop_role('user_bloqueado'),'Bloqueado')
        self.assertEqual(self.view.get_cop_role('user_moderador'),'Moderador')

    def test_ViewCoPConfig_get_participantes(self):
        """Test method get_participantes
        """
        self.addUsersCoP()
        participantes = self.view.get_participantes()
        url = self.portal.absolute_url() + '/author/'
        resultado = [
            {'nome': TEST_USER_NAME.capitalize(), 'login': TEST_USER_ID, 'papel': ['Owner', 'Moderador'], 'author': url + TEST_USER_ID}, 
            {'nome': 'User_bloqueado', 'login': 'user_bloqueado', 'papel': ['Bloqueado'], 'author': url + 'user_bloqueado'}, 
            {'nome': 'User_moderador', 'login': 'user_moderador', 'papel': ['Moderador'], 'author': url + 'user_moderador'}, 
            {'nome': 'User_observador', 'login': 'user_observador', 'papel': ['Observador'], 'author': url + 'user_observador'},
            {'nome': 'User_participante', 'login': 'user_participante', 'papel': ['Participante'], 'author': url + 'user_participante'},
        ]

        self.assertEquals(participantes, resultado)

        #Test for subcops
        allowTypes(self.cop, ['CoPMenu'])
        self.factorySubCoP()
        initialCreatedContents(self.subcop,False)
        self.request.set('PARENTS',[self.subcop.configuracoes, self.subcop, self.copmenu, self.cop, self.copcommunityfolder])
        sub_cop_view = getMultiAdapter((self.subcop.configuracoes, self.request), name='viewCoPConfig')
        sub_cop_view.__call__()
        subcop_roles = sub_cop_view.get_participantes()
        resultado = [
            {'nome': TEST_USER_NAME.capitalize(), 'login': TEST_USER_ID, 'papel': ['Owner', 'Moderador', 'Super'], 'author': url + TEST_USER_ID},
            {'nome': 'User_moderador', 'login': 'user_moderador', 'papel': ['Moderador', 'Super'], 'author': url + 'user_moderador'},
            {'nome': 'User_observador', 'login': 'user_observador', 'papel': ['Observador', 'Super'], 'author': url + 'user_observador'},
        ]
        self.assertEquals(subcop_roles, resultado)

    def test_ViewCoPConfig_set_participantes(self):
        """Test method set_participantes
        """            
        self.request.form = {
            'submit_participantes':'Salvar', 
            'user_bloqueado':['Participante', 'Bloqueado'],
            'user_participante':['Moderador', 'Participante'],
            'user_moderador':['Bloqueado', 'Moderador'],
            'user_observador':['Observador', 'Observador'],
        }
        self.addUsersCoP()
        self.view.participantes = []
        self.view.set_participantes()
        local_roles = dict(self.cop.get_local_roles())
        self.assertEquals(local_roles['user_bloqueado'][0], 'Participante')
        self.assertEquals(local_roles['user_participante'][0], 'Moderador')
        self.assertEquals(local_roles['user_moderador'][0], 'Bloqueado')
        self.assertEquals(local_roles['user_observador'][0], 'Observador')
        self.assertEquals(local_roles[TEST_USER_ID][0], 'Owner')
        self.request.form = {'submit_participantes':'Salvar', 'user_participante':'Excluir'}
        self.view.set_participantes()
        local_roles = dict(self.cop.get_local_roles())
        self.assertFalse('user_participantes' in local_roles.keys())


    def test_VewCoPConfig_set_participantes_moderador_subCoP(self):
        self.addUsersCoP()
        allowTypes(self.cop, ['CoPMenu'])
        self.factorySubCoP()
        subcop_content = AddContents(self.subcop)
        subcop_content.create_content()

        self.cop.manage_setLocalRoles('user_nao_participa1',['Participante'])
        local_roles = dict(self.subcop.get_local_roles())
        #teste para moderador e observador herdado da SuperCoP no momento da criacao da SubCoP
        self.assertEquals(local_roles['user_moderador'][0], 'Moderador')
        self.assertEquals(local_roles['user_observador'][0], 'Observador')
        self.request.form = {
            'submit_participantes':'Salvar',
            'user_participante':['Moderador', 'Participante'],
            'user_moderador':['Participante', 'Moderador'],
            'user_observador':['Participante', 'Observador'],
            'user_nao_participa1':['Observador', 'Participante'],
        }
        self.view.set_participantes()

        local_roles = dict(self.cop.get_local_roles())
        #teste para alteracao de papel local
        self.assertEquals(local_roles['user_moderador'][0], 'Participante')
        self.assertEquals(local_roles['user_participante'][0], 'Moderador')
        self.assertEquals(local_roles['user_observador'][0], 'Participante')
        self.assertEquals(local_roles['user_nao_participa1'][0], 'Observador')

        sub_local_roles = dict(self.subcop.get_local_roles())
        #teste para propagacao de moderadores e observadores
        self.assertEquals(sub_local_roles['user_moderador'][0], 'Moderador')
        self.assertEquals(sub_local_roles['user_participante'][0], 'Moderador')
        self.assertEquals(sub_local_roles['user_observador'][0], 'Observador')
        self.assertEquals(sub_local_roles['user_nao_participa1'][0], 'Observador')

        #teste nao deve permitir que superusuario tenha seu papel alterado
        self.view_subcop = getMultiAdapter((self.subcop.configuracoes, self.request), name='viewCoPConfig')
        self.view_subcop.__call__()
        self.request.form = {
            'submit_participantes':'Salvar',
            'user_participante':['Participante', 'Moderador'],
            'user_nao_participa1':['Participante', 'Observador'],
        }
        self.view_subcop.set_participantes()

        subcop_local_roles = dict(self.subcop.get_local_roles())
        self.assertEquals(sub_local_roles['user_participante'][0], 'Moderador')
        self.assertEquals(sub_local_roles['user_nao_participa1'][0], 'Observador')

        #testa se moderador nao perde owner da sub caso vire participante na super
        logout()
        login(self.portal, 'user_participante')
        self.subcopmenu.invokeFactory('CoP','subcop2',
                                    title="SubCoP Title2",
                                    description="SubCoP Description2",
                                    subject=("SubCoPSubject",),
                                    acervo_input=OPCOES[0],
                                    calendario_input=OPCOES[0],
                                    forum_input=OPCOES[0],
                                    portfolio_input=OPCOES[0],
                                    tarefas_input=OPCOES[1],
                                    exibir_todo_conteudo_input=OPCOES[0],
                                    )
        subcop2 = self.subcopmenu.subcop2
        login(self.portal, TEST_USER_NAME)
        self.request.form = {
            'submit_participantes':'Salvar',
            'user_participante':['Moderador', 'Participante'],
        }
        self.view.set_participantes()
        user_roles = subcop2.get_local_roles_for_userid('user_participante')
        self.assertIn('Moderador', user_roles)
        self.assertIn('Owner', user_roles)

    def test_VewCoPConfig_set_participantes_excluir_subCoP(self):
        self.addUsersCoP()
        allowTypes(self.cop, ['CoPMenu'])
        self.factorySubCoP()
        subcop_content = AddContents(self.subcop)
        subcop_content.create_content()
        self.request.form= {'submit_adicionar_participantes':'Adicionar',
                            'user_participante':'Participante',
                           }
        self.view_subcop = getMultiAdapter((self.subcop.configuracoes, self.request), name='viewCoPConfig')
        self.view_subcop.__call__()
        self.view_subcop.set_adicionar_participantes()

        local_roles = dict(self.subcop.get_local_roles())
        self.assertEquals(local_roles['user_participante'][0], 'Participante')
        self.assertEquals(local_roles['user_moderador'][0], 'Moderador')
        self.assertEquals(local_roles['user_observador'][0], 'Observador')
        self.request.form = {
            'submit_participantes':'Salvar', 
            'user_participante':['Excluir', 'Participante'],
            'user_moderador':['Excluir', 'Moderador'],
            'user_observador':['Excluir', 'Observador'],
        }
        self.view_subcop.set_participantes()

        local_roles = dict(self.subcop.get_local_roles())
        self.assertEquals(local_roles['user_moderador'][0], 'Moderador')
        self.assertEquals(local_roles['user_observador'][0], 'Observador')
        self.assertFalse('user_participante' in local_roles.keys())

        self.request.form= {'submit_adicionar_participantes':'Adicionar',
                            'user_participante':'Participante',
                           }
        self.view_subcop.set_adicionar_participantes()
        self.request.form = {
            'submit_participantes':'Salvar',
            'user_participante':['Excluir', 'Participante'],
            'user_moderador':['Excluir', 'Moderador'],
            'user_observador':['Excluir', 'Observador'],
        }
        self.view.set_participantes()

        local_roles = dict(self.cop.get_local_roles())
        self.assertFalse('user_participante' in local_roles.keys())
        self.assertFalse('user_moderador' in local_roles.keys())
        self.assertFalse('user_observador' in local_roles.keys())

        subcop_local_roles = dict(self.subcop.get_local_roles())
        self.assertFalse('user_participante' in subcop_local_roles.keys())
        self.assertFalse('user_moderador' in subcop_local_roles.keys())
        self.assertFalse('user_observador' in subcop_local_roles.keys())

    def test_ViewCoPConfig_get_participacoes_pendentes(self):
        """Test method get_participacoes_pendentes
        """
        self.addUsersCoP()
        aguardando = self.view.get_participacoes_pendentes()
        url = self.portal.absolute_url() + '/author/'
        resultado = [{'nome': 'User_aguarda1', 'login': 'user_aguarda1', 'papel': 'Aguardando', 'author': url + 'user_aguarda1'}, 
                     {'nome': 'User_aguarda2', 'login': 'user_aguarda2', 'papel': 'Aguardando', 'author': url + 'user_aguarda2'}, 
                     {'nome': 'User_aguarda3', 'login': 'user_aguarda3', 'papel': 'Aguardando', 'author': url + 'user_aguarda3'}] 
        self.assertEquals(aguardando, resultado)

    def test_ViewCoPConfig_set_participacoes_pendentes(self):
        """Test method set_participacoes_pendentes
        """            
        self.request.form = {'submit_participacoes_pendentes':'Salvar', 'user_aguarda1':'aguardando', 'user_aguarda2':'aprovar', 'user_aguarda3':'negar', 'refused_message':'Mensagem.'}
        self.addUsersCoP()
        self.cop.at_post_create_script()
        self.view.set_participacoes_pendentes()
        local_roles = dict(self.cop.get_local_roles())
        self.assertEquals(local_roles['user_aguarda1'][0], 'Aguardando')
        self.assertEquals(local_roles['user_aguarda2'][0], 'Participante')
        self.assertFalse('user_aguarda3' in local_roles.keys())
        
    def test_ViewCoPConfig_get_adicionar_participantes(self):
        """Test method get_adicionar_participantes
        """
        self.request.form = {'text_busca_adicionar_participantes':'user_nao_participa'}
        self.addUsersCoP()
        self.cop.manage_setLocalRoles('user_nao_participa1',['Moderador_Master'])
        self.cop.manage_setLocalRoles('user_nao_participa2',['Observador_Master'])
        adicionar = self.view.get_adicionar_participantes()
        url = self.portal.absolute_url() + '/author/'
        resultado = [
            {'nome': 'User_nao_participa1', 'login': 'user_nao_participa1', 'author': url + 'user_nao_participa1'}, 
            {'nome': 'User_nao_participa2', 'login': 'user_nao_participa2', 'author': url + 'user_nao_participa2'}, 
            {'nome': 'User_nao_participa3', 'login': 'user_nao_participa3', 'author': url + 'user_nao_participa3'},
            {'nome': 'User_nao_participa4', 'login': 'user_nao_participa4', 'author': url + 'user_nao_participa4'},
        ] 
        self.assertEquals(adicionar, resultado)

        #verifica lista de usuarios para SubCoPs, somente aqueles que participam da SuperCoP
        allowTypes(self.cop, ['CoPMenu'])
        self.factorySubCoP()
        initialCreatedContents(self.subcop,False)
        self.request.set('PARENTS',[self.subcop.configuracoes, self.subcop, self.copmenu, self.cop, self.copcommunityfolder])
        self.request.form = {'text_busca_adicionar_participantes':'user'}
        sub_cop_view = getMultiAdapter((self.subcop.configuracoes, self.request), name='viewCoPConfig')
        sub_cop_view.__call__()
        user_list = sub_cop_view.get_adicionar_participantes()
        resultado = [
            {'nome': 'User_participante', 'login': 'user_participante', 'author': url +  'user_participante'},
        ]
        self.assertEquals(user_list, resultado)

    def test_ViewCoPConfig_set_adicionar_participantes(self):
        """Test method set_adicionar_participantes
        """
        self.addUsersCoP()
        self.cop.at_post_create_script()
        self.request.form = {
            'submit_adicionar_participantes':'Adicionar', 
            'user_nao_participa1':'Participante', 
            'user_nao_participa3':'Participante',
            'user_nao_participa4':'Observador',
        }
        self.view.set_adicionar_participantes()
        local_roles = dict(self.cop.get_local_roles())
        self.assertEquals(local_roles['user_nao_participa1'][0], 'Participante')
        self.assertEquals(local_roles['user_nao_participa3'][0], 'Participante')
        self.assertEquals(local_roles['user_nao_participa4'][0], 'Observador')
        self.assertFalse('user_nao_participa2' in local_roles.keys())

        self.request.form = {'text_busca_adicionar_participantes':'user_nao_participa', 'botao_busca_adicionar_participantes':'Buscar'}
        adicionar = self.view.get_adicionar_participantes()
        url = self.portal.absolute_url() + '/author/'
        resultado = [{'nome': 'User_nao_participa2', 'login': 'user_nao_participa2', 'author': url + 'user_nao_participa2'}] 
        self.assertEquals(adicionar, resultado)

        allowTypes(self.cop, ['CoPMenu'])
        self.factorySubCoP()
        initialCreatedContents(self.subcop, False)
        self.request.form= {'submit_adicionar_participantes':'Adicionar',
                            'user_aguarda1':'Participante',
                            'user_bloquado':'Participante',
                            'user_participante':'Participante',
                            'user_ausente':'Participante',
                           }
        self.view_subcop = getMultiAdapter((self.subcop.configuracoes, self.request), name='viewCoPConfig')
        self.view_subcop.__call__()
        self.view_subcop.set_adicionar_participantes()
        local_roles = dict(self.subcop.get_local_roles())
        self.assertEquals(local_roles['user_moderador'][0], 'Moderador')
        self.assertEquals(local_roles['user_observador'][0], 'Observador')
        self.assertEquals(local_roles['user_participante'][0], 'Participante')
        self.assertFalse('user_aguarda1' in local_roles.keys())
        self.assertFalse('user_bloqueado' in local_roles.keys())
        self.assertFalse('user_ausente' in local_roles.keys())

    def test_ViewCoPConfig_add_user_group(self):
        """Verifica se usuario e' inserido e removido do grupo de observadores
        """
        self.factoryCoP()
        createCoPGroup(self.cop)
        setCoPGroup(self.cop)
        portal_groups = getToolByName(self.cop, 'portal_groups')
        group_id = self.cop.UID()
        group = portal_groups.getGroupById(group_id)
        self.portal.acl_users.userFolderAddUser('user1','user1',[],[])
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        self.request.form= {'submit_adicionar_participantes':'Adicionar', 'user1':'Participante', 'user2':'Participante'}
        self.view.set_adicionar_participantes()

        self.cop.manage_setLocalRoles('user3',['Aguardando'])
        self.request.form = {'submit_participacoes_pendentes':'Salvar', 'user3':'aprovar', 'user4':'negar', 'refused_message':'Mensagem.'}
        self.view.set_participacoes_pendentes()

        local_roles = dict(self.cop.get_local_roles())
        self.assertEquals(local_roles['user1'][0], 'Participante')
        self.assertEquals(local_roles['user2'][0], 'Participante')
        self.assertEquals(local_roles['user3'][0], 'Participante')
        self.assertFalse('user4' in local_roles.keys())
        group_users = group.getGroupMembers()
        self.assertTrue('user1' in [user.getId() for user in group_users])
        self.assertTrue('user2' in [user.getId() for user in group_users])
        self.assertTrue('user3' in [user.getId() for user in group_users])

        self.request.form = {'submit_participantes':'Salvar', 'user1':['Excluir', 'Participante']}
        self.view.set_participantes()
        local_roles = dict(self.cop.get_local_roles())
        self.assertEquals(local_roles['user2'][0], 'Participante')
        self.assertFalse('user1' in local_roles.keys())
        group_users = group.getGroupMembers()
        self.assertFalse('user1' in [user.getId() for user in group_users])
        self.assertTrue('user2' in [user.getId() for user in group_users])


    def addUsersCoP(self):
        """Adds users and add them to CoP
        """
        self.portal.acl_users.userFolderAddUser('user_participante','user_participante',[],[])
        self.cop.manage_setLocalRoles('user_participante',['Participante'])
        self.portal.acl_users.userFolderAddUser('user_moderador','user_moderador',[],[])
        self.cop.manage_setLocalRoles('user_moderador',['Moderador'])
        self.portal.acl_users.userFolderAddUser('user_bloqueado','user_bloqueado',[],[])
        self.cop.manage_setLocalRoles('user_bloqueado',['Bloqueado'])
        self.portal.acl_users.userFolderAddUser('user_aguarda1','user_aguarda1',[],[])
        self.cop.manage_setLocalRoles('user_aguarda1',['Aguardando'])
        self.portal.acl_users.userFolderAddUser('user_aguarda2','user_aguarda2',[],[])
        self.cop.manage_setLocalRoles('user_aguarda2',['Aguardando'])
        self.portal.acl_users.userFolderAddUser('user_aguarda3','user_aguarda3',[],[])
        self.cop.manage_setLocalRoles('user_aguarda3',['Aguardando'])
        self.portal.acl_users.userFolderAddUser('user_observador','user_observador',[],[])
        self.cop.manage_setLocalRoles('user_observador',['Observador'])
        self.portal.acl_users.userFolderAddUser('user_nao_participa1','user_nao_participa1',[],[])
        self.portal.acl_users.userFolderAddUser('user_nao_participa2','user_nao_participa2',[],[])
        self.portal.acl_users.userFolderAddUser('user_nao_participa3','user_nao_participa3',[],[])
        self.portal.acl_users.userFolderAddUser('user_nao_participa4','user_nao_participa4',[],[])
        self.portal.acl_users.userFolderAddUser('user_outro','user_outro',[],[])
