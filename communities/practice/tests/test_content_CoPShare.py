# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPShareTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPShareTestCase, self).setUp()
        self.factoryCoPShare()

    def test_CoPShare_addable(self):
        self.assertEquals(self.copshare.Title(), "CoPShare Title")
        self.assertEquals(self.copshare.Description(), "CoPShare Description")
        self.assertEquals(self.copshare.getRemoteUrl(), "http://www.otics.org/otics")
