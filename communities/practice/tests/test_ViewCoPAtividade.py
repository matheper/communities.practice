# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import add_users_CoP, create_content_atividade
from communities.practice.subscribers import initialCreatedContents
from communities.practice.subscribers import editionCreatedContents
from communities.practice.config import OPCOES
from communities.practice.generics.atividade_generics import TYPES_ATIVIDADE
from communities.practice.tests.CoPUtils import ALL_MEMBERS, FILTERED_MEMBERS, installPseudoProduct
from zope.component import queryMultiAdapter
from zope.security.management import queryInteraction
from zope.security.management import newInteraction, endInteraction

class ViewCoPAtividadeTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPAtividadeTestCase, self).setUp()
        self.factoryCoP()
        self.cop.setTarefas_input(OPCOES[0])
        initialCreatedContents(self.cop, False)
        add_users_CoP(self.portal, self.cop)
        create_content_atividade(self.cop)
        self.request.set('PARENTS',[self.cop.atividade, self.cop, self.copcommunityfolder])
        newInteraction()
        interaction = queryInteraction()
        self.view = queryMultiAdapter((self.cop.atividade, self.request), name='viewCoPAtividade')
        self.view.__call__() 

    def tearDown(self):
        endInteraction()

    def test_ViewCoPAtividade_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPAtividade_get_community_members(self):
        input_filter = "MemberSearchableText"
        self.request.form = {}
        member_list = self.view.get_community_members(input_filter)
        self.assertItemsEqual(member_list, ALL_MEMBERS)

        self.request.form = {input_filter : "user_participante"}
        member_list = self.view.get_community_members(input_filter)
        self.assertItemsEqual(member_list, FILTERED_MEMBERS)

        input_filter = "MemberSearchableTextAcervo"
        self.request.form = {input_filter : "user_participante"}
        member_list = self.view.get_community_members(input_filter)
        self.assertItemsEqual(member_list, FILTERED_MEMBERS)

        input_filter = "MemberSearchableTextPortfolio"
        self.request.form = {input_filter : "user_participante"}
        member_list = self.view.get_community_members(input_filter)
        self.assertItemsEqual(member_list, FILTERED_MEMBERS)

        input_filter = "MemberSearchableTextTarefas"
        self.request.form = {input_filter : "user_participante"}
        member_list = self.view.get_community_members(input_filter)
        self.assertItemsEqual(member_list, FILTERED_MEMBERS)

    def test_ViewCoPAtividade_get_community_content(self):
        member_list = self.view.get_community_members(self.cop)

        folder = ""
        content = self.view.get_community_content(member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(content[('user_participante1','user_participante1')][('total','Total')], 4)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPDocument',u'Páginas')], 2)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPFile',u'Arquivos')], 1)

        folder = "acervo"
        content = self.view.get_community_content(member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(content[('user_participante1','user_participante1')][('total','Total')], 1)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPDocument',u'Páginas')], 1)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPFile',u'Arquivos')], 0)

        folder = "portfolio"
        content = self.view.get_community_content(member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(content[('user_participante1','user_participante1')][('total','Total')], 1)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPDocument',u'Páginas')], 1)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPFile',u'Arquivos')], 0)

        folder = "tarefas"
        content = self.view.get_community_content(member_list, folder)
        self.assertItemsEqual(content.keys(), ALL_MEMBERS)
        self.assertEqual(content[('user_participante1','user_participante1')][('total','Total')], 1)
        self.assertEqual(content[('user_participante1','user_participante1')][('CoPFile',u'Arquivos')], 1)        

    def test_ViewCoPAtividade_get_types(self):
        folder = ""
        types = self.view.get_types(folder)
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')])

        folder = "acervo"
        types = self.view.get_types(folder)
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')])

        folder = "portfolio"
        types = self.view.get_types(folder)
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get(folder) + [('total', u'Total')])

        folder = "tarefas"
        types = self.view.get_types(folder)
        self.assertItemsEqual(types, TYPES_ATIVIDADE.get(folder))

    def test_ViewCoPAtividade_get_total_types(self):
        folder = ""
        total_types = self.view.get_total_types(folder)
        self.assertEqual(total_types[('total', 'Total')], 5)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 3)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)

        folder = "acervo"
        total_types = self.view.get_total_types(folder)
        self.assertEqual(total_types[('total', 'Total')], 2)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 2)

        folder = "portfolio"
        total_types = self.view.get_total_types(folder)
        self.assertEqual(total_types[('total', 'Total')], 1)
        self.assertEqual(total_types[('CoPDocument', u'Páginas')], 1)

        folder = "tarefas"
        total_types = self.view.get_total_types(folder)
        self.assertEqual(total_types[('CoPFile', u'Arquivos')], 1)

    def test_ViewCoPAtividade_get_tabs(self):
        installPseudoProduct("cop.forms", "new")
        self.cop.setAvailable_forms(())
        tabs = self.view.get_tabs()
        self.assertItemsEqual(tabs, [['comunidade', u'Comunidade'], ['acervo', u'Acervo'], ['portfolio', u'Portfolio'], ['forum', u'Fórum'], ['tarefas', u'Tarefas']])

        self.cop.setTarefas_input(OPCOES[1])
        editionCreatedContents(self.cop, False) 
        tabs = self.view.get_tabs()
        self.assertItemsEqual(tabs, [['comunidade', u'Comunidade'], ['acervo', u'Acervo'], ['portfolio', u'Portfolio'], ['forum', u'Fórum']])

        installPseudoProduct("cop.forms", "installed")
        self.cop.setAvailable_forms(('BFCheckList','BFGoodPracticeReport','BFVisitAcknowledge','DABExperience','DABChroniclesEvaluation'))
        editionCreatedContents(self.cop, False)
        tabs = self.view.get_tabs()
        self.assertItemsEqual(tabs, [['comunidade', u'Comunidade'], ['acervo', u'Acervo'], ['portfolio', u'Portfolio'], ['forum', u'Fórum'], ['formularios', u'Formulários']])

        installPseudoProduct("cop.forms", "new")

    def test_ViewCoPAtividade_get_len_members(self):
        len_members = self.view.get_len_members()
        self.assertEqual(len_members, 5)
