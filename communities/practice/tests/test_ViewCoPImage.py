# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPImageTestCase(IntegrationTestCase):

    def test_ViewCoPImage_is_registered(self):
        self.factoryCoPImage()
        view = queryMultiAdapter((self.copimage, self.request), name='viewCoPImage')
        self.assertTrue(view is not None)

    def test_ViewCoPImage_Attributes(self):
        self.factoryCoPImage()
        self.request.set('PARENTS',[self.copimage, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copimage,self.request), name='viewCoPImage')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
