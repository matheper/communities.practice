# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import add_users_CoP, create_content_atividade
from communities.practice.tests.CoPUtils import add_users_CoP, create_content_atividade
from communities.practice.subscribers import initialCreatedContents
from communities.practice.config import OPCOES
from zope.component import queryMultiAdapter
from communities.practice.generics.atividade_generics import getCoPAtividadeTypes
from DateTime import DateTime
from datetime import datetime
from datetime import timedelta
    
class ViewCoPConteudoAtividadeTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPConteudoAtividadeTestCase, self).setUp()
        self.factoryCoP()
        self.cop.setTarefas_input(OPCOES[0])
        initialCreatedContents(self.cop, False)
        add_users_CoP(self.portal, self.cop)
        create_content_atividade(self.cop)
        self.request.set('PARENTS',[self.cop.atividade, self.cop, self.copcommunityfolder])
        self.request.set('user','')
        self.request.set('type',"('','')")
        self.request.set('folder','')
        self.request.set('period_start','')
        self.request.set('period_end','')
        self.view = queryMultiAdapter((self.cop.atividade, self.request), name='viewCoPConteudoAtividade')

    def test_ViewCoPConteudoAtividade_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPConteudoAtividade_get_conteudo(self):
        self.cop.setAvailable_forms(('BFCheckList','BFGoodPracticeReport','BFVisitAcknowledge','DABExperience','DABChroniclesEvaluation'))
        portal_catalog = self.cop.portal_catalog
        query = {}
        query['path'] = "/".join(self.cop.getPhysicalPath())
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("", False)]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)

        #total de conteudos na comunidade
        self.request.set('type',"('total', u'Total')")
        conteudo = self.view.get_conteudo()
        #desconta 1 do arquivo de configuracao
        self.assertEqual(len(conteudo), len(conteudo_catalog) - 1)

        #total no acervo
        self.request.set('folder', 'acervo')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("acervo", False)]
        query['portal_type'] = tipos_consulta
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'acervo')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total no acervo com periodo
        old_date = self.cop.acervo.created()
        self.cop.acervo.boas_vindas.setCreationDate(DateTime(datetime.now() - timedelta(days=1)))
        self.cop.acervo.boas_vindas.reindexObject()
        folder = ""
        today = datetime.today()
        today = DateTime("%s/%s/%s" % (today.year,today.month,today.day))
        start_period = today
        end_period = today + 1
        query['created'] = {"query": [start_period,end_period], 'range':'min:max'}
        conteudo_catalog = portal_catalog(query)
        self.request.set("period_start", "%s-%s-%s 00:00" % (start_period.year(), start_period.mm(), start_period.dd()))
        self.request.set("period_end", "%s-%s-%s 00:00" % (end_period.year(), end_period.mm(), end_period.dd()))
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))
        query.pop('created')
        self.request.set('period_start', '')
        self.request.set('period_end', '')
        self.cop.acervo.boas_vindas.setCreationDate(old_date)
        self.cop.acervo.boas_vindas.reindexObject()

        #total no portfolio
        self.request.set('folder', 'portfolio')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("portfolio", False)]
        query['portal_type'] = tipos_consulta
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'portfolio')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total nas tarefas
        self.request.set('folder', 'tarefas')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("tarefas", False)]
        query['portal_type'] = tipos_consulta
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'tarefas')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo na comunidade
        self.request.set('folder', '')
        self.request.set('type', "('CoPDocument', u'Página')")
        query['path'] = "/".join(self.cop.getPhysicalPath())
        query['portal_type'] = "CoPDocument"
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo no acervo
        self.request.set('folder', 'acervo')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'acervo')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo no portfolio
        self.request.set('folder', 'portfolio')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'portfolio')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo nas tarefas
        self.request.set('folder', 'tarefas')
        self.request.set('type', "('CoPFile', u'Arquivos')")
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'tarefas')
        query['portal_type'] = "CoPFile"
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))
         
        #total de member na comunidade
        self.request.set('folder', '')
        self.request.set('type', "('total', u'Total')")
        self.request.set('user', 'user_participante1')
        query['path'] = "%s" % ("/".join(self.cop.getPhysicalPath()))
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("", False)]
        query['portal_type'] = tipos_consulta
        query['Creator'] = "user_participante1"
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de member no acervo
        self.request.set('folder', 'acervo')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'acervo')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("acervo", False)]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de member no portfolio
        self.request.set('folder', 'portfolio')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'portfolio')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("portfolio", False)]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de member nas tarefas
        self.request.set('folder', 'tarefas')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'tarefas')
        tipos_consulta = [tipo[0] for tipo in getCoPAtividadeTypes("tarefas", False)]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo member na comunidade
        self.request.set('folder', '')
        self.request.set('type', "('CoPDocument', u'Páginas')")
        query['path'] = "%s" % ("/".join(self.cop.getPhysicalPath()))
        query['portal_type'] = "CoPDocument"
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo member no acervo
        self.request.set('folder', 'acervo')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'acervo')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo member no portfolio
        self.request.set('folder', 'portfolio')
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'portfolio')
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        #total de tipo member nas tarefas
        self.request.set('folder', 'tarefas')
        self.request.set('type', "('CoPFile', u'Arquivos')")
        query['path'] = "%s/%s" % ("/".join(self.cop.getPhysicalPath()), 'tarefas')
        query['portal_type'] = 'CoPFile'
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))
