# -*- coding:utf-8 -*-
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.generics.generics import setSuperCoPRoles
from communities.practice.subscribers import initialCreatedContents
from communities.practice.config import OPCOES
from zope.component import createObject
from Products.CMFPlone.utils import _createObjectByType
from plone.app.discussion.interfaces import IConversation
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from StringIO import StringIO
from zptlogo import zptlogo
from zope.component.hooks import getSite
from Products.CMFQuickInstallerTool.InstalledProduct import InstalledProduct
from xml.dom import minidom
from xml.dom.minidom import  Element
import os
from plone.app.testing import login, logout

ALL_MEMBERS = [('user_participante1', 'user_participante1'), ('user_participante2', 'user_participante2'), ('user_participante3', 'user_participante3'), ('user_moderador', 'user_moderador'), ('test_user_1_', 'test_user_1_')]

FILTERED_MEMBERS = [('user_participante1', 'user_participante1'), ('user_participante2', 'user_participante2'), ('user_participante3', 'user_participante3')]

def get_workflow_xml(workflow_file, padrao=True):

    if padrao:
        tags = dict(workflow = {'tag':'dc-workflow', 'attr':'name'},
                    state = {'tag':'state', 'attr':'state_id'},
                    permission = {'tag':'permission-map', 'attr':'name'},
                    roles = {'tag':'permission-role', 'attr':''})
    else:
        tags = dict(workflow = {'tag':'workflow', 'attr':'name'},
                    state = {'tag':'state', 'attr':'name'},
                    permission = {'tag':'permission', 'attr':'name'},
                    roles = {'tag':'role', 'attr':'name'})

    PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
    try:
        doc = minidom.parse(("%s/%s") % (PROJECT_PATH, workflow_file))
    except:
        return False
    workflow_name = doc.getElementsByTagName(tags['workflow']['tag'])[0].getAttribute(tags['workflow']['attr'])
    permission_roles = {}
    states_permission_roles = {}
    role_list = []

    for state in doc.getElementsByTagName(tags['state']['tag']):
        for permission in state.getElementsByTagName(tags['permission']['tag']):
            for role in permission.getElementsByTagName(tags['roles']['tag']):
                role_list.append(role.childNodes[0].data)
            permission_roles[permission.getAttribute(tags['permission']['attr'])] = tuple(role_list)
            role_list = []
        states_permission_roles[state.getAttribute(tags['state']['attr'])] = permission_roles
        permission_roles = {}
    return dict(workflow_name=workflow_name, states_permission_roles=states_permission_roles)


def createStructure(portal, initial_roles_file):
    """ Cria usuários e hierarquia de Comunidades.
        Adiciona local roles aos usuarios nos contextos.
    """

    PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
    file_name = ("%s/%s") % (PROJECT_PATH, initial_roles_file)
    try:
        doc = minidom.parse(file_name)
    except:
        return False

    users_node = doc.getElementById('users')
    createUsers(portal, users_node)

    portal_node = doc.getElementById('portal')
    clearLocalRoles(portal, portal_node, 0)
    createObjectsInContext(portal, portal_node, 0)

def createUsers(portal, users_node):
    for user_node in users_node.getElementsByTagName('user'):
        user_id = user_node.getAttribute('id')
        user_name = user_node.getAttribute('name')
        user_mail = user_node.getAttribute('email')
        portal.acl_users.userFolderAddUser(user_id, user_name,[],[])
        user = portal.portal_membership.getMemberById(user_id)
        user.setMemberProperties(mapping={"email":user_mail})

def createObjectsInContext(context, current_node, depth):

    portal = getSite()
    users = [child for child in current_node.childNodes\
             if isinstance(child, Element) and child.tagName == 'user']

    if context.portal_type == 'CoP':
        setSuperCoPRoles(context)
    for user in users:
        user_id = user.getAttribute('id')
        roles = user.getElementsByTagName('role')
        local_roles = list(context.get_local_roles_for_userid(user_id))
        local_roles += [role.firstChild.nodeValue for role in roles]
        context.manage_setLocalRoles(user_id, local_roles)

    for element_node in current_node.getElementsByTagName('element'):
        if element_node.getAttribute('depth') == str(depth):
            old_creator = ''
            element_id = element_node.getAttribute('id')
            if element_id in context.objectIds():
                new_element = context[element_id]
            elif context.portal_type == 'CoP' and element_id in context.subcop.objectIds():
                new_element = context.subcop[element_id]
            else:
                element_type = element_node.getAttribute('type')
                new_creator = element_node.getAttribute('creator')
                if new_creator:
                    old_creator = portal.portal_membership.getAuthenticatedMember().getUserName()
                    logout()
                    login(portal, new_creator)
                if context.portal_type == 'Plone Site':
                    new_element = _createObjectByType(
                        'CoPCommunityFolder',
                        context,
                        id=element_id,
                        title=u'%s name'%(element_id),
                        description=u'%s description'%(element_id),
                    )
                else:
                    if context.portal_type == 'CoP':
                        context = context.subcop

                    new_element = _createObjectByType(
                        'CoP',
                        context,
                        id=element_id,
                        title=u'%s name'%(element_id),
                        description=u'%s description'%(element_id),
                        subject=("SubCoPSubject",),
                        acervo_input=OPCOES[0],
                        subcop_input=OPCOES[0],
                        calendario_input=OPCOES[0],
                        forum_input=OPCOES[0],
                        portfolio_input=OPCOES[0],
                        tarefas_input=OPCOES[1],
                        exibir_todo_conteudo_input=OPCOES[0],
                    )
                    initialCreatedContents(new_element, False)
            createObjectsInContext(new_element, element_node, depth + 1)
            if old_creator:
                logout()
                login(portal, old_creator)

def clearLocalRoles(context, current_node, depth):
    for element_node in current_node.getElementsByTagName('element'):
        if element_node.getAttribute('depth') == str(depth):
            element_id = element_node.getAttribute('id')
            element_creator_id = element_node.getAttribute('creator')
            element = None
            if element_id in context.objectIds():
                element = context[element_id]
            elif context.portal_type == 'CoP' and element_id in context.subcop.objectIds():
                element = context.subcop[element_id]
            if element:
                for user, roles in element.get_local_roles():
                    element.manage_delLocalRoles([user])
                #reset ownership
                portal = getSite()
                if element_creator_id:
                    old_creator = portal.portal_membership.getAuthenticatedMember().getUserName()
                    logout()
                    login(portal, element_creator_id)
                creator = portal.portal_membership.getAuthenticatedMember()
                element.setCreators(creator.getId())
                element.changeOwnership(creator.getUser(), recursive=False)
                roles = ['Owner', 'Moderador']
                element.manage_setLocalRoles(creator.getId(), roles)
                clearLocalRoles(element, element_node, depth + 1)
                if element_creator_id:
                    logout()
                    login(portal, old_creator)


def add_users_CoP(portal, cop):
    """Adds users and add them to CoP
    """
    portal.acl_users.userFolderAddUser('user_moderador','user_moderador',[],[])
    cop.manage_setLocalRoles('user_moderador',['Moderador'])
    updateCommunityCache(cop, 'user_moderador', 'Moderador')
    portal.acl_users.userFolderAddUser('user_participante1','user_participante1',[],[])
    cop.manage_setLocalRoles('user_participante1',['Participante'])
    updateCommunityCache(cop, 'user_participante1', 'Participante')
    portal.acl_users.userFolderAddUser('user_participante2','user_participante2',[],[])
    cop.manage_setLocalRoles('user_participante2',['Participante'])
    updateCommunityCache(cop, 'user_participante2', 'Participante')
    portal.acl_users.userFolderAddUser('user_participante3','user_participante3',[],[])
    cop.manage_setLocalRoles('user_participante3',['Participante'])
    updateCommunityCache(cop, 'user_participante3', 'Participante')

def create_content_atividade(cop):
    """Creates content for tests
    """
    cop.acervo.invokeFactory('CoPDocument', 
                             'copdocument',
                             title="CoPDocument Title",
                             description="CoPDocument Description",) 

    registry = queryUtility(IRegistry)
    settings = registry.forInterface(IDiscussionSettings)
    settings.globally_enabled = True
    conversation = IConversation(cop.acervo.copdocument)
    comment = createObject('plone.Comment')
    comment.creator = 'user_participante1'
    conversation.addComment(comment)

    cop.portfolio.invokeFactory('CoPPortfolio', 'copportfolio',
                                title="CoPPortfolio Title",
                                description="CoPPortfolio Description",)
    cop.portfolio.copportfolio.invokeFactory('CoPDocument', 
                                             'copdocument',
                                             title="CoPDocument Title",
                                             description="CoPDocument Description",) 

    cop.tarefas.invokeFactory('CoPUpload', 'copupload',
                              title="CoPUpload Title",
                              description="CoPUpload Description",)
    stringio = StringIO(zptlogo)
    cop.tarefas.copupload.invokeFactory('CoPFile',
                                        'copfile',
                                        description="CoPFile Description",
                                        file=stringio,)



    for obj in [cop.acervo.copdocument, cop.portfolio.copportfolio.copdocument, cop.tarefas.copupload.copfile,]:
        obj.setCreators(['user_participante1'])
        obj.reindexObject()

def installPseudoProduct(product_name, status):
    portal = getSite()
    qi = getattr(portal, 'portal_quickinstaller')
    product = qi._getOb(product_name, None)
    if not product:
        full_product_name = "/plone/portal_quickinstaller/%s" % product_name
        product = InstalledProduct(full_product_name)
        qi._setOb(product_name, product)
    product.status = status
