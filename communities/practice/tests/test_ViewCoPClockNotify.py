# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents, editionCreatedContents
from zope.component import getMultiAdapter
from datetime import datetime, timedelta
from DateTime import DateTime
from StringIO import StringIO
from zptlogo import zptlogo
from plone.app.testing import TEST_USER_ID
from StringIO import StringIO
from zptlogo import zptlogo
from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings

class ViewCoPClockNotifyTestCase(IntegrationTestCase):

    def test_ViewCoPClockNotify_is_registered(self):
        view = getMultiAdapter((self.portal, self.request), name='viewCoPClockNotify')
        self.assertTrue(view is not None)

    def test_ViewCoPClockNotify(self):
        self.factoryCoP()
        self.factoryCoPDocument()
        self.factoryCoPEvent()
        view = getMultiAdapter((self.portal, self.request), name='viewCoPClockNotify')
        view.sendMail()
        self.cop.setDescription = ""
        self.assertFalse(view.getHTMLMessage(self.cop))

        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={'email':'test@test.com.br'})

        date = datetime.now()
        date = DateTime("%s/%s/%s" % (date.year,date.month,date.day)) - 1
        self.copdocument.setCreationDate(date)
        self.copevent.setCreationDate(date)
        self.copevent.setDescription("")
        self.copdocument.reindexObject()
        self.copevent.reindexObject()

        view = getMultiAdapter((self.portal, self.request), name='viewCoPClockNotify')
        view.sendMail()
        news = view.getNovidades(self.cop)
        self.assertEqual(news['Eventos'], [{'url': self.copevent.absolute_url(),
                                            'titulo': 'CoPEvent Title',
                                            'criador': TEST_USER_ID,
                                            'descricao': ''}])
        self.assertEqual(news['Documentos'], [{ 'url': self.copdocument.absolute_url() + '/view',
                                                'titulo': 'CoPDocument Title',
                                                'criador': TEST_USER_ID,
                                                'descricao': 'CoPDocument Description'}])
        self.assertEqual(news['Forum'], [])

        self.cop.setDescription = "CoP Description"
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.factoryCoPDocument()
        self.factoryCoPEvent()

        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)

        email_list = "test@test.com.br\ntest2@test.com.br\ntest3@test.com.br\ntest4@test.com.br\ntest5@test.com.br"
        self.cop.configuracoes.arquivo_configuracao.setFile(email_list)

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.cop.manage_setLocalRoles('user3',['Moderador_Master'])
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        self.cop.manage_setLocalRoles('user4',['Moderador'])
        self.portal.acl_users.userFolderAddUser('user5','user5',[],[])

        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={'email':'test@test.com.br'})
        member = self.portal.portal_membership.getMemberById('user2')
        member.setMemberProperties(mapping={'email':'test2@test.com.br'})
        member = self.portal.portal_membership.getMemberById('user3')
        member.setMemberProperties(mapping={'email':'test3@test.com.br'})
        member = self.portal.portal_membership.getMemberById('user4')
        member.setMemberProperties(mapping={'email':'test4@test.com.br'})
        member = self.portal.portal_membership.getMemberById('user5')
        member.setMemberProperties(mapping={'email':'test5@test.com.br'})

        date = datetime.now()
        date = DateTime("%s/%s/%s" % (date.year,date.month,date.day)) - 1
        self.copdocument.setCreationDate(date)
        self.copevent.setCreationDate(date)

        stringio = StringIO(zptlogo)
        self.copfolder.invokeFactory('CoPImage', 'copimage2',
                                    title='',
                                    description='',
                                    image=stringio,)
        self.copimage2 = self.copfolder.copimage2
        self.copimage2.setCreationDate(date)

        self.copdocument.reindexObject()
        self.copevent.reindexObject()
        self.copimage2.reindexObject()

        view = getMultiAdapter((self.portal, self.request), name='viewCoPClockNotify')
        view.sendMail()
        news = view.getNovidades(self.cop)
        self.assertEqual(news['Eventos'], [{'url': self.copevent.absolute_url(),
                                            'titulo': 'CoPEvent Title',
                                            'criador': TEST_USER_ID,
                                            'descricao': ''}])
        self.assertIn({'url': self.copimage2.absolute_url() + '/view',
                                                 'titulo': 'copimage2',
                                                 'criador': 'test_user_1_',
                                                 'descricao': ''},
                      news['Documentos'])
        self.assertIn({'url': self.copdocument.absolute_url() + '/view',
                                                 'titulo': 'CoPDocument Title',
                                                 'criador': 'test_user_1_',
                                                 'descricao': 'CoPDocument Description'},
                      news['Documentos'])
        self.assertEqual(news['Forum'], [])
        emails_from_view = view.getListaEmail(self.cop)
        email_list = ["test@test.com.br","test2@test.com.br","test3@test.com.br","test4@test.com.br","test5@test.com.br"]
        self.assertEqual(emails_from_view, email_list)
        #Testa lista de email para comunidade sem arquivo de configuracao
        self.cop.configuracoes.manage_delObjects(['arquivo_configuracao'])
        emails_from_view = view.getListaEmail(self.cop)
        email_list = ['test2@test.com.br', 'test4@test.com.br', 'test3@test.com.br', 'test@test.com.br']
        self.assertEqual(sorted(emails_from_view), sorted(email_list))
        #Reconstroi arquivo de configuracao
        editionCreatedContents(self.cop, False)

    def test_ViewCoPClockNotify_getcommentaries(self):
        self.factoryCoPFile()
        stringio = StringIO(zptlogo)
        self.copfolder.invokeFactory('CoPFile', 'copfile2',
                                    title="",
                                    description="CoPFile Description",
                                    file=stringio,)
        self.copfile2 = self.copfolder.copfile2

        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        conversation = IConversation(self.copfile)
        comment = createObject('plone.Comment')
        comment.creator = 'user_participante1'
        comment.creation_date = datetime.now() + timedelta(days=-1)
        comment.modification_date = datetime.now() + timedelta(days=-1)
        conversation.addComment(comment)
        self.copfile.modification_date = self.copfile.modification_date -1
        self.copfile.reindexObject()

        conversation2 = IConversation(self.copfile2)
        comment2 = createObject('plone.Comment')
        comment2.creator = TEST_USER_ID
        comment2.creation_date = datetime.now() + timedelta(days=-1)
        comment2.modification_date = datetime.now() + timedelta(days=-1)
        conversation2.addComment(comment2)
        conversation2.addComment(comment)
        self.copfile2.modification_date = self.copfile.modification_date -1
        self.copfile2.reindexObject()
        view = getMultiAdapter((self.portal, self.request), name='viewCoPClockNotify')
        view.sendMail()
        comentarios = view.getComentarios(self.cop)
        self.assertTrue(comentarios)
