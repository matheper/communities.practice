# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase

from StringIO import StringIO
from zptlogo import zptlogo
from Products.CMFPlone.utils import getToolByName
from Products.Archetypes.event import ObjectInitializedEvent
from communities.practice.subscribers import reindexSuperCoP


class SubscribersTestCase(IntegrationTestCase):

    def test_reindexSuperCoP(self):
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.copcommunityfolder.setImagem(stringio)

        catalog = getToolByName(self.cop, 'portal_catalog')
        path = '/'.join(self.copcommunityfolder.getPhysicalPath())
        query = {}
        query['path'] = {'query': path, 'depth': 1}
        query['portal_type'] = "CoP"
        brain = catalog(query)
        cop = brain[0]
        self.assertEqual(cop.subCoPNumber, 0)
        self.factorySubCoP()
        initialized_event = ObjectInitializedEvent(self.subcop)
        reindexSuperCoP(self.subcop, initialized_event)
        brain = catalog(query)
        cop = brain[0]
        self.assertEqual(cop.subCoPNumber, 1)
