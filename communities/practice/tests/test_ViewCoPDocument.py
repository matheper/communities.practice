# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPDocumentTestCase(IntegrationTestCase):

    def test_ViewCoPDocument_is_registered(self):
        self.factoryCoPDocument()
        view = queryMultiAdapter((self.copdocument, self.request), name='viewCoPDocument')
        self.assertTrue(view is not None)

    def test_ViewCoPDocument_Attributes(self):
        self.factoryCoPDocument()
        self.request.set('PARENTS',[self.copdocument, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copdocument,self.request), name='viewCoPDocument')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
