# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPShareTestCase(IntegrationTestCase):

    def test_ViewCoPShare_is_registered(self):
        self.factoryCoPShare()
        view = queryMultiAdapter((self.copshare, self.request), name='viewCoPShare')
        self.assertTrue(view is not None)

    def test_ViewCoPShare_Attributes(self):
        self.factoryCoPShare()
        self.request.set('PARENTS',[self.copshare, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copshare,self.request), name='viewCoPShare')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
