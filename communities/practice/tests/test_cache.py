# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from communities.practice.generics.cache import cleanMemberCache
from communities.practice.generics import cache
from plone.memoize.volatile import CleanupDict

class CacheTestCase(IntegrationTestCase):
    
    def test_cache_getCoPLocalRoles(self):
        self.factoryCoP()
        user_id = 'user'
        self.portal.acl_users.userFolderAddUser(user_id,user_id,[],[])
        self.cop.manage_setLocalRoles(user_id,['Participante'])
        cop_brain = self.portal.portal_catalog(UID=self.cop.UID())
        cop_brain = cop_brain[0]
        cop_local_roles = getCoPLocalRoles(cop_brain)
        cop_local_roles = [cop_member[0] for cop_member in cop_local_roles[cop_brain.UID]['Participante']]
        self.assertIn(user_id, cop_local_roles)
        self.cop.manage_setLocalRoles(user_id,['Moderador'])
        updateCommunityCache(self.cop, user_id, 'Moderador')
        cop_local_roles = getCoPLocalRoles(cop_brain)
        cop_local_roles = [cop_member[0] for cop_member in cop_local_roles[cop_brain.UID]['Moderador']]
        self.assertIn(user_id, cop_local_roles)

    def test_cache_listMemberRoleCommunitiesParticipating(self):
        self.factoryCoP()
        user_id = 'user'
        self.portal.acl_users.userFolderAddUser(user_id, user_id, [], [])
        self.cop.manage_setLocalRoles(user_id, ['Participante'])
        updateCommunityCache(self.cop, user_id, 'Participante')
        member_roles = listMemberRoleCommunitiesParticipating(user_id)
        self.assertEqual(member_roles[user_id][self.cop.UID()], 'Participante')
        self.cop.manage_setLocalRoles(user_id, ['Moderador'])
        updateCommunityCache(self.cop, user_id, 'Moderador')
        member_roles = listMemberRoleCommunitiesParticipating(user_id)
        self.assertEqual(member_roles[user_id][self.cop.UID()], 'Moderador')
        self.cop.manage_delLocalRoles([user_id])
        updateCommunityCache(self.cop, user_id)
        member_roles = listMemberRoleCommunitiesParticipating(user_id)
        self.assertNotIn(self.cop.UID(), member_roles[user_id].keys())
        cleanMemberCache(user_id)
        member_roles = listMemberRoleCommunitiesParticipating(user_id)
        self.assertFalse(member_roles[user_id])
