# -*- coding: utf-8 -*-

from zope.component import queryMultiAdapter
from zope.component import getMultiAdapter
from zope.component import queryUtility
from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from plone.app.testing import TEST_USER_ID

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import getCoPUpdates, getBrainValues
from communities.practice.generics.cache import updateCommunityCache

class FormCoPSearchTestCase(IntegrationTestCase):

    def test_ViewCoP_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='formCoPSearch')
        self.assertTrue(view is not None)

    def test_ViewCoP_menu(self):
        self.factoryCoP()
        self.factoryCoPEvent()

        updates = getCoPUpdates(self.cop)
        view = queryMultiAdapter((self.cop, self.request), name='formCoPSearch')
        view.__call__()
        self.assertFalse(view.catalog_search())
        self.assertDictEqual(view.get_brain_values(updates)[0], getBrainValues(self.cop, updates)[0])

    def test_get_commentaries(self):
        self.factoryCoP()
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        #self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.factoryCoPEvent()
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        view = getMultiAdapter((self.cop, self.request), name='formCoPSearch')
        conversation = IConversation(self.copevent)
        comment = createObject('plone.Comment')
        comment.creator = TEST_USER_ID
        conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 1)
        self.assertFalse(view.see_more)
        for i in range(5):
            comment = createObject('plone.Comment')
            comment.creator = TEST_USER_ID
            conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()), 4)
        self.assertEqual(len(comments), 4)
        self.assertTrue(view.see_more)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 6)
        self.assertFalse(view.see_more)
        self.assertTrue(view.is_discussion_allowed(self.cop.UID()))
