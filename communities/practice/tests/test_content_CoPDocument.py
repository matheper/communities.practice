# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPDocumentTestCase(IntegrationTestCase):

    def test_CoPDocument_addable(self):
        self.factoryCoPFolder()
        self.copfolder.invokeFactory('CoPDocument', 'copdocument',
                                     title="CoPDocument Title",
                                     description="CoPDocument Description",
                                     text="<b>CoPDocument</b> Text",)

        copdocument = self.copfolder.copdocument
        self.assertEquals(copdocument.Title(), "CoPDocument Title")
        self.assertEquals(copdocument.Description(), "CoPDocument Description")
        self.assertEquals(copdocument.getText(), "<b>CoPDocument</b> Text")

    def test_CoPDocument_description(self):
        self.factoryCoPFolder()
        self.copfolder.invokeFactory('CoPDocument', 'copdocument_text',
                                     title="CoPDocument Title",
                                     description="",
                                     text="<b>CoPDocument</b> Text",)

        copdocument = self.copfolder.copdocument_text
        copdocument.at_post_create_script()
        self.assertEquals(copdocument.Description(), "CoPDocument Text\xe2\x80\xa6")

    def test_CoPDocument_addable_CoPPortfolio(self):
        self.factoryCoPPortfolio()
        self.copportfolio.invokeFactory('CoPDocument', 'copdocument',
                                        title="CoPDocument Title",
                                        description="CoPDocument Description",
                                        text="<b>CoPDocument</b> Text",)

        copdocument = self.copportfolio.copdocument
        self.assertEquals(copdocument.Title(), "CoPDocument Title")
        self.assertEquals(copdocument.Description(), "CoPDocument Description")
        self.assertEquals(copdocument.getText(), "<b>CoPDocument</b> Text")
