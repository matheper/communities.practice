# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.browser.ViewCoPInvitation import ViewCoPInvitation
from communities.practice.subscribers import initialCreatedContents
from communities.practice.generics.generics import encodeUTF8
from communities.practice.subscribers import editionCreatedContents
from communities.practice.config import OPCOES
from zope.component import queryMultiAdapter
from StringIO import StringIO
from zptlogo import zptlogo

class ViewCoPInvitationTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPInvitationTestCase, self).setUp()
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.copcommunityfolder.setImagem(stringio)
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.request.set('ACTUAL_URL',self.cop.absolute_url())
        self.view = queryMultiAdapter((self.cop, self.request), name='viewCoPInvitation')
        self.view.__call__()

    def test_ViewCoPInvitation_get_cop_context(self):
        self.assertEqual(self.view.get_cop_context().Type(), u'Comunidade')

    def test_ViewCoPInvitation_get_communities_participating(self):
        self.assertEqual(self.view.get_communities_participating(), [])

    def test_ViewCoPInvitation_send_invitation_request(self):
        self.request.form = {'submit' : 'submit',
                             'message' : 'Mensagem',
                             'email_list' : 'teste@teste.com, a@teste.com',
                             encodeUTF8(self.cop.Title()) : self.cop.absolute_url(),
                            }
        self.assertEqual(self.view.send_invitation_request(),self.cop.absolute_url())

        self.copcommunityfolder.invokeFactory('CoP', 'cop2',
                                                title="CoP Title 2",
                                                description="CoP Description 2",
                                                subject=("CoPSubject 2",),
                                                acervo_input=OPCOES[0],
                                                calendario_input=OPCOES[0],
                                                forum_input=OPCOES[0],
                                                rss_input=OPCOES[0],
                                                webfolio_input=OPCOES[1],
                                                exibir_todo_conteudo_input=OPCOES[0],)
        self.cop2 = self.copcommunityfolder.cop2
        editionCreatedContents(self.cop2, False)
        self.request.form = {'submit' : 'submit',
                             'message' : 'Mensagem',
                             'email_list' : 'teste@teste.com, a@teste.com',
                             encodeUTF8(self.cop2.Title()) : self.cop2.absolute_url(),
                             encodeUTF8(self.cop.Title()) : self.cop.absolute_url(),
                            }
        self.view = queryMultiAdapter((self.cop2, self.request), name='viewCoPInvitation')
        self.assertEqual(self.view.send_invitation_request(),self.cop2.absolute_url())
