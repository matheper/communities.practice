# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, getUtility
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

from plone.portlets.interfaces import IPortletManager, IPortletRenderer, IPortletAssignmentMapping, IPortletType
from plone.app.portlets.portlets import navigation
from zope.app.container.interfaces import INameChooser

from plone.app.testing import login, logout

from communities.practice.portlets import portletCoPMembers
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.config import  OPCOES

class portletCoPMembersTestCase(IntegrationTestCase):

    def test_invoke_add_view(self):
        portlet = getUtility(IPortletType, name='communities.practice.portlets.portletCoPMembers')
        mapping = self.portal.restrictedTraverse('++contextportlets++plone.rightcolumn')
        for m in mapping.keys():
            del mapping[m]
        addview = mapping.restrictedTraverse('+/' + portlet.addview)
        addview.createAndAdd(data={})
        self.assertEquals(len(mapping), 1)
        self.failUnless(isinstance(mapping.values()[0], portletCoPMembers.Assignment))

    def test_portletCoPMembers_renderPortlet(self):
        """Render test. """
        self.factoryCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        updateCommunityCache(self.cop, 'user2', 'Participante')
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.cop.manage_setLocalRoles('user3',['Participante'])
        updateCommunityCache(self.cop, 'user3', 'Participante')
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        self.cop.manage_setLocalRoles('user4',['Moderador'])
        updateCommunityCache(self.cop, 'user4', 'Moderador')

        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        manager = getUtility(IPortletManager, name='plone.rightcolumn',context=self.portal)
        assignment = portletCoPMembers.Assignment('Participantes')
        renderer = getMultiAdapter((self.cop, self.request, view, manager, assignment), IPortletRenderer)

        self.assertEqual(len(renderer.get_moderadores()),2)
        self.assertEqual(len(renderer.get_participantes()),2)
        self.assertEqual(renderer.get_total_participantes(),4)
        self.assertTrue(renderer.get_communitie_participating())
        self.assertEqual(assignment.title, 'Participantes')
        self.assertIsNotNone(renderer.render())

        logout()
        login(self.portal, 'user2')
        self.assertTrue(renderer.get_communitie_participating())
        self.cop.participar_input = OPCOES[1]
        self.assertFalse(renderer.get_communitie_participating())
