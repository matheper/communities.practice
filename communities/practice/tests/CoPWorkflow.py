#-*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import get_workflow_xml

class CoPWorkflow(IntegrationTestCase):
    
    def setUp(self):
        super(CoPWorkflow, self).setUp();
        #definicoes iniciais
        self.requisites = None
        self.requisites_name = None
        self.requisites_content = None
        self.requisites_content_states = None
        self.requisites_content_permissions = None
        #workflow Plone
        self.definition = None
        self.definition_name = None
        self.definition_content = None
        self.definition_content_states = None
        self.definition_content_permissions = None
        self.content_type = None

    def configure(self, workflow_name, content_type):
        #definicoes iniciais
        self.requisites = get_workflow_xml('workflows/%s.xml' % workflow_name, False)
        self.requisites_name = self.requisites.get('workflow_name')
        self.requisites_content = self.requisites.get('states_permission_roles')
        self.requisites_content_states = self.requisites_content.keys()
        self.requisites_content_permissions = self.requisites_content.values()[0].keys()
        #workflow Plone
        self.definition = self.workflow_tool.getWorkflowById(workflow_name)
        self.definition_name = workflow_name
        self.definition_content = {}
        for state in self.definition.states.values(): 
            self.definition_content[state.title] = state.permission_roles
        self.definition_content_states = self.definition_content.keys()
        self.definition_content_permissions = self.definition_content.values()[0].keys()
        self.content_type = content_type

    def _test_workflow_is_installed(self):
        """Tests if workflow has been successfully installed
        """
        self.assertNotEqual(self.definition, None)

    def _test_workflow_assigned(self):
        """Tests if workflow is assigned to correct content type 
        """
        self.assertEqual(self.workflow_tool.getChainFor(self.content_type), (self.definition_name,))

    def _test_states(self):
        """Tests if states are properly defined
        """
        self.assertItemsEqual(self.requisites_content_states, self.definition_content_states)

    def _test_permissions(self):
        """Tests if permissions are properly defined
        """
        self.assertItemsEqual(self.requisites_content_permissions, self.definition_content_permissions)

    def _test_permissions_roles(self):
        """Tests if permissions and roles are properly defined
        """
        for state in self.requisites_content_states:
            permissions = self.requisites_content.get(state)
            for permission,roles in permissions.items():
                self.assertItemsEqual([permission,] + list(roles), [permission,] + list(self.definition_content[state][permission]))
                
    def run_tests(self):
        self._test_workflow_is_installed()
        self._test_workflow_assigned()
        self._test_states()
        self._test_permissions()
        self._test_permissions_roles()
