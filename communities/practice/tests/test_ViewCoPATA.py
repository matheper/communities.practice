# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter

class ViewCoPATATestCase(IntegrationTestCase):

    def test_ViewCoPATA_is_registered(self):
        self.factoryCoPATA()
        view = queryMultiAdapter((self.copata, self.request), name='viewCoPATA')
        self.assertTrue(view is not None)

    def test_ViewCoPATA_Attributes(self):
        self.factoryCoPATA()
        self.request.set('PARENTS',[self.copata, self.copmenu, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.copata,self.request), name='viewCoPATA')
        view.__call__()
        self.assertEquals(view.cop_menu_status[self.copmenu.titlemenu], 'cop_menu_selected')
