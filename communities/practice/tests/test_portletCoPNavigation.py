# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, getUtility
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

from plone.portlets.interfaces import IPortletManager, IPortletRenderer, IPortletAssignmentMapping, IPortletType
from plone.app.portlets.portlets import navigation
from zope.app.container.interfaces import INameChooser

from plone.app.testing import login, logout

from communities.practice.portlets import portletCoPNavigation
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.config import  OPCOES

class portletCoPNavigationTestCase(IntegrationTestCase):

    def test_invoke_add_view(self):
        portlet = getUtility(IPortletType, name='communities.practice.portlets.portletCoPNavigation')
        mapping = self.portal.restrictedTraverse('++contextportlets++plone.rightcolumn')
        for m in mapping.keys():
            del mapping[m]
        addview = mapping.restrictedTraverse('+/' + portlet.addview)
        addview.createAndAdd(data={})
        self.assertEquals(len(mapping), 1)
        self.failUnless(isinstance(mapping.values()[0], portletCoPNavigation.Assignment))

    def test_portletCoPNavigation_renderPortlet(self):
        """Render test. """
        self.factoryCoP()
        self.factorySubCoP()

        communityfolder_path = '/copcommunityfolder'
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        manager = getUtility(IPortletManager, name='plone.rightcolumn',context=self.portal)
        assignment = portletCoPNavigation.Assignment('Comunidades', communityfolder_path)
        renderer = getMultiAdapter((self.cop, self.request, view, manager, assignment), IPortletRenderer)

        self.assertEqual(renderer.get_root_path(), '/'.join(self.copcommunityfolder.getPhysicalPath()))

        recursive_content = renderer.get_recursive_content('/'.join(self.copcommunityfolder.getPhysicalPath()))
        self.assertEqual(len(recursive_content), 1)
        cop_brain = recursive_content[0]
        self.assertEqual(cop_brain.Title, self.cop.Title())

        recursive_content = renderer.get_recursive_content("%s/subcop"%(cop_brain.getPath()))
        self.assertEqual(len(recursive_content), 1)
        cop_brain = recursive_content[0]
        self.assertEqual(cop_brain.Title, self.subcop.Title())

        self.assertIsNotNone(renderer.get_recurse_cop_navigation('/'.join(self.copcommunityfolder.getPhysicalPath())))
        self.assertIsNotNone(renderer.render())
