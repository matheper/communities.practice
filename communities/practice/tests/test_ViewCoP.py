# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

from zope.component import getMultiAdapter, queryMultiAdapter
from StringIO import StringIO
from zptlogo import zptlogo
from plone.app.testing import logout
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from plone.app.testing import login, logout

from zope.component import createObject
from plone.app.discussion.interfaces import IConversation

from communities.practice.generics.cache import cleanMemberCache
from communities.practice.generics.cache import updateCommunityCache

class ViewCoPTestCase(IntegrationTestCase):

    def test_ViewCoP_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoP')
        self.assertTrue(view is not None)

    def test_ViewCoP_menu(self):
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.copcommunityfolder.setImagem(stringio)
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        view.__call__()
        self.assertEquals(view.cop_menu_status['inicio'],'cop_menu_selected')
        self.assertEquals(view.cop_participation,True)
        self.assertEquals(view.cop_title, self.cop.Title())
        self.assertEquals(view.cop_description, self.cop.Description())
        self.assertEquals(view.cop_image, self.copcommunityfolder.getImagem().absolute_url())

        logout()
        self.cop.setImagem(stringio)
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        view.__call__()
        self.assertEquals(view.cop_participation,False)
        self.assertEquals(view.cop_title, self.cop.Title())
        self.assertEquals(view.cop_description, self.cop.Description())
        self.assertEquals(view.cop_image, self.cop.getImagem().absolute_url())

    def test_get_commentaries(self):
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.request.set('PARENTS',[self.cop, self.copcommunityfolder])
        self.factoryCoPEvent()
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        conversation = IConversation(self.copevent)
        comment = createObject('plone.Comment')
        comment.creator = TEST_USER_ID
        conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 1)
        self.assertFalse(view.see_more)
        for i in range(5):
            comment = createObject('plone.Comment')
            comment.creator = TEST_USER_ID
            conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()), 4)
        self.assertEqual(len(comments), 4)
        self.assertTrue(view.see_more)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 6)
        self.assertFalse(view.see_more)

    def test_get_participation_role(self):
        cleanMemberCache(TEST_USER_ID)
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Aguardando'])
        updateCommunityCache(self.cop, 'user2', 'Aguardando')
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])

        view = queryMultiAdapter((self.cop, self.request), name='viewCoP')
        view.__call__()
        self.assertEquals(view.get_participation_role(), 'Participante')

        logout()
        login(self.portal, 'user2')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoP')
        view.__call__()
        self.assertEquals(view.get_participation_role(), 'Aguardando')

        logout()
        login(self.portal, 'user3')
        view = queryMultiAdapter((self.cop, self.request), name='viewCoP')
        view.__call__()
        self.assertEquals(view.get_participation_role(), 'Autenticado')

    def test_get_participation_allowed(self):
        self.factoryCoP()
        self.factorySubCoP()
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])

        logout()
        login(self.portal, 'user2')
        view = getMultiAdapter((self.cop, self.request), name='viewCoP')
        participation = view.participation_allowed()
        self.assertTrue(participation)
        view = getMultiAdapter((self.subcop, self.request), name='viewCoP')
        participation = view.participation_allowed()
        self.assertFalse(participation)

        logout()
        login(self.portal, TEST_USER_NAME)
        self.cop.manage_setLocalRoles('user2',['Participante'])
        updateCommunityCache(self.cop, 'user2', 'Participante')
        logout()
        login(self.portal, 'user2')
        view = getMultiAdapter((self.subcop, self.request), name='viewCoP')
        participation = view.participation_allowed()
        self.assertTrue(participation)

