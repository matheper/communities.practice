# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import queryMultiAdapter
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

class ViewCoPModeradorMasterTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPModeradorMasterTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.request.set('PARENTS',[self.copcommunityfolder])
        self.view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPModeradorMaster')
        self.view.__call__()        


    def test_ViewCoPModeradorMaster_is_registered(self):
        self.assertTrue(self.view is not None)


    def test_ViewCoPNotificacao_get_moderadores_master(self):
        """Test method get_moderadores_master
        """
        self.addUsersCoPCommunityFolder()
        moderadores = self.view.get_moderadores_master()
        url = self.portal.absolute_url() + '/author/'
        resultado = [
            {'login': 'user_master1', 'author': url + 'user_master1', 'papel': 'Moderador_Master', 'nome': 'User_master1'},
            {'login': 'user_master2', 'author': url + 'user_master2', 'papel': 'Moderador_Master', 'nome': 'User_master2'},
            {'login': 'user_master5', 'author': url + 'user_master5', 'papel': 'Observador_Master', 'nome': 'User_master5'},
        ]        

        self.assertEquals(moderadores, resultado)


    def test_ViewCoPModeradorMaster_set_moderadores_master(self):
        """Test method set_moderadores_master
        """
        self.factorySubCoP()
        self.addUsersCoPCommunityFolder()


        self.request.form = {
            'submit_moderadores_master':'Salvar',
            'user_master1':['Moderador_Master', 'Moderador_Master'],
            'user_master2':['Observador_Master', 'Moderador_Master'],
            'user_master5':['Moderador_Master','Observador_Master'],
        }
        self.view.moderadores = []
        self.view.set_moderadores_master()
        local_roles = dict(self.copcommunityfolder.get_local_roles())
        self.assertEquals(local_roles['user_master1'][0], 'Moderador_Master')
        self.assertEquals(local_roles['user_master2'][0], 'Observador_Master')
        self.assertEquals(local_roles['user_master5'][0], 'Moderador_Master')
        cop_roles = dict(self.cop.get_local_roles())
        sub_cop_roles = dict(self.subcop.get_local_roles())
        self.assertEquals(cop_roles['user_master2'][0], 'Observador_Master')
        self.assertEquals(cop_roles['user_master5'][0], 'Moderador_Master')
        self.assertEquals(sub_cop_roles['user_master2'][0], 'Observador_Master')
        self.assertEquals(sub_cop_roles['user_master5'][0], 'Moderador_Master')

        self.request.form = {
            'submit_moderadores_master':'Salvar',
            'user_master1':['Moderador_Master', 'Moderador_Master'],
            'user_master2':['Excluir', 'Observador_Master'],
            'user_master5':['Observador_Master','Moderador_Master'],
        }
        self.view.moderadores = []
        self.view.set_moderadores_master()
        local_roles = dict(self.copcommunityfolder.get_local_roles())
        self.assertEquals(local_roles['user_master1'][0], 'Moderador_Master')
        self.assertEquals(local_roles['user_master5'][0], 'Observador_Master')
        self.assertFalse('user_master2' in local_roles.keys())
        cop_roles = dict(self.cop.get_local_roles())
        sub_cop_roles = dict(self.subcop.get_local_roles())

        self.assertEquals(cop_roles['user_master5'][0], 'Observador_Master')
        self.assertFalse('user_master2' in cop_roles.keys())
        self.assertEquals(sub_cop_roles['user_master5'][0], 'Observador_Master')
        self.assertFalse('user_master2' in sub_cop_roles.keys())


    def test_ViewCoPModeradorMaster_get_adicionar_moderadores(self):
        """Test method get_adicionar_moderadores
        """
        self.request.form = {'text_busca_adicionar_moderadores':'user_master'}
        self.addUsersCoPCommunityFolder()
        adicionar = self.view.get_adicionar_moderadores()
        url = self.portal.absolute_url() + '/author/'
        resultado = [{'login': 'user_master3', 'author': url + 'user_master3', 'nome': 'User_master3'}, 
                     {'login': 'user_master4', 'author': url + 'user_master4', 'nome': 'User_master4'},
                     {'login': 'user_master6', 'author': url + 'user_master6', 'nome': 'User_master6'}]
        self.assertEquals(adicionar, resultado)


    def test_ViewCoPModeradorMaster_set_adicionar_moderadores(self):
        """Test method set_adicionar_moderadores
        """
        self.addUsersCoPCommunityFolder()
        self.request.form= {
            'submit_adicionar_moderadores':'Adicionar', 
            'user_master2':'Moderador_Master', 
            'user_master3':'Moderador_Master',
            'user_master6':'Observador_Master',
        }
        self.view.set_adicionar_moderadores()
        local_roles = dict(self.copcommunityfolder.get_local_roles())
        self.assertEquals(local_roles['user_master2'][0], 'Moderador_Master')
        self.assertEquals(local_roles['user_master3'][0], 'Moderador_Master')
        self.assertEquals(local_roles['user_master6'][0], 'Observador_Master')
        self.assertFalse('user_master4' in local_roles.keys())

        self.request.form = {'text_busca_adicionar_moderadores':'user_master'}
        adicionar = self.view.get_adicionar_moderadores()
        url = self.portal.absolute_url() + '/author/'
        resultado = [{'login': 'user_master4', 'author': url + 'user_master4', 'nome': 'User_master4'}]
        self.assertEquals(adicionar, resultado)


    def addUsersCoPCommunityFolder(self):
        """Adds users and add them to CoPCommunityFolder
        """
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master1','user_master1',[],[])
        self.copcommunityfolder.manage_setLocalRoles('user_master1',['Moderador_Master'])
        member = self.portal.portal_membership.getMemberById('user_master1')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master2','user_master2',[],[])
        self.copcommunityfolder.manage_setLocalRoles('user_master2',['Moderador_Master'])
        member = self.portal.portal_membership.getMemberById('user_master2')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master3','user_master3',[],[])
        member = self.portal.portal_membership.getMemberById('user_master3')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master4','user_master4',[],[])
        member = self.portal.portal_membership.getMemberById('user_master4')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master5','user_master5',[],[])
        self.copcommunityfolder.manage_setLocalRoles('user_master5',['Observador_Master'])
        member = self.portal.portal_membership.getMemberById('user_master5')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_master6','user_master6',[],[])
        member = self.portal.portal_membership.getMemberById('user_master6')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})
