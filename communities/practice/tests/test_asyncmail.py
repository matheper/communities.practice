# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.asyncmail import *
from communities.practice.generics import asyncmail_config
import shutil

class AsyncmailTestCase(IntegrationTestCase):

    def setUp(self):
        super(AsyncmailTestCase, self).setUp()
        self.factoryCoP()

    def test_asyncmail_methods(self):        
        #test method configureSMTPMailer
        mailer = configureSMTPMailer()
        self.assertEquals(mailer.hostname, asyncmail_config.HOSTNAME)
        self.assertEquals(mailer.port, asyncmail_config.PORT)
        self.assertEquals(mailer.username, asyncmail_config.USERNAME)
        self.assertEquals(mailer.password, asyncmail_config.PASSWORD)

        #test do method configureMaildir
        maildir = configureMaildir()

        paths = []
        for path in ('/tmp', '/cur', '/new'):
            paths.append(maildir.path + path)

        self.assertTrue(os.path.exists(maildir.path))
        for path in paths:
            self.assertTrue(os.path.exists(path))
            shutil.rmtree(path)

        maildir = configureMaildir()
        for path in paths:
            self.assertTrue(os.path.exists(path))

        shutil.rmtree(maildir.path)
        asyncmail_config.DIRECTORY = ''
        maildir = configureMaildir()
        for path in ('', '/tmp', '/cur', '/new'):
            self.assertTrue(os.path.exists(maildir.path + path))

        #test methods createMessage and sendAsyncMail
        mail_from = asyncmail_config.USERNAME
        list_mail_to = asyncmail_config.USERNAME
        subject = 'Subject'
        message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
        createMessage(mail_from, list_mail_to, subject, message)
        sendAsyncMail()
