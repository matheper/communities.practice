# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents

from zope.component import queryMultiAdapter
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME

class ViewCoPNotificacaoTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPNotificacaoTestCase, self).setUp()
        self.factoryCoP()
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.configuracoes, self.cop, self.copcommunityfolder])
        self.view = queryMultiAdapter((self.cop.configuracoes, self.request), name='viewCoPNotificacao')
        self.view.__call__()        

    def test_ViewCoPNotificacao_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPNotificacao_get_participantes(self):
        """Test method get_participantes
        """
        self.addUsersCoP()
        participantes = self.view.get_participantes()
        url = self.portal.absolute_url() + '/author/'
        resultado = [{'id': TEST_USER_ID, 'nome': TEST_USER_ID}, 
                     {'id': 'user_moderador', 'nome': 'user_moderador'}, 
                     {'id': 'user_moderador_master', 'nome': 'user_moderador_master'}, 
                     {'id': 'user_participante', 'nome': 'user_participante'}]

        self.assertEquals(participantes, resultado)

    def test_ViewCoPNotificacao_send_mail_notificacao(self):
        """Test method send_mail_notificacao
        """            
        self.addUsersCoP()
        self.request.form = {}
        self.assertEquals(self.view.send_mail_notificacao(), "")

        self.request.form = {'submit_send_mail_notificacao':'Enviar'}
        self.assertEquals(self.view.send_mail_notificacao(), u"Campos obrigatórios não preenchidos")
        
        self.request.form = {'submit_send_mail_notificacao':'Enviar', 
                             'assunto':'Assunto',
                             'mensagem':'Mensagem',
                             'user_sem_email':'Sim'}
        self.assertEquals(self.view.send_mail_notificacao(), u"Selecione emails para envio.")

        self.request.form = {'submit_send_mail_notificacao':'Enviar', 
                             'assunto':'Assunto',
                             'mensagem':'Mensagem',
                             'user_moderador':'Sim', 
                             'user_participante':'Sim', 
                             'user_moderador_master':'Sim',
                             'user_sem_email':'Sim'}
        self.assertEquals(self.view.send_mail_notificacao(), u"Email enviado com sucesso.")

    def addUsersCoP(self):
        """Adds users and add them to CoP
        """
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_participante','user_participante',[],[])
        self.cop.manage_setLocalRoles('user_participante',['Participante'])
        member = self.portal.portal_membership.getMemberById('user_participante')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})
        
        self.portal.acl_users.userFolderAddUser('user_moderador','user_moderador',[],[])
        self.cop.manage_setLocalRoles('user_moderador',['Moderador'])
        member = self.portal.portal_membership.getMemberById('user_moderador')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})
        
        self.portal.acl_users.userFolderAddUser('user_moderador_master','user_moderador_master',[],[])
        self.cop.manage_setLocalRoles('user_moderador_master',['Moderador_Master'])
        member = self.portal.portal_membership.getMemberById('user_moderador_master')
        member.setMemberProperties(mapping={"email":"test@test.com.br"})

        self.portal.acl_users.userFolderAddUser('user_sem_email','user_sem_email',[],[])
        self.cop.manage_setLocalRoles('user_sem_email',['Participante'])
