# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, queryMultiAdapter
from StringIO import StringIO
from zptlogo import zptlogo
from plone.app.testing import TEST_USER_ID, TEST_USER_NAME
from plone.app.testing import login, logout
from communities.practice.generics.cache import updateCommunityCache


class ViewCoPTestCase(IntegrationTestCase):

    def test_ViewCoP_is_registered(self):
        self.factoryCoP()
        view = queryMultiAdapter((self.cop, self.request), name='viewCoPList')
        self.assertTrue(view is not None)

    def test_get_cop_list(self):
        self.factoryCoP()
        self.factorySubCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        view = getMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPList')
        cop_list = view.get_cop_list()
        self.assertEqual(len(cop_list), 1)
        cop = cop_list[0]
        self.assertEqual(cop['titulo'], 'CoP Title')
        self.assertEqual(cop['descricao'], 'CoP Description')
        self.assertEqual(cop['papel_participante'], 'Owner')

        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.subcop.manage_setLocalRoles('user2',['Aguardando'])
        updateCommunityCache(self.subcop, 'user2', 'Aguardando')
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.subcop.manage_setLocalRoles('user3',['Bloqueado'])
        updateCommunityCache(self.subcop, 'user3', 'Bloqueado')

        logout()
        login(self.portal, 'user2')
        view = getMultiAdapter((self.cop, self.request), name='viewCoPList')
        cop_list = view.get_cop_list()
        self.assertEqual(len(cop_list), 1)
        cop = cop_list[0]
        self.assertEqual(cop['titulo'], 'SubCoP Title')
        self.assertEqual(cop['descricao'], 'SubCoP Description')
        self.assertEqual(cop['papel_participante'], 'Aguardando')

        logout()
        login(self.portal, TEST_USER_NAME)
        self.subcop.manage_setLocalRoles('user2',['Moderador'])
        updateCommunityCache(self.subcop, 'user2', 'Moderador')
        logout()
        login(self.portal, 'user2')
        view = getMultiAdapter((self.cop, self.request), name='viewCoPList')
        cop_list = view.get_cop_list()
        self.assertEqual(len(cop_list), 1)
        cop = cop_list[0]
        self.assertEqual(cop['titulo'], 'SubCoP Title')
        self.assertEqual(cop['descricao'], 'SubCoP Description')
        self.assertEqual(cop['papel_participante'], 'Participante')

        logout()
        login(self.portal, 'user3')
        view = getMultiAdapter((self.cop, self.request), name='viewCoPList')
        cop_list = view.get_cop_list()
        self.assertEqual(len(cop_list), 1)
        cop = cop_list[0]
        self.assertEqual(cop['titulo'], 'SubCoP Title')
        self.assertEqual(cop['descricao'], 'SubCoP Description')
        self.assertEqual(cop['papel_participante'], 'Bloqueado')

