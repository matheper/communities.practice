# -*- coding: utf-8 -*-

from plone.app.testing import setRoles
from plone.app.testing import login
from plone.app.testing import TEST_USER_ID
from plone.app.testing import TEST_USER_NAME
from communities.practice.config import OPCOES 
import unittest2 as unittest
from communities.practice.testing import INTEGRATION_TESTING
from StringIO import StringIO
from zptlogo import zptlogo

class IntegrationTestCase(unittest.TestCase):
    
    layer = INTEGRATION_TESTING
    copcommunityfolder = None
    cop = None
    copmenu = None
    subcopmenu = None
    copfolder = None
    copfolderportfolio = None
    copportfolio = None
    coptarefas= None
    copupload = None
    coplink = None
    copimage = None
    copevent = None
    copdocument = None
    copfile = None
    copata = None
    copshare = None
    subcop = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.workflow_tool = self.portal.portal_workflow
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        login(self.portal, TEST_USER_NAME)
        
    def factoryCoPCommunityFolder(self):
        if not self.copcommunityfolder:
            self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                      title="CoPCommunityFolder Title",
                                      description="CoPCommunityFolder Description",)
            self.copcommunityfolder = self.portal.copcommunityfolder
        
    def factoryCoP(self):
        if not self.copcommunityfolder:
            self.factoryCoPCommunityFolder()
        if not self.cop:
            self.copcommunityfolder.invokeFactory('CoP', 'cop',
                                                  title="CoP Title",
                                                  description="CoP Description",
                                                  subject=("CoPSubject",),
                                                  acervo_input=OPCOES[0],
                                                  calendario_input=OPCOES[0],
                                                  forum_input=OPCOES[0],
                                                  portfolio_input=OPCOES[0],
                                                  tarefas_input=OPCOES[1],
                                                  exibir_todo_conteudo_input=OPCOES[0],)
            self.cop = self.copcommunityfolder.cop

    def factoryCoPMenu(self):
        if not self.cop:    
            self.factoryCoP()
        if not self.copmenu:
            self.cop.invokeFactory('CoPMenu', 'copmenu',
                                   title="CoPMenu Title",
                                   description="CoPMenu Description",)
            self.copmenu = self.cop.copmenu

    def factoryCoPFolder(self):
        if not self.copmenu:    
            self.factoryCoPMenu()
        if not self.copfolder:
            self.copmenu.invokeFactory('CoPFolder', 'copfolder',
                                       title="CoPFolder Title",
                                       description="CoPFolder Description",)
            self.copfolder = self.copmenu.copfolder

    def factoryCoPFolderPortfolio(self):
        if not self.cop:
            self.factoryCoP()

        if not self.copfolderportfolio:
            self.cop.invokeFactory('CoPMenu', 'copfolderportfolio',
                                   title="CoPFolderPortfolio Title",
                                   description="CoPFolderPortfolio Description",)
            self.copfolderportfolio = self.cop.copfolderportfolio

    def factoryCoPPortfolio(self):
        if not self.copfolderportfolio:
            self.factoryCoPFolderPortfolio()

        if not self.copportfolio:
            self.copfolderportfolio.invokeFactory('CoPPortfolio', 'copportfolio',
                                                  title="CoPPortfolio Title",
                                                 description="CoPPortfolio Description",)

            self.copportfolio = self.copfolderportfolio.copportfolio
    
    def factoryCoPTarefas(self):
        if not self.cop:
            self.factoryCoP()
        
        if not self.coptarefas:
            self.cop.invokeFactory('CoPMenu', 'coptarefas',
                                   title="CoPTarefas Title",
                                   description="CoPTarefas Description",)

            self.coptarefas = self.cop.coptarefas
     
    def factoryCoPUpload(self):
        if not self.coptarefas:
            self.factoryCoPTarefas()

        if not self.copupload:
            self.coptarefas.invokeFactory('CoPUpload', 'copupload',
                                           title="CoPUpload Title",
                                           description="CoPUpload Description",)

            self.copupload = self.coptarefas.copupload

    def factoryCoPLink(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.coplink:
            self.copfolder.invokeFactory('CoPLink', 'coplink',
                                        title="CoPLink Title",
                                        description="CoPLink Description",
                                        remoteUrl="http://www.otics.org/otics",)
            self.coplink = self.copfolder.coplink

    def factoryCoPImage(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copimage:
            stringio = StringIO(zptlogo)
            self.copfolder.invokeFactory('CoPImage', 'copimage',
                                        title="CoPImage Title",
                                        description="CoPImage Description",
                                        image=stringio,)

            self.copimage = self.copfolder.copimage

    def factoryCoPEvent(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copevent:
            self.copmenu.invokeFactory('CoPEvent', 'copevent',
                                        title="CoPEvent Title",
                                        description="CoPEvent Description",
                                        location="CoPEvent Location",
                                        event_url="http://www.otics.org/otics",
                                        contact_name="CoPEvent Contact",
                                        contact_email="contact@mail.com",)
            self.copevent = self.copmenu.copevent

    def factoryCoPFile(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copfile:
            stringio = StringIO(zptlogo)
            self.copfolder.invokeFactory('CoPFile', 'copfile',
                                        title="CoPFile Title",
                                        description="CoPFile Description",
                                        file=stringio,)
            self.copfile = self.copfolder.copfile

    def factoryCoPDocument(self):
        if not self.copfolder:
            self.factoryCoPFolder()
        if not self.copdocument:
            self.copfolder.invokeFactory('CoPDocument', 'copdocument',
                                        title="CoPDocument Title",
                                        description="CoPDocument Description",
                                        )
            self.copdocument = self.copfolder.copdocument

    def factoryCoPATA(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copata:
            self.copmenu.invokeFactory('CoPATA', 'copata',
                                       title="CoPATA Title",
                                       description="CoPATA Description",
                                       pauta="<b>CoPATA</b> Pauta",
                                       discussao="<b>CoPATA</b> Discussao",
                                       encaminhamentos="<b>CoPATA</b> Encaminhamentos",)
            self.copata = self.copmenu.copata

    def factoryCoPShare(self):
        if not self.copmenu:
            self.factoryCoPMenu()
        if not self.copshare:
            self.copmenu.invokeFactory('CoPShare', 'copshare',
                                        title="CoPShare Title",
                                        description="CoPShare Description",
                                        remoteUrl="http://www.otics.org/otics",)
            self.copshare = self.copmenu.copshare

    def factorySubCoPMenu(self):
        if not self.subcopmenu:
            self.cop.invokeFactory('CoPMenu', 'subcop',
                                    title="SubCoPMenu Title",
                                    description="SubCoPMenu Description",)
            self.subcopmenu = self.cop.subcop

    def factorySubCoP(self):
        if not self.cop:
            self.factoryCoP()
        if not self.subcopmenu:
            self.factorySubCoPMenu()
        if not self.subcop:
            self.subcopmenu.invokeFactory('CoP','subcop',
                                        title="SubCoP Title",
                                        description="SubCoP Description",
                                        subject=("SubCoPSubject",),
                                        acervo_input=OPCOES[0],
                                        calendario_input=OPCOES[0],
                                        forum_input=OPCOES[0],
                                        portfolio_input=OPCOES[0],
                                        tarefas_input=OPCOES[1],
                                        exibir_todo_conteudo_input=OPCOES[0],
                                        )
            self.subcop = self.subcopmenu.subcop
