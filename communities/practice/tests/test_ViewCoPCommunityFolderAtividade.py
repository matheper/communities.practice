# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.tests.CoPUtils import add_users_CoP, create_content_atividade
from communities.practice.subscribers import initialCreatedContents
from communities.practice.config import OPCOES
from communities.practice import config
from communities.practice.generics.atividade_generics import TYPES_ATIVIDADE
from zope.component import queryMultiAdapter
from zope.security.management import queryInteraction
from zope.security.management import newInteraction, endInteraction
from datetime import datetime

class ViewCoPCommunityFolderAtividadeTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPCommunityFolderAtividadeTestCase, self).setUp()
        self.factorySubCoP()
        self.cop.setTarefas_input(OPCOES[0])
        initialCreatedContents(self.cop, False)
        add_users_CoP(self.portal, self.cop)
        create_content_atividade(self.cop)
        self.request.set('PARENTS',[self.copcommunityfolder])
        newInteraction()
        interaction = queryInteraction()
        self.view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolderAtividade')
        self.view.__call__()    

    def tearDown(self):
        endInteraction()

    def test_ViewCoPCommunityFolderAtividade_is_registered(self):
        self.assertTrue(self.view is not None)

    def test_ViewCoPCommunityFolderAtividade_get_types(self):
        extras = [('total', u'Total'),]
        forms = []
        for form, description in config.AVAILABLE_FORMS.items():
            forms.append(form)
        if forms:
            extras.append((tuple(forms),u'Formulários'))
        self.assertItemsEqual(self.view.get_types(), TYPES_ATIVIDADE.get("") + extras)

    def test_ViewCoPCommunityFolderAtividade_get_communities(self):
        communities = self.view.get_communities()
        UIDS = [community['brain'].UID for community in communities]
        self.assertIn(self.cop.UID(), UIDS)
        self.assertIn(self.subcop.UID(), UIDS)

    def test_ViewCoPCommunityFolderAtividade_get_total_types(self):
        total_types = self.view.get_total_types()
        self.assertEqual(total_types[0]['total_content'][('total', u'Total')], 5)
        self.assertEqual(total_types[0]['total_content'][('CoPDocument', u'Páginas')], 3)
        self.assertEqual(total_types[0]['total_content'][('CoPFile', u'Arquivos')], 1)

    def test_ViewCoPCommunityFolderAtividade_export_data(self):
        self.request.form = {'submit_export_atividades': 'Exportar'}
        self.view.export_data()
        filename = "Atividades_%s_%s.csv" % (self.copcommunityfolder.Title(), 
                                             datetime.now().strftime("%d_%m_%Y"))
        headers = self.request.response.headers
        self.assertEqual(headers.get("content-length", None), "0")
        self.assertEqual(headers.get("Content-Type", None), "text/csv")
        self.assertEqual(headers.get("content-disposition", None), 'attachment; filename="%s"' % filename)
        self.assertEqual(self.request.response.status, 200)
