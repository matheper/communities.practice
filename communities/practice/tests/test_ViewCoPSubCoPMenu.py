# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.subscribers import initialCreatedContents

from zope.component import queryMultiAdapter

class ViewCoPSubCoPMenuTestCase(IntegrationTestCase):

    def test_ViewCoPSubCoPMenu_is_registered(self):
        self.factorySubCoP()
        view = queryMultiAdapter((self.cop.subcop, self.request), name='viewCoPSubCoPMenu')
        self.assertTrue(view is not None)

    def test_ViewCoPMenu_addable_types(self):
        self.factoryCoP()
        self.cop.subcop_input = u'Habilitar'
        #Criacao do conteudo da CoP
        initialCreatedContents(self.cop,False)
        self.request.set('PARENTS',[self.cop.subcop, self.cop, self.copcommunityfolder])
        view = queryMultiAdapter((self.cop.subcop, self.request), name='viewCoPSubCoPMenu')
        view.__call__()
        self.assertEqual(view.cop_addable_types_list, [['CoP', u'Comunidade']])
