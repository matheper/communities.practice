# -*- coding: utf-8 -*-

from communities.practice.config import OPCOES
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.content.CoPCommunityFolder import add_portletCoPSocialNetworks

from zope.component import getMultiAdapter, getUtility
from plone.portlets.interfaces import IPortletManager, IPortletAssignmentMapping

class CoPCommunityFolderTestCase(IntegrationTestCase):

    def test_CoPCommunityFolder_addable(self):
        self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                   title="CoPCommunityFolder Title",
                                   description="CoPCommunityFolder Description",
                                   twitter = "CoPCommunityFolder Twitter",
                                   facebook = "CoPCommunityFolder Facebook",
                                   youtube = "CoPCommunityFolder Youtube",
                                   criacao_comunidades=OPCOES[0],)

        copcommunityfolder = self.portal.copcommunityfolder
        self.assertEquals(copcommunityfolder.Title(), "CoPCommunityFolder Title")
        self.assertEquals(copcommunityfolder.Description(), "CoPCommunityFolder Description")
        self.assertEquals(copcommunityfolder.getTwitter(), "CoPCommunityFolder Twitter")
        self.assertEquals(copcommunityfolder.getFacebook(), "CoPCommunityFolder Facebook")
        self.assertEquals(copcommunityfolder.getYoutube(), "CoPCommunityFolder Youtube")
        self.assertEquals(copcommunityfolder.getCriacao_comunidades(), OPCOES[0])
        self.assertTrue(copcommunityfolder.get_criacao_habilitado())
        copcommunityfolder.setCriacao_comunidades(OPCOES[1])
        self.assertFalse(copcommunityfolder.get_criacao_habilitado())

    def test_CoPCommunityFolder_setLocalRoles(self):
        self.portal.invokeFactory('CoPCommunityFolder', 'copcommunityfolder',
                                   title="CoPCommunityFolder Title",
                                   description="CoPCommunityFolder Description",
                                   criacao_comunidades=OPCOES[0],)
        copcommunityfolder = self.portal.copcommunityfolder
        copcommunityfolder.at_post_create_script()
        self.assertEquals(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'), ('Contributor',))
        copcommunityfolder.at_post_edit_script()
        self.assertEquals(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'), ('Contributor',))
        copcommunityfolder.setCriacao_comunidades(OPCOES[1])
        copcommunityfolder.at_post_edit_script()
        self.assertEquals(copcommunityfolder.get_local_roles_for_userid('AuthenticatedUsers'), ('',))

    def test_CoPCommunityFolder_portlets(self):
        self.factoryCoP()
        add_portletCoPSocialNetworks(self.cop, 'redes-sociais')
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")
        manager = getMultiAdapter((self.cop, column,), IPortletAssignmentMapping)
        items = manager.items()
        self.assertEqual(len(items),0)

        add_portletCoPSocialNetworks(self.copcommunityfolder, 'redes-sociais')
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")
        manager = getMultiAdapter((self.copcommunityfolder, column,), IPortletAssignmentMapping)
        items = manager.items()
        self.assertEqual(len(items),1)
        self.assertIn('redes-sociais', items[0])
