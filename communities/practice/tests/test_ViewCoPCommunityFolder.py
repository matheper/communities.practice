# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.config import OPCOES
from communities.practice.subscribers import editionCreatedContents
from communities.practice.subscribers import membersCommunitiesCache
from communities.practice.subscribers import cleanMembersCommunitiesCache
from communities.practice.generics.cache import updateCommunityCache
from zope.component import queryMultiAdapter
from plone.app.testing import login, logout
from StringIO import StringIO
from zptlogo import zptlogo

class ViewCoPCommunityFolderTestCase(IntegrationTestCase):

    def test_ViewCoPCommunityFolder_is_registered(self):
        self.factoryCoPCommunityFolder()
        view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolder')
        self.assertTrue(view is not None)

    def test_ViewCoPCommunityFolder_comunidades(self):
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.copcommunityfolder.setImagem(stringio)

        self.copcommunityfolder.invokeFactory('CoP', 'cop2',
                                                title="CoP Title 2",
                                                description="CoP Description 2",
                                                subject=("CoPSubject 2",),
                                                acervo_input=OPCOES[0],
                                                calendario_input=OPCOES[0],
                                                forum_input=OPCOES[0],
                                                rss_input=OPCOES[0],
                                                webfolio_input=OPCOES[1],
                                                exibir_todo_conteudo_input=OPCOES[0],)
        self.cop2 = self.copcommunityfolder.cop2
        editionCreatedContents(self.cop2, False)
        editionCreatedContents(self.cop, False)
        self.request.set('PARENTS',[self.copcommunityfolder])
        view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolder')
        view.__call__()
        self.assertEquals(len(view.communities),2)
        self.assertEquals(len(view.my_communities),2)
        
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        updateCommunityCache(self.cop, 'user2', 'Participante')
        logout()
        login(self.portal, 'user2')
        membersCommunitiesCache(self.cop, False)
        view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolder')
        view.__call__()
        self.assertEquals(len(view.communities),2)
        self.assertEquals(len(view.my_communities),1)

        view.busca_comunidades()
        dict_cop = view.communities[0]
        self.assertEquals(dict_cop["titulo"], self.cop.title)
        self.assertEquals(dict_cop["descricao"], self.cop.description())
        self.assertEquals(dict_cop["url"], self.cop.absolute_url())
        self.assertEquals(dict_cop["participar"],"%s/viewCoPJoin"%(self.cop.absolute_url()))
        self.assertEquals(dict_cop["imagem"],"%s/imagem"%(self.cop.absolute_url()))
        self.assertEquals(dict_cop["participante"],"Participante")
        review_state = self.cop.portal_workflow.getInfoFor(self.cop, "review_state", "")
        workflows = self.workflow_tool.getWorkflowsFor(self.cop)
        for w in workflows:
            if w.states.has_key(review_state):
                review_state = w.states[review_state].title
        self.assertEquals(dict_cop["estado"], review_state)

        dict_cop = view.communities[1]
        self.assertEquals(dict_cop["titulo"], self.cop2.title)
        self.assertEquals(dict_cop["descricao"], self.cop2.description())
        self.assertEquals(dict_cop["url"], self.cop2.absolute_url())
        self.assertEquals(dict_cop["participar"],"%s/viewCoPJoin"%(self.cop2.absolute_url()))
        self.assertEquals(dict_cop["imagem"],"%s/imagem"%(self.copcommunityfolder.absolute_url()))

        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.cop.manage_setLocalRoles('user3',['Aguardando'])
        updateCommunityCache(self.cop, 'user3', 'Aguardando')
        logout()
        cleanMembersCommunitiesCache(self.cop, False)
        login(self.portal, 'user3')
        view = queryMultiAdapter((self.copcommunityfolder, self.request), name='viewCoPCommunityFolder')
        view.__call__()
        dict_cop = view.communities[0]
        self.assertEquals(len(view.communities),2)
        self.assertEquals(len(view.my_communities),0)
        self.assertEquals(dict_cop["participante"],"Aguardando")

        #testar cache com PropertiedUser
        application = self.portal.getPhysicalRoot()
        application.acl_users.userFolderAddUser('propertied_user', 'propertied_user', [], [])
        propertied_user = application.acl_users.getUserById('propertied_user')
        propertied_user.REQUEST.set('PUBLISHED', self.cop)
        membersCommunitiesCache(propertied_user, False)



