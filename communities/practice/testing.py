# -*- coding: utf-8 -*-

import unittest2 as unittest

from plone.app.testing import PloneSandboxLayer
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from plone.app.testing.selenium_layers import SELENIUM_PLONE_FUNCTIONAL_TESTING

from plone.testing import z2

class Fixture(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import communities.practice 
        self.loadZCML(package=communities.practice)

    def setUpPloneSite(self, portal):
        # Install into Plone site using portal_setup
        self.applyProfile(portal, 'communities.practice:default')


FIXTURE = Fixture()
INTEGRATION_TESTING = IntegrationTesting(
    bases=(FIXTURE,),
    name='communities.practice:Integration',
    )
FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(FIXTURE,),
    name='communities.practice:Functional',
    )

class SeleniumTestCase(unittest.TestCase):
    """We use this base class for all functional tests.
    """
    layer = SELENIUM_PLONE_FUNCTIONAL_TESTING
