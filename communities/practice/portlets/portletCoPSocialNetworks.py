from zope.interface import Interface
from zope.interface import implements

from plone.app.portlets.portlets import base
from plone.portlets.interfaces import IPortletDataProvider

from zope import schema
from zope.formlib import form
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from Products.CMFPlone import PloneMessageFactory as _

from zope.i18nmessageid import MessageFactory
__ = MessageFactory("plone")

from Acquisition import aq_inner, aq_parent
from communities.practice.generics.generics import getCoPContext

class IPortletCoPSocialNetworks(IPortletDataProvider):
    """A portlet

    It inherits from IPortletDataProvider because for this portlet, the
    data that is being rendered and the portlet assignment itself are the
    same.
    """

    # TODO: Add any zope.schema fields here to capture portlet configuration
    # information. Alternatively, if there are no settings, leave this as an
    # empty interface - see also notes around the add form and edit form
    # below.

    some_field = schema.TextLine(title=_(u"Titulo"),
                                  description=_(u"Titulo do portlet"),
                                  required=True)


class Assignment(base.Assignment):
    """Portlet assignment.

    This is what is actually managed through the portlets UI and associated
    with columns.
    """

    implements(IPortletCoPSocialNetworks)

    # TODO: Set default values for the configurable parameters here

    some_field = u""

    # TODO: Add keyword parameters for configurable parameters here
    def __init__(self, some_field=u''):
        self.some_field = some_field

    @property
    def title(self):
        """This property is used to give the title of the portlet in the
        "manage portlets" screen.
        """
        return self.some_field

class Renderer(base.Renderer):
    """Portlet renderer.

    This is registered in configure.zcml. The referenced page template is
    rendered, and the implicit variable 'view' will refer to an instance
    of this class. Other methods can be added and referenced in the template.
    """

    render = ViewPageTemplateFile('portletCoPSocialNetworks.pt')

    def __init__(self, *args):
        base.Renderer.__init__(self, *args)
        context = getCoPContext(self.context)
        self.facebook_user_name = context.getFacebook()
        self.twitter_user_name = context.getTwitter()
        self.youtube_user_name = context.getYoutube()
        if context.portal_type == 'CoP':
            context = aq_parent(aq_inner(context))
            if not self.twitter_user_name:
                self.twitter_user_name = context.getTwitter()
            if not self.facebook_user_name:
                self.facebook_user_name = context.getFacebook()
            if not self.youtube_user_name:
                self.youtube_user_name = context.getYoutube()


# NOTE: If this portlet does not have any configurable parameters, you can
# inherit from NullAddForm and remove the form_fields variable.

class AddForm(base.AddForm):
    """Portlet add form.

    This is registered in configure.zcml. The form_fields variable tells
    zope.formlib which fields to display. The create() method actually
    constructs the assignment that is being added.
    """
    form_fields = form.Fields(IPortletCoPSocialNetworks)

    def create(self, data):
        return Assignment(**data)


# NOTE: IF this portlet does not have any configurable parameters, you can
# remove this class definition and delete the editview attribute from the
# <plone:portlet /> registration in configure.zcml

class EditForm(base.EditForm):
    """Portlet edit form.

    This is registered with configure.zcml. The form_fields variable tells
    zope.formlib which fields to display.
    """
    form_fields = form.Fields(IPortletCoPSocialNetworks)
